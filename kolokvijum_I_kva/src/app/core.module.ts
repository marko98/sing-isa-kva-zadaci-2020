import { NgModule } from "@angular/core";
import { MainService } from './shared/service/main.service';

@NgModule({
    providers: [
        MainService
    ]
})
export class CoreModule {}