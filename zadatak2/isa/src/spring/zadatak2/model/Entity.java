package spring.zadatak2.model;

import spring.zadatak2.dto.DTO;

public interface Entity<T> {
	public String getIdentifikator();
	public void update(T t);
	public DTO getDTO();
}
