import { Component, OnInit, OnDestroy } from '@angular/core';

// model
import { Table } from 'my-angular-table';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit, OnDestroy {

  constructor() {}

  ngOnInit(): void {
    console.log("WelcomeComponent init");
  }

  ngOnDestroy(): void {
    console.log("WelcomeComponent destroyed");
  }

}
