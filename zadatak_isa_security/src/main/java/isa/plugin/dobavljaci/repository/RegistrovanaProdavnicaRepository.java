package isa.plugin.dobavljaci.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import isa.model.Dobavljaci.RegistrovanaProdavnica;
import isa.repository.CrudRepository;

@Repository
@Scope("singleton")

public class RegistrovanaProdavnicaRepository extends CrudRepository<RegistrovanaProdavnica, Long> {

}