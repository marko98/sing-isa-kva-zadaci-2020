import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
  CLASS_DIAGRAM_DATA,
  ClassInterface,
} from "./shared/model/patterns/structural/composite/class/interfaces";

export declare interface MiserablesData {
  nodes: [
    {
      id: string;
      group: number;
    }
  ];
  links: [
    {
      source: string;
      target: string;
      value: number;
    }
  ];
}

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  podaci = [];
  public data: MiserablesData;
  public classDiagramData: ClassInterface[];

  constructor(private httpClient: HttpClient) {
    this.httpClient.get<MiserablesData>("../assets/miserables.json").subscribe(
      (data: MiserablesData) => {
        this.data = data;
      },
      (err) => console.log(err)
    );

    this.classDiagramData = CLASS_DIAGRAM_DATA;
  }

  public ngOnInit() {
    this.noviPodaci();
  }

  public noviPodaci() {
    let length = 10; //Math.trunc(Math.random()*100+10);
    let tmp = [];
    for (let i = 0; i < length; i++) {
      tmp.push({
        x: i * 10,
        y: Math.random() * 200,
      });
    }

    this.podaci = tmp;

    // console.log("noviPodaci() se izvrsilo");
  }
}
