import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

// service
import { UserService } from './user.service';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

    constructor(private userService: UserService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let headers;
        if(this.userService.getLoggedUser()){
            let identificator = this.userService.getLoggedUser().getIdentificator();

            headers = req.headers
                .set('Content-Type', 'application/json')
                .set("identificator", identificator);
        } else {
            headers = req.headers.set('Content-Type', 'application/json');
        }

        const authReq = req.clone({ headers });
        // console.log(authReq);
        return next.handle(authReq);
    }
}