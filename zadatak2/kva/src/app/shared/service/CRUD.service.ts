// repository
import { CRUDRepository } from '../repository/CRUD.repository';

// model
import { Observable } from '../model/patterns/behavioural/observer/observable.model';

export class CRUDService<T> extends Observable {
	private path: string;
    
    constructor(private repository: CRUDRepository<T>, mainPath: string){
		super();
		this.path = mainPath;
    }

    findAll = (): T[] => {
		// vracamo kopiju
		return this.repository.findAll().slice();
	}
	
	findOne = (identifikator: string): T => {
		return this.repository.findOne(identifikator);
	}
	
	save = (model: T): void => {
		this.repository.save(this.path, model);
		this.notify();
	}
	
	delete = (model: T): void => {
		this.repository.delete(this.path, model);
		this.notify();
	}
	
	deleteByIdentificatior = (identifikator: string): void => {
		this.repository.deleteByIdentificator(this.path, identifikator);
		this.notify();
    }
    
    // override-ujemo
    notify = () => {
        this.observers.forEach(observer => {
            observer(this);
        });
    }

}