package isa.plugin.prodavnice.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import isa.model.Prodavnice.Prodavnica;
import isa.service.CrudService;

@Service
@Scope("singleton")

public class ProdavnicaService extends CrudService<Prodavnica, Long> {

}