import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  Input,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import * as d3 from "d3";

@Component({
  selector: "app-line-chart",
  templateUrl: "./line-chart.component.html",
  styleUrls: ["./line-chart.component.css"],
})
export class LineChartComponent implements OnInit, OnChanges {
  @Input("data")
  data = [];

  private svg;
  private line;
  private chart;
  private xAxis;
  private yAxis;
  private x;
  private y;
  private marginHorizontal = 25;
  private marginVertical = 25;
  private width = 500;
  private height = 500;

  constructor(private element: ElementRef) {}

  ngOnInit(): void {
    this.svg = d3
      .select(this.element.nativeElement)
      .append("svg")
      .attr("height", this.height)
      .attr("width", this.width);
    this.updateAxis();

    this.line = d3
      .line()
      .x((d) => this.x(d["x"]))
      .y((d) => this.y(d["y"]))
      .curve(d3.curveLinear);

    let g = this.svg
      .append("g")
      .attr("transform", `translate(${this.marginHorizontal}, ${0})`);
    this.chart = g.append("path");
    this.updateLine();
  }

  private updateAxis() {
    if (this.svg) {
      this.x = d3
        .scaleLinear()
        .domain(d3.extent(this.data, (d) => d.x))
        .range([0, this.width - this.marginHorizontal * 2]);
      this.xAxis = (g) =>
        g
          .attr(
            "transform",
            `translate(${this.marginHorizontal},${
              this.height - this.marginVertical
            })`
          )
          .call(d3.axisBottom(this.x));

      this.y = d3
        .scaleLinear()
        .domain(d3.extent(this.data, (d) => d.y))
        .nice()
        .range([this.height - this.marginVertical, this.marginVertical]);
      this.yAxis = (g) =>
        g
          .attr("transform", `translate(${this.marginHorizontal},0)`)
          .call(d3.axisLeft(this.y));
      this.svg.selectAll(".axis-x").remove();
      this.svg.selectAll(".axis-y").remove();
      this.svg.append("g").attr("class", "axis-x").call(this.yAxis);
      this.svg.append("g").attr("class", "axis-y").call(this.xAxis);
    }
  }

  private updateLine() {
    if (this.chart) {
      this.chart
        .datum(this.data)
        .attr("class", "line-chart")
        .attr("d", <any>this.line(<any>this.data));
    }
  }

  public ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if (changes.data.currentValue != changes.data.previousValue) {
      this.updateAxis();
      this.updateLine();
      console.log("this.updateAxis(), this.updateLine() se izvrsilo");
    }
  }
}
