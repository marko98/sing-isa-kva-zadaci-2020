package com.isa.app.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.isa.app.model.Korisnik;
import com.isa.app.model.Uloga;

@Repository
@Scope("singleton")
public class KorisnikRepository extends CrudRepository<Korisnik, Integer> {
	
	public KorisnikRepository() {
		super();
	}
	
	@PostConstruct
	private void onInit() {
//		get session
		Session session = this.hibernateSessionFactory.getSession();
		
//		begin transaction
		session.beginTransaction();
		
//		CRUD...
//		create user if no one exists
		
		Query<Korisnik> query = session.createQuery("select k from Korisnik k", Korisnik.class);
		if(query.getResultList().isEmpty()) {
			session.save(new Korisnik("Marko", "Zahorodni", "marko98", Uloga.ADMINISTRATOR, "marko98"));
			session.save(new Korisnik("David", "Zahorodni", "david98", Uloga.ADMINISTRATOR, "david98"));
			session.save(new Korisnik("Maja", "Zahorodni", "maja2001", Uloga.KUPAC, "maja2001"));
			session.save(new Korisnik("Vesna", "Zahorodni", "vesna69", Uloga.KUPAC, "vesna69"));
			session.save(new Korisnik("Ivan", "Zahorodni", "ivan65", Uloga.ADMINISTRATOR, "ivan65"));
		}
		
//		commit transaction
		session.getTransaction().commit();
		
//		close session
		session.close();
	}

	public List<Korisnik> findByRole(Uloga uloga) {
//		get all korisnici
		Iterable<Korisnik> korisnici = this.pagingAndSortingRepository.findAll();
		
//		get iterator
		Iterator<Korisnik> iterator = korisnici.iterator();
		
		List<Korisnik> pronadjeniKorisnici = new ArrayList<Korisnik>();
		
		while (iterator.hasNext()) {
			Korisnik k = iterator.next();
			if (k.getUloga().equals(uloga))
				pronadjeniKorisnici.add(k);
		}
		
		return pronadjeniKorisnici;
	}
	
	public Korisnik findByUsername(String korisnickoIme) {
//		get session
		Session session = this.hibernateSessionFactory.getSession();
		
//		begin transaction
		session.beginTransaction();
		
		Query<Korisnik> query = session.createQuery("select k from Korisnik k "
													+ "where k.korisnickoIme=:korisnickoIme",
													Korisnik.class);
	
		query.setParameter("korisnickoIme", korisnickoIme);
		
		Korisnik korisnik = query.getSingleResult();
		
//		commit transaction
		session.getTransaction().commit();
		
//		close session
		session.close();
		
		return korisnik;
	}
	
}