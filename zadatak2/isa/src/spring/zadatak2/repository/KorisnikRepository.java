package spring.zadatak2.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import spring.zadatak2.model.Korisnik;
import spring.zadatak2.model.Uloga;

@Repository
@Scope("singleton")
public class KorisnikRepository extends CRUDRepository<Korisnik> {	
	public KorisnikRepository() {
		super();
		this.models = new ArrayList<Korisnik>();
		this.models.add(new Korisnik("Marko", "Zahorodni", "marko98", Uloga.ADMINISTRATOR, "marko98"));
		this.models.add(new Korisnik("David", "Zahorodni", "david98", Uloga.KUPAC, "david98"));
		this.models.add(new Korisnik("Maja", "Zahorodni", "maja2001", Uloga.KUPAC, "maja2001"));
		this.models.add(new Korisnik("Vesna", "Zahorodni", "vesna69", Uloga.ADMINISTRATOR, "vesna69"));
	}
	
	public List<Korisnik> findByRole(Uloga uloga) {
		ArrayList<Korisnik> pronadjeni = new ArrayList<Korisnik>();
		
		for (Korisnik korisnik : models) {
			if(korisnik.getUloga().equals(uloga)) {
				pronadjeni.add(korisnik);
			}
		}
		return pronadjeni;
	}
}
