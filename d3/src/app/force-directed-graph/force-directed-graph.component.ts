import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  ElementRef,
} from "@angular/core";
import * as d3 from "d3";
import { map } from "rxjs/operators";
import { MiserablesData } from "../app.component";

@Component({
  selector: "app-force-directed-graph",
  templateUrl: "./force-directed-graph.component.html",
  styleUrls: ["./force-directed-graph.component.css"],
})
export class ForceDirectedGraphComponent implements OnInit {
  @Input() data: MiserablesData;

  private svg: d3.Selection<any, unknown, null, undefined>;
  private width: number = 500;
  private height: number = 300;

  constructor(private elementRef: ElementRef) {
    // this.onCreateForceDirectedGraphExample();
  }

  onCreateForceDirectedGraph = (): void => {
    const links = this.data.links.map((d) => Object.create(d));
    const nodes = this.data.nodes.map((d) => Object.create(d));

    const simulation = d3
      .forceSimulation(nodes)
      .force(
        "link",
        d3.forceLink(links).id((d) => (<any>d).id)
      )
      .force("charge", d3.forceManyBody())
      .force("center", d3.forceCenter(this.width / 2, this.height / 2));

    this.svg = d3
      .select(this.elementRef.nativeElement)
      .append("svg")
      .attr("viewBox", `0 0 ${this.width} ${this.height}`);

    // const link = this.svg
    //   .append("g")
    //   .attr("stroke", "#999")
    //   .attr("stroke-opacity", 0.6)
    //   .selectAll("line")
    //   .data(links)
    //   .join("line")
    //   .attr("stroke-width", (d) => Math.sqrt(d.value));

    let strelica = d3.create("g");
    strelica
      .append("line")
      .attr("stroke", "red")
      .attr("x1", 5)
      .attr("y1", 20)
      .attr("x2", 55)
      .attr("y2", 20);
    strelica
      .append("path")
      .attr("d", "M 55 20 L 45 15 45 25 Z")
      .attr("fill", "red");

    const link = this.svg
      .append("g")
      .attr("stroke", "#999")
      .attr("stroke-opacity", 0.6)
      .selectAll("g")
      .data(links)
      .join("g");

    link.append("line").attr("stroke-width", (d) => Math.sqrt(d.value));
    link
      .append("path")
      .attr("d", "M 55 20 L 45 15 45 25 Z")
      .attr("fill", "red");

    console.log(link.data());

    // link
    //   .selectAll("g")
    //   .append("path")
    //   .attr("d", "M 55 20 L 45 15 45 25 Z")
    //   .attr("fill", "red");

    const node = this.svg
      .append("g")
      .attr("stroke", "#fff")
      .attr("stroke-width", 1.5)
      .selectAll("circle")
      .data(nodes)
      .join("circle")
      .attr("r", 5)
      .attr("fill", "#ccc")
      .call(this.drag(simulation));

    node.append("title").text((d) => d.id);

    simulation.on("tick", () => {
      link
        .selectAll("line")
        .attr("x1", (d) => (<any>d).source.x)
        .attr("y1", (d) => (<any>d).source.y)
        .attr("x2", (d) => (<any>d).target.x)
        .attr("y2", (d) => (<any>d).target.y);

      link.selectAll("path").attr("d", (d) => {
        let lx = ((<any>d).target.x + (<any>d).source.x) / 2;

        let ly = ((<any>d).target.y + (<any>d).source.y) / 2;

        lx = (lx + (<any>d).target.x) / 2;

        ly = (ly + (<any>d).target.y) / 2;

        return `M ${(<any>d).target.x} ${(<any>d).target.y} L ${lx} ${ly} Z`;
      });

      console.log(link);

      node.attr("cx", (d) => d.x).attr("cy", (d) => d.y);
    });

    // <g>
    //     <line x1=5 y1=20 x2=55 y2=20 style="stroke: green;"></line>
    //     <path d="M 55 20 L 45 15 45 25 Z" fill="green" />
    // </g>

    // console.log(links);
  };

  drag = (simulation) => {
    function dragstarted(d) {
      if (!d3.event.active) simulation.alphaTarget(0.3).restart();
      d.fx = d.x;
      d.fy = d.y;
    }

    function dragged(d) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }

    function dragended(d) {
      if (!d3.event.active) simulation.alphaTarget(0);
      d.fx = null;
      d.fy = null;
    }

    return d3
      .drag()
      .on("start", dragstarted)
      .on("drag", dragged)
      .on("end", dragended);
  };

  onCreateForceDirectedGraphExample = (): void => {
    let width = 640;
    let height = 480;

    let links = [
      { source: "Baratheon", target: "Lannister" },
      { source: "Baratheon", target: "Stark" },
      { source: "Lannister", target: "Stark" },
    ];

    let nodes = {};

    // parse links to nodes
    links.forEach((link) => {
      link.source =
        nodes[link.source] || (nodes[link.source] = { name: link.source });
      link.target =
        nodes[link.target] || (nodes[link.target] = { name: link.target });
    });

    // add svg to body
    this.svg = d3
      .select(this.elementRef.nativeElement)
      .append("svg")
      .attr("viewBox", `0 0 ${width} ${height}`);

    let forceSimulation = d3
      .forceSimulation(d3.values(nodes))
      .force("link", d3.forceLink(links))
      .on("tick", () => {
        node
          .attr("cx", (d) => (<any>d).x)
          .attr("cy", (d) => (<any>d).y)
          .call(drag(forceSimulation));

        link
          .attr("x1", (d) => (<any>d.source).x)
          .attr("y1", (d) => (<any>d.source).y)
          .attr("x2", (d) => (<any>d.target).x)
          .attr("y2", (d) => (<any>d.target).y);
      })
      .force("charge", d3.forceManyBody());

    let link = this.svg
      .selectAll("link")
      .data(links)
      .join("line")
      .attr("class", "link");

    let node = this.svg
      .selectAll("node")
      .data(forceSimulation.nodes())
      .join("circle")
      .attr("class", "node")
      .attr("r", width * 0.03);

    let drag = (simulation) => {
      function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
      }

      function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
      }

      function dragended(d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
      }

      return d3
        .drag()
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended);
    };

    console.log(nodes);
    console.log(links);
  };

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.data.currentValue) {
      this.onCreateForceDirectedGraph();
    }
  }
}
