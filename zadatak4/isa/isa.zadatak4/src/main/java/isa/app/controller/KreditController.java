package isa.app.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.app.model.Kredit;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/kredit")
@CrossOrigin(origins = "*")
public class KreditController extends CrudController<Kredit, Long> {

}