package isa.host.controller.dobavljaci;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.host.controller.HostCrudController;
import isa.model.Dobavljaci.Dobavljac;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/dobavljac")
@CrossOrigin(origins = "*")

public class DobavljacController extends HostCrudController<Dobavljac, Long> {

    public DobavljacController() {
	this.category = "dobavljac";
    }

}