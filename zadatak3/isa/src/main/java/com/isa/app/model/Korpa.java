package com.isa.app.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.isa.app.dto.KorpaDTO;

@Entity
@Table(name = "korpa")
public class Korpa implements ModelEntity<Korpa> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "java.lang.Integer")
	@Column(name = "id")
	private Integer id;
	
	@OneToOne(fetch = FetchType.EAGER,
				cascade = {
						CascadeType.DETACH,
						CascadeType.MERGE,
						CascadeType.PERSIST,
						CascadeType.REFRESH
				},
				optional = true)
	@JoinColumn(name = "vlasnik_id", nullable = true)
	private Korisnik vlasnik;
	
	@OneToMany(mappedBy = "korpa",
				fetch = FetchType.LAZY,
				cascade = CascadeType.ALL)
	private List<Stavka> stavke;

	public Korpa() {
		super();
	}

	public Korpa(Korisnik vlasnik) {
		super();
		this.vlasnik = vlasnik;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Korisnik getVlasnik() {
		return vlasnik;
	}

	public void setVlasnik(Korisnik vlasnik) {
		this.vlasnik = vlasnik;
	}

	public List<Stavka> getStavke() {
		return stavke;
	}

	public void setStavke(List<Stavka> stavke) {
		this.stavke = stavke;
	}

	@Override
	public String toString() {
		return "Korpa [id=" + id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Korpa other = (Korpa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	//	interface
	public Integer getIdentifikator() {
		return this.id;
	};
	
	
	public void update(Korpa korpa) {
		this.setStavke(korpa.getStavke());
		this.setVlasnik(korpa.getVlasnik());
	};
	
	public KorpaDTO getDTO() {
		return new KorpaDTO(this.id, this.vlasnik, this.stavke);
	};
}
