package isa.model.Dobavljaci;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class RegistrovanaProdavnicaRola {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private RegistrovanaProdavnica registrovanaProdavnica;

    @ManyToOne
    private Rola rola;

    public RegistrovanaProdavnicaRola() {
    }

    public RegistrovanaProdavnicaRola(Long id, RegistrovanaProdavnica registrovanaProdavnica, Rola rola) {
	super();
	this.id = id;
	this.registrovanaProdavnica = registrovanaProdavnica;
	this.rola = rola;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public RegistrovanaProdavnica getRegistrovanaProdavnica() {
	return registrovanaProdavnica;
    }

    public void setRegistrovanaProdavnica(RegistrovanaProdavnica registrovanaProdavnica) {
	this.registrovanaProdavnica = registrovanaProdavnica;
    }

    public Rola getRola() {
	return rola;
    }

    public void setRola(Rola rola) {
	this.rola = rola;
    }

}
