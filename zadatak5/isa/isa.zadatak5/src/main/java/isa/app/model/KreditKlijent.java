package isa.app.model;

import java.time.LocalDateTime;

import javax.persistence.GenerationType;

import isa.app.dto.KreditKlijentDTO;

@javax.persistence.Entity
public class KreditKlijent implements Entity<KreditKlijent, Long> {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime datum;

    @javax.persistence.ManyToOne
    private Klijent klijent;

    @javax.persistence.ManyToOne
    private Kredit kredit;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public Klijent getKlijent() {
	return this.klijent;
    }

    public void setKlijent(Klijent klijent) {
	this.klijent = klijent;
    }

    public Kredit getKredit() {
	return this.kredit;
    }

    public void setKredit(Kredit kredit) {
	this.kredit = kredit;
    }

    public KreditKlijent() {
	super();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((datum == null) ? 0 : datum.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((klijent == null) ? 0 : klijent.hashCode());
	result = prime * result + ((kredit == null) ? 0 : kredit.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	KreditKlijent other = (KreditKlijent) obj;
	if (datum == null) {
	    if (other.datum != null)
		return false;
	} else if (!datum.equals(other.datum))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (klijent == null) {
	    if (other.klijent != null)
		return false;
	} else if (!klijent.equals(other.klijent))
	    return false;
	if (kredit == null) {
	    if (other.kredit != null)
		return false;
	} else if (!kredit.equals(other.kredit))
	    return false;
	return true;
    }

    @Override
    public KreditKlijentDTO getDTO() {
	KreditKlijentDTO kreditKlijentDTO = new KreditKlijentDTO();
	kreditKlijentDTO.setId(id);
	kreditKlijentDTO.setDatum(datum);

	/*
	 * BITNO!!!
	 * 
	 * za slozene tipove koji su opcioni za entitet, a nisu liste, pre poziva bilo
	 * kakvih metoda nad njima(npr. getDTOinsideDTO()) proveriti da li su razliciti
	 * od null
	 */
	if (klijent != null)
	    kreditKlijentDTO.setKlijentDTO(klijent.getDTOinsideDTO());
	if (kredit != null)
	    kreditKlijentDTO.setKreditDTO(kredit.getDTOinsideDTO());

	return kreditKlijentDTO;
    }

    @Override
    public KreditKlijentDTO getDTOinsideDTO() {
	KreditKlijentDTO kreditKlijentDTO = new KreditKlijentDTO();
	kreditKlijentDTO.setId(id);
	kreditKlijentDTO.setDatum(datum);

	return kreditKlijentDTO;
    }
    
    @Override
    public void update(KreditKlijent entity) {
	/*
	 * BITNO!!! (primer za entitet Banka)
	 * 
	 * kod polja koja su liste imamo problem -> recimo da zelimo da update-ujemo
	 * banku, ali samo njen naziv mi onda moramo da dostavimo i sve klijente i sve
	 * kredite, ako sa druge strane izbacimo iz ove metode setovanje polja koje su
	 * liste, onda mozemo bez problema update-ovati samo naziv banke ili jos neka
	 * polja ako ih bude imala bez da moramo da dovlacimo sve klijente i kredite
	 * 
	 * ako zelimo da iz banke izbacimo neki krediti onda buduci da banka ima vise
	 * kredita, a kredit ima jednu banku, update-ujemo taj kredit odnosno kod njega
	 * za polje banka stavljamo null ili ga u potpunosti brisemo isto vazi i za
	 * klijene, medjutim da bi to moglo tako da radi ne smemo imati vezu izmedju dve
	 * klase vise na prema vise, vec ubaciti izmedju klasu koja ce ih 'spajati', ta
	 * dodatna klasa bi trebala da ima kardinalitete prema ove dve klase 1 na prema
	 * vise ili slicno, ali nikako vise na prema vise
	 * 
	 * dakle: kod update-a ne update-ujemo liste SLOZENIH tipova(entiteta), ako ima
	 * potrebe za izbacivanjem nekih ili dodavanjem nekih u listu obracamo se ili
	 * njima(slucaj jedan na prema vise) ili dodatnoj klasi koja je spoj izmedju
	 * entiteta koji 'imaju' kardinalitet vise na prema vise
	 */
	
        this.setDatum(entity.getDatum());
        this.setKlijent(entity.getKlijent());
        this.setKredit(entity.getKredit());
    }
}