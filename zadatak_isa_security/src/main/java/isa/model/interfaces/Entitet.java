package isa.model.interfaces;

import java.io.Serializable;

import isa.dto.interfaces.DTO;
import isa.interfaces.Identifikacija;

public interface Entitet<T, ID extends Serializable> extends Identifikacija<ID> {

    public DTO<ID> getDTO();

    public DTO<ID> getDTOinsideDTO();

    public void update(T entitet);

}