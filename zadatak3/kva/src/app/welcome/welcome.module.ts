import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { WelcomeRoutingModule } from './welcome-routing.module';

// component
import { WelcomeComponent } from './welcome.component';

@NgModule({
    declarations: [
        WelcomeComponent
    ],
    imports: [
        SharedModule,
        WelcomeRoutingModule
    ],
    exports: [
        WelcomeRoutingModule
    ]
})
export class WelcomeModule {}