import { Component, OnInit, OnDestroy } from '@angular/core';

// model
import { Article } from '../shared/model/article.model';

// service
import { ArticleService } from '../shared/service/article.service';

@Component({
    selector: 'app-articles',
    templateUrl: './articles.component.html',
    styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit, OnDestroy {
  public articles: Article[] = [];
  public articleToUpdate: Article;
  public shouldUpdate: boolean = false;
  public shouldCreate: boolean = false;

  constructor(private articleService: ArticleService){}

  onAddArticle = (): void => {
    this.shouldCreate = true;
  }

  onDone = (): void => {
    this.shouldCreate = false;
    this.shouldUpdate = false;
    this.articleToUpdate = null;
  }

  onUpdateArticle = (article: Article): void => {
    this.articleToUpdate = article;
    this.shouldUpdate = true;
  }

  onDeleteArticle = (identificator: string): void => {
    this.articleService.deleteByIdentificatior(identificator);
  }

  /*
    observers nisu nista drugo do same funkcije koja treba da se pozove kada nad
    objektom tipa Observable na koju smo subscribe-ovani dodje do promena
  */
  articleServiceObserver = (observable: ArticleService): void => {
    // console.log("AppComponent notified about changes in ArticleService");
    // console.log(observable);
    this.articles = observable.findAll();
  }

  // interfaces
  ngOnInit() {
    this.articleService.attach(this.articleServiceObserver);

    this.articles = this.articleService.findAll();

    // console.log("ArticlesComponent init");
  }

  ngOnDestroy() {
    if (this.articleService)
      this.articleService.dettach(this.articleServiceObserver);

    // console.log("ArticlesComponent destroyed");
  }
}