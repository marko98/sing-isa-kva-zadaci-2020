package isa.dto.Prodavnice;

import isa.dto.interfaces.DTO;

public class RobaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1997029970095087260L;

    private Long id;

    private String naziv;

    private Double kolicina;

    private Double cena;

    private ProdavnicaDTO prodavnicaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public Double getKolicina() {
	return this.kolicina;
    }

    public void setKolicina(Double kolicina) {
	this.kolicina = kolicina;
    }

    public Double getCena() {
	return this.cena;
    }

    public void setCena(Double cena) {
	this.cena = cena;
    }

    public ProdavnicaDTO getProdavnicaDTO() {
	return this.prodavnicaDTO;
    }

    public void setProdavnicaDTO(ProdavnicaDTO prodavnicaDTO) {
	this.prodavnicaDTO = prodavnicaDTO;
    }

    public RobaDTO() {
	super();
    }

}