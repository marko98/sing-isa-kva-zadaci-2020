package isa.app.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import isa.app.model.Kredit;

@Service
@Scope("singleton")
public class KreditService extends CrudService<Kredit, Long> {

}