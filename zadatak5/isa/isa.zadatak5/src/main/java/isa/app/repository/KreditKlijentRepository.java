package isa.app.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import isa.app.model.KreditKlijent;

@Repository
@Scope("singleton")
public class KreditKlijentRepository extends CrudRepository<KreditKlijent, Long> {
    
}