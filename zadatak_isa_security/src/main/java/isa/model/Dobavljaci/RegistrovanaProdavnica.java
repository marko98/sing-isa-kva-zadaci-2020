package isa.model.Dobavljaci;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import isa.dto.Dobavljaci.RegistrovanaProdavnicaDTO;
import isa.model.interfaces.Entitet;

@javax.persistence.Entity

@Table(name = "registrovana_prodavnica")
public class RegistrovanaProdavnica implements Entitet<RegistrovanaProdavnica, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -4002656341077866700L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "registrovana_prodavnica_id")
    private Long id;

    private String pib;

    private String maticniBroj;

    private String naziv;

    private String lozinka;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "dobavljac_id")
    private Dobavljac dobavljac;

    @OneToMany(mappedBy = "registrovanaProdavnica", cascade = CascadeType.ALL)
    private Set<RegistrovanaProdavnicaRola> registrovanaProdavnicaRole;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getPib() {
	return this.pib;
    }

    public void setPib(String pib) {
	this.pib = pib;
    }

    public String getMaticniBroj() {
	return this.maticniBroj;
    }

    public void setMaticniBroj(String maticniBroj) {
	this.maticniBroj = maticniBroj;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public String getLozinka() {
	return this.lozinka;
    }

    public void setLozinka(String lozinka) {
	this.lozinka = lozinka;
    }

    public Dobavljac getDobavljac() {
	return this.dobavljac;
    }

    public void setDobavljac(Dobavljac dobavljac) {
	this.dobavljac = dobavljac;
    }

    public Set<RegistrovanaProdavnicaRola> getRegistrovanaProdavnicaRole() {
        return registrovanaProdavnicaRole;
    }

    public void setRegistrovanaProdavnicaRole(Set<RegistrovanaProdavnicaRola> registrovanaProdavnicaRole) {
        this.registrovanaProdavnicaRole = registrovanaProdavnicaRole;
    }

    public RegistrovanaProdavnica() {
	super();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((dobavljac == null) ? 0 : dobavljac.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((lozinka == null) ? 0 : lozinka.hashCode());
	result = prime * result + ((maticniBroj == null) ? 0 : maticniBroj.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((pib == null) ? 0 : pib.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	RegistrovanaProdavnica other = (RegistrovanaProdavnica) obj;
	if (dobavljac == null) {
	    if (other.dobavljac != null)
		return false;
	} else if (!dobavljac.equals(other.dobavljac))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (lozinka == null) {
	    if (other.lozinka != null)
		return false;
	} else if (!lozinka.equals(other.lozinka))
	    return false;
	if (maticniBroj == null) {
	    if (other.maticniBroj != null)
		return false;
	} else if (!maticniBroj.equals(other.maticniBroj))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (pib == null) {
	    if (other.pib != null)
		return false;
	} else if (!pib.equals(other.pib))
	    return false;
	return true;
    }

    @Override
    public RegistrovanaProdavnicaDTO getDTO() {
	RegistrovanaProdavnicaDTO registrovanaProdavnicaDTO = new RegistrovanaProdavnicaDTO();
	registrovanaProdavnicaDTO.setId(id);
	registrovanaProdavnicaDTO.setLozinka(lozinka);
	registrovanaProdavnicaDTO.setMaticniBroj(maticniBroj);
	registrovanaProdavnicaDTO.setNaziv(naziv);
	registrovanaProdavnicaDTO.setPib(pib);

	if (this.dobavljac != null)
	    registrovanaProdavnicaDTO.setDobavljacDTO(this.dobavljac.getDTOinsideDTO());

	return registrovanaProdavnicaDTO;
    }

    @Override
    public RegistrovanaProdavnicaDTO getDTOinsideDTO() {
	RegistrovanaProdavnicaDTO registrovanaProdavnicaDTO = new RegistrovanaProdavnicaDTO();
	registrovanaProdavnicaDTO.setId(id);
	registrovanaProdavnicaDTO.setLozinka(lozinka);
	registrovanaProdavnicaDTO.setMaticniBroj(maticniBroj);
	registrovanaProdavnicaDTO.setNaziv(naziv);
	registrovanaProdavnicaDTO.setPib(pib);
	return registrovanaProdavnicaDTO;
    }

    @Override
    public void update(RegistrovanaProdavnica entitet) {
	this.setLozinka(entitet.getLozinka());
	this.setMaticniBroj(entitet.getMaticniBroj());
	this.setNaziv(entitet.getNaziv());
	this.setPib(entitet.getPib());

	this.setDobavljac(entitet.getDobavljac());

    }

}