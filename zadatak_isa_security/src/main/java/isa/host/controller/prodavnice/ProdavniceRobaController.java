package isa.host.controller.prodavnice;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.host.controller.HostCrudController;
import isa.model.Prodavnice.Roba;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/prodavnice_roba")
@CrossOrigin(origins = "*")
public class ProdavniceRobaController extends HostCrudController<Roba, Long> {

    public ProdavniceRobaController() {
	this.category = "prodavnice_roba";
    }

}