package com.isa.app.model;

import com.isa.app.dto.DTO;

public interface ModelEntity<T> {
	public Integer getId();
	public void update(T t);
	public DTO getDTO();
}
