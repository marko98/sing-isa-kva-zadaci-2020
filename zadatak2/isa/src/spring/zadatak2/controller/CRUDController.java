package spring.zadatak2.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import spring.zadatak2.dto.DTO;
import spring.zadatak2.model.Entity;
import spring.zadatak2.service.CRUDService;

public abstract class CRUDController<T extends Entity<T>>  {
	@Autowired
	protected CRUDService<T> service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<List<DTO>> getAll() {
		System.out.println(this.getClass());
		
		return new ResponseEntity<List<DTO>>(this.service.findAll().stream().map(t -> t.getDTO()).collect(Collectors.toList()), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{identifikator}", method = RequestMethod.GET)
	public ResponseEntity<?> get(@PathVariable("identifikator") String identifikator) {
		T t = this.service.findOne(identifikator);
		if (t != null)
			return new ResponseEntity<DTO>(t.getDTO(), HttpStatus.OK);
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody T t) {
		if (this.service.findOne(t.getIdentifikator()) != null)
			return new ResponseEntity<String>("Username is taken", HttpStatus.CONFLICT);
		this.service.save(t);
		return new ResponseEntity<DTO>(t.getDTO(), HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@RequestBody T changedT) {
		T originalT = this.service.findOne(changedT.getIdentifikator());
		if(originalT == null) 
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		this.service.save(changedT);
		return new ResponseEntity<DTO>(changedT.getDTO(), HttpStatus.OK);
	}
	
	@RequestMapping(path = "", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@RequestBody T changedT) {
		T originalT = this.service.findOne(changedT.getIdentifikator());
		if(originalT == null) 
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		this.service.delete(changedT.getIdentifikator());
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{identifikator}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable("identifikator") String identifikator) {
		T originalT = this.service.findOne(identifikator);
		if(originalT == null) 
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		this.service.delete(identifikator);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
