package isa.model.Dobavljaci;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import isa.dto.Dobavljaci.RobaUMagacinuDTO;
import isa.model.interfaces.Entitet;

@javax.persistence.Entity

@Table(name = "roba_u_magacinu")
public class RobaUMagacinu implements Entitet<RobaUMagacinu, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -7458645396131866415L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "roba_u_magacinu_id")
    private Long id;

    private Double kolicina;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "dobavljac_id")
    private Dobavljac dobavljac;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "roba_id")
    private Roba roba;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Double getKolicina() {
	return this.kolicina;
    }

    public void setKolicina(Double kolicina) {
	this.kolicina = kolicina;
    }

    public Dobavljac getDobavljac() {
	return this.dobavljac;
    }

    public void setDobavljac(Dobavljac dobavljac) {
	this.dobavljac = dobavljac;
    }

    public Roba getRoba() {
	return this.roba;
    }

    public void setRoba(Roba roba) {
	this.roba = roba;
    }

    public RobaUMagacinu() {
	super();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((dobavljac == null) ? 0 : dobavljac.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((kolicina == null) ? 0 : kolicina.hashCode());
	result = prime * result + ((roba == null) ? 0 : roba.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	RobaUMagacinu other = (RobaUMagacinu) obj;
	if (dobavljac == null) {
	    if (other.dobavljac != null)
		return false;
	} else if (!dobavljac.equals(other.dobavljac))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (kolicina == null) {
	    if (other.kolicina != null)
		return false;
	} else if (!kolicina.equals(other.kolicina))
	    return false;
	if (roba == null) {
	    if (other.roba != null)
		return false;
	} else if (!roba.equals(other.roba))
	    return false;
	return true;
    }

    @Override
    public RobaUMagacinuDTO getDTO() {
	RobaUMagacinuDTO robaUMagacinuDTO = new RobaUMagacinuDTO();
	robaUMagacinuDTO.setId(id);
	robaUMagacinuDTO.setKolicina(kolicina);

	if (this.dobavljac != null)
	    robaUMagacinuDTO.setDobavljacDTO(this.dobavljac.getDTOinsideDTO());
	if (this.roba != null)
	    robaUMagacinuDTO.setRobaDTO(this.roba.getDTOinsideDTO());

	return robaUMagacinuDTO;
    }

    @Override
    public RobaUMagacinuDTO getDTOinsideDTO() {
	RobaUMagacinuDTO robaUMagacinuDTO = new RobaUMagacinuDTO();
	robaUMagacinuDTO.setId(id);
	robaUMagacinuDTO.setKolicina(kolicina);
	return robaUMagacinuDTO;
    }

    @Override
    public void update(RobaUMagacinu entitet) {
	this.setKolicina(entitet.getKolicina());

	this.setDobavljac(entitet.getDobavljac());
	this.setRoba(entitet.getRoba());

    }

}