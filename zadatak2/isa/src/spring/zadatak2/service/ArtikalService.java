package spring.zadatak2.service;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import spring.zadatak2.model.Artikal;
import spring.zadatak2.repository.ArtikalRepository;

@Service
@Scope("singleton")
public class ArtikalService extends CRUDService<Artikal> {
	
	public List<Artikal> findByPrice(Double donjaCena, Double gornjaCena) {
		return ((ArtikalRepository)this.cRUDRepository).findByPrice(gornjaCena, donjaCena);
	}
	
}
