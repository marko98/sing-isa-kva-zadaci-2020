package isa.dto.Dobavljaci;

import isa.dto.interfaces.DTO;

public class RobaUMagacinuDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -9191285627649694377L;

    private Long id;

    private Double kolicina;

    private DobavljacDTO dobavljacDTO;

    private RobaDTO robaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Double getKolicina() {
	return this.kolicina;
    }

    public void setKolicina(Double kolicina) {
	this.kolicina = kolicina;
    }

    public DobavljacDTO getDobavljacDTO() {
	return this.dobavljacDTO;
    }

    public void setDobavljacDTO(DobavljacDTO dobavljacDTO) {
	this.dobavljacDTO = dobavljacDTO;
    }

    public RobaDTO getRobaDTO() {
	return this.robaDTO;
    }

    public void setRobaDTO(RobaDTO robaDTO) {
	this.robaDTO = robaDTO;
    }

    public RobaUMagacinuDTO() {
	super();
    }

}