package com.isa.app.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.isa.app.dto.StavkaDTO;

@Entity
@Table(name = "stavka")
public class Stavka implements ModelEntity<Stavka> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "java.lang.Integer")
	@Column(name = "id")
	private Integer id;
	
	@ManyToOne(fetch = FetchType.EAGER,
				cascade = {
						CascadeType.DETACH,
						CascadeType.MERGE,
						CascadeType.PERSIST,
						CascadeType.REFRESH
				},
				optional = true)
	@JoinColumn(name = "korpa_id", nullable = true)
	private Korpa korpa;
	
	@ManyToOne(fetch = FetchType.EAGER,
			cascade = {
					CascadeType.DETACH,
					CascadeType.MERGE,
					CascadeType.PERSIST,
					CascadeType.REFRESH
			},
			optional = true)
	@JoinColumn(name = "artikal_id", nullable = true)
	private Artikal artikal;
	
	@Type(type = "java.lang.Integer")
	@Column(name = "kolicina")
	private Integer kolicina;
	
	@ManyToOne(fetch = FetchType.EAGER,
				cascade = {
						CascadeType.DETACH,
						CascadeType.MERGE,
						CascadeType.PERSIST,
						CascadeType.REFRESH
				},
				optional = true)
	@JoinColumn(name = "racun_id", nullable = true)
	private Racun racun;

	public Stavka() {
		super();
	}

	public Stavka(Korpa korpa, Artikal artikal, Integer kolicina) {
		super();
		this.korpa = korpa;
		this.artikal = artikal;
		this.kolicina = kolicina;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Korpa getKorpa() {
		return korpa;
	}

	public void setKorpa(Korpa korpa) {
		this.korpa = korpa;
	}

	public Artikal getArtikal() {
		return artikal;
	}

	public void setArtikal(Artikal artikal) {
		this.artikal = artikal;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public Racun getRacun() {
		return racun;
	}

	public void setRacun(Racun racun) {
		this.racun = racun;
	}

	@Override
	public String toString() {
		return "Stavka [id=" + id + ", korpa=" + korpa + ", artikal=" + artikal + ", kolicina=" + kolicina + ", racun="
				+ racun + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stavka other = (Stavka) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
//	interface
	public Integer getIdentifikator() {
		return this.id;
	};
	
	public void update(Stavka stavka) {
		this.setArtikal(stavka.getArtikal());
		this.setKolicina(stavka.getKolicina());
		this.setKorpa(stavka.getKorpa());
		this.setRacun(stavka.getRacun());
	};
	
	public StavkaDTO getDTO() {
		return new StavkaDTO(this.id, this.korpa, this.artikal, this.kolicina, this.racun);
	};
	
}
