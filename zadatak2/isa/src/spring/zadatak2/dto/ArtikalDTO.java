package spring.zadatak2.dto;

public class ArtikalDTO extends DTO {
	private String naziv, opis, identifikator;
	private Double cena;
	
	public ArtikalDTO() {}
	
	public ArtikalDTO(String naziv, Double cena, String opis, String identifikator) {
		this.naziv = naziv;
		this.opis = opis;
		this.cena = cena;
		this.identifikator = identifikator;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public void setIdentifikator(String identifikator) {
		this.identifikator = identifikator;
	}
	
	public String getIdentifikator() {
		return this.identifikator;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}
}
