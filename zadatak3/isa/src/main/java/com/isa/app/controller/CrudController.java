package com.isa.app.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.isa.app.dto.DTO;
import com.isa.app.model.ModelEntity;
import com.isa.app.service.CrudService;

public abstract class CrudController<T extends ModelEntity<T>, ID extends Serializable>  {
	
	@Autowired
	protected CrudService<T, ID> service;
	
//	http://localhost:8080/isa.zadatak.3/api/korisnici?page=0&size=5&sort=uloga,desc
	
//	pogledaj za vise:
//		https://docs.spring.io/spring-data/rest/docs/current/reference/html/#paging-and-sorting.sorting
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<DTO>> getAll(Pageable pageable) {
		System.out.println(pageable); // Page request [number: 0, size 20, sort: UNSORTED]

		Page<T> page = this.service.findAll(pageable);
//		System.out.println(page);
		
//		konvertovanje
		Page<DTO> pageDTO = page.map(t -> t.getDTO());
		
		return new ResponseEntity<Page<DTO>>(pageDTO, HttpStatus.OK);
	}	
	
	@RequestMapping(path = "/{identifikator}", method = RequestMethod.GET)
	public ResponseEntity<?> get(@PathVariable("identifikator") Integer identifikator) {
		Optional<T> t = this.service.findOne(identifikator);
		if (t.isPresent())
			return new ResponseEntity<DTO>(t.get().getDTO(), HttpStatus.OK);
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody T t) {
		if (t.getId() != null && this.service.findOne(t.getId()) != null)
			return new ResponseEntity<String>("Username is taken", HttpStatus.CONFLICT);
		
		this.service.save(t);
		
		return new ResponseEntity<DTO>(t.getDTO(), HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@RequestBody T changedT) {
		Optional<T> originalT = this.service.findOne(changedT.getId());
		
		if(originalT.isEmpty()) 
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		
		this.service.save(changedT);
		
		return new ResponseEntity<DTO>(changedT.getDTO(), HttpStatus.OK);
	}
	
	@RequestMapping(path = "", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@RequestBody T changedT) {
		Optional<T> originalT = this.service.findOne(changedT.getId());
		
		if(originalT.isEmpty()) 
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		
		this.service.delete(changedT.getId());
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{identifikator}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable("identifikator") int identifikator) {
		Optional<T> originalT = this.service.findOne(identifikator);
		
		if(originalT.isEmpty()) 
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		
		this.service.delete(identifikator);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
