package isa.model.Prodavnice;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Table;

import isa.dto.Prodavnice.ProdavnicaDTO;
import isa.dto.Prodavnice.RobaDTO;
import isa.model.interfaces.Entitet;

@javax.persistence.Entity

@Table(name = "prodavnica")
public class Prodavnica implements Entitet<Prodavnica, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 3005776365859743582L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "prodavnica_id")
    private Long id;

    private String pib;

    private String maticniBroj;

    private String naziv;

    @javax.persistence.OneToMany(mappedBy = "prodavnica")
    private java.util.List<Roba> robe;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getPib() {
	return this.pib;
    }

    public void setPib(String pib) {
	this.pib = pib;
    }

    public String getMaticniBroj() {
	return this.maticniBroj;
    }

    public void setMaticniBroj(String maticniBroj) {
	this.maticniBroj = maticniBroj;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public java.util.List<Roba> getRobe() {
	return this.robe;
    }

    public void setRobe(java.util.List<Roba> robe) {
	this.robe = robe;
    }

    public Prodavnica() {
	super();
	this.robe = new java.util.ArrayList<>();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((maticniBroj == null) ? 0 : maticniBroj.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((pib == null) ? 0 : pib.hashCode());
	result = prime * result + ((robe == null) ? 0 : robe.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Prodavnica other = (Prodavnica) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (maticniBroj == null) {
	    if (other.maticniBroj != null)
		return false;
	} else if (!maticniBroj.equals(other.maticniBroj))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (pib == null) {
	    if (other.pib != null)
		return false;
	} else if (!pib.equals(other.pib))
	    return false;
	if (robe == null) {
	    if (other.robe != null)
		return false;
	} else if (!robe.equals(other.robe))
	    return false;
	return true;
    }

    @Override
    public ProdavnicaDTO getDTO() {
	ProdavnicaDTO prodavnicaDTO = new ProdavnicaDTO();
	prodavnicaDTO.setId(id);
	prodavnicaDTO.setMaticniBroj(maticniBroj);
	prodavnicaDTO.setNaziv(naziv);
	prodavnicaDTO.setPib(pib);

	ArrayList<RobaDTO> robeDTOs = new ArrayList<RobaDTO>();
	for (Roba roba : this.robe) {
	    robeDTOs.add(roba.getDTOinsideDTO());
	}
	prodavnicaDTO.setRobeDTO(robeDTOs);

	return prodavnicaDTO;
    }

    @Override
    public ProdavnicaDTO getDTOinsideDTO() {
	ProdavnicaDTO prodavnicaDTO = new ProdavnicaDTO();
	prodavnicaDTO.setId(id);
	prodavnicaDTO.setMaticniBroj(maticniBroj);
	prodavnicaDTO.setNaziv(naziv);
	prodavnicaDTO.setPib(pib);
	return prodavnicaDTO;
    }

    @Override
    public void update(Prodavnica entitet) {
	this.setMaticniBroj(entitet.getMaticniBroj());
	this.setNaziv(entitet.getNaziv());
	this.setPib(entitet.getPib());

    }

}