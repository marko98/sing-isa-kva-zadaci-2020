package isa.dto.Prodavnice;


import isa.dto.interfaces.DTO;

public class ProdavnicaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -4302336078688411524L;

    private Long id;

    private String pib;

    private String maticniBroj;

    private String naziv;

    private java.util.List<RobaDTO> robeDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getPib() {
	return this.pib;
    }

    public void setPib(String pib) {
	this.pib = pib;
    }

    public String getMaticniBroj() {
	return this.maticniBroj;
    }

    public void setMaticniBroj(String maticniBroj) {
	this.maticniBroj = maticniBroj;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public java.util.List<RobaDTO> getRobeDTO() {
	return this.robeDTO;
    }

    public void setRobeDTO(java.util.List<RobaDTO> robeDTO) {
	this.robeDTO = robeDTO;
    }

    public ProdavnicaDTO() {
	super();
	this.robeDTO = new java.util.ArrayList<>();
    }

}