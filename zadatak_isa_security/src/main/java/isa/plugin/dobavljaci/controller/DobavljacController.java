package isa.plugin.dobavljaci.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.controller.CrudController;
import isa.model.Dobavljaci.Dobavljac;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/dobavljac")
@CrossOrigin(origins = "*")

public class DobavljacController extends CrudController<Dobavljac, Long> {

}