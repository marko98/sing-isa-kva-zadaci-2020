import { OnInit, OnDestroy, Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// model
import { User, Role } from 'src/app/shared/model/user.model';

// service
import { UserService } from 'src/app/shared/service/user.service';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
    @Input() shouldCreate: boolean = false;
    @Input() user: User;
    @Output() onDone: EventEmitter<any> = new EventEmitter();
    public form: FormGroup;

    constructor(private userService: UserService){}

    onSubmit = (): void => {
        // console.log(this.form);

        if(this.form.valid){
            let user = new User();
            user.setFirstName(this.form.value.firstName);
            user.setLastName(this.form.value.lastName);
            user.setUsername(this.form.value.username);
            user.setPassword(this.form.value.password);

            let role = Role.KUPAC;
            if(this.form.value.role == 'administrator')
                role = Role.ADMINISTRATOR;

            user.setRole(role);

            this.userService.save(user);
            this.onDone.emit();
        }
        
    }

    // interfaces
    ngOnInit() {
        if(!this.shouldCreate) {
            this.form = new FormGroup({
                'firstName': new FormControl(this.user.getFirstName(), [Validators.required]),
                'lastName': new FormControl(this.user.getLastName(), [Validators.required]),
                'username': new FormControl(this.user.getUsername(), [Validators.required, Validators.minLength(6)]),
                'password': new FormControl(this.user.getPassword(), [Validators.required, Validators.minLength(6)]),
                'role': new FormControl(this.user.getStringRole(), [Validators.required])
            })
        } else {
            this.form = new FormGroup({
                'firstName': new FormControl('', [Validators.required]),
                'lastName': new FormControl('', [Validators.required]),
                'username': new FormControl('', [Validators.required, Validators.minLength(6)]),
                'password': new FormControl('', [Validators.required, Validators.minLength(6)]),
                'role': new FormControl('', [Validators.required])
            })
        }
        
        // console.log("UserComponent init");
    }

    ngOnDestroy() {
        // console.log("UserComponent destroyed");
    }

}