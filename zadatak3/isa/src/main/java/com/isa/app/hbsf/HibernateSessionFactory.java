package com.isa.app.hbsf;

import javax.annotation.PreDestroy;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.isa.app.model.Artikal;
import com.isa.app.model.Korisnik;
import com.isa.app.model.Korpa;
import com.isa.app.model.Racun;
import com.isa.app.model.Stavka;

@Component
@Scope("singleton")
public class HibernateSessionFactory {
	private SessionFactory sessionFactory;

	public HibernateSessionFactory() {
		super();
		this.sessionFactory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Artikal.class)
				.addAnnotatedClass(Korisnik.class)
				.addAnnotatedClass(Racun.class)
				.addAnnotatedClass(Korpa.class)
				.addAnnotatedClass(Stavka.class)
				.buildSessionFactory();
		
//		get session
		Session session = this.sessionFactory.getCurrentSession();
		
//		begin transaction
		session.beginTransaction();
		
//		-------------------- CREATE -------------------------------------------------------------------------
		
////		1)
//		Korisnik korisnik = new Korisnik("Marko", "Zahorodni", "marko98", Uloga.ADMINISTRATOR, "marko98");
//		
//		Racun racun = new Racun("321", 5000.00);
//		
//		korisnik.addRacun(racun);
//		
////		save racun
//		session.save(racun);
//		
////		save korisnik
//		session.save(korisnik);
		
////		2)
//		Artikal artikal = new Artikal("Voda", 50.00, "Mineralna voda");
//		
//		session.save(artikal);
		
////		3)
//		Korpa korpa = new Korpa(session.get(Korisnik.class, 1));
//		
//		session.save(korpa);
		
////		4)
//		Stavka stavka = new Stavka(session.get(Korpa.class, 1), session.get(Artikal.class, 1), 15);
//		
//		session.save(stavka);
		
////		5)
//		Racun racun = session.get(Racun.class, 1);
//		racun.addStavka(session.get(Stavka.class, 1));
//		
//		session.save(racun);
		
//		-------------------- READ ---------------------------------------------------------------------------
		
//		Korisnik theKorisnik = session.get(Korisnik.class, 1);
//		
//		System.out.println(theKorisnik.getRacuni().toString());
		
//		commit transaction
		session.getTransaction().commit();
		
//		close session
		session.close();
	}
	
	public Session getSession() {
		return this.sessionFactory.getCurrentSession();
	}
	
	@PreDestroy
	public void onDestroy() {
		System.out.println("HibernateSessionFactory bean destroyed");
		this.sessionFactory.close();
	}

}
