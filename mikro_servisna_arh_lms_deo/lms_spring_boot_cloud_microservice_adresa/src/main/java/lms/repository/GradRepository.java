package lms.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import lms.model.Grad;
import lms.repository.CrudRepository;

@Repository
@Scope("singleton")
public class GradRepository extends CrudRepository<Grad, Long> {
    
}