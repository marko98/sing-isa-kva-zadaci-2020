import {
  DataSource,
  CollectionViewer,
  ListRange,
} from '@angular/cdk/collections';
import { BehaviorSubject, Subscription, Observable, Subject } from 'rxjs';
import {
  PageableCrudService,
  Page,
  SortParam,
} from '../service/pageable-crud.service';
import { OptionItem } from './interface/option-item.interface';
import { Entitet } from './interface/entitet.interface';
import { ENTITET_MODEL_INTERFACE } from './interface/entitet.model-interface';

export class PageDataSource<
  T extends Entitet<T, ID>,
  ID,
  G extends ENTITET_MODEL_INTERFACE
> extends DataSource<OptionItem> {
  protected _cachedData: OptionItem[] = [];
  protected _fetchedPages = new Set<number>(); // skup brojeva koji predstavljaju brojeve dohvacenih stranica
  protected _dataStream = new BehaviorSubject<OptionItem[]>(this._cachedData);
  protected _sentRequests: number = 0;
  protected _subscription = new Subscription();

  private _hasDataToShowSubject: Subject<boolean> = new Subject();
  private _isLoadingSubject: Subject<boolean> = new Subject();
  private _hasFetchedDataSubject: Subject<boolean> = new Subject();
  private _totalItemsSubject: Subject<number> = new Subject();
  private _sortParams: SortParam[] = [];

  /**
   * @param pageableCrudService posto ce nam trebati service(tipa PageableCrudService) za dobavljanje
   * stranica zahtevamo ga kao argument u konstruktoru
   *
   * @param _pageSize koliko item-a zelimo da prikazemo po stranici
   */
  constructor(
    protected pageableCrudService: PageableCrudService<T, ID>,
    protected _pageSize = 10
  ) {
    super();
    // this._fetchPage(0);
  }

  connect(collectionViewer: CollectionViewer): Observable<OptionItem[]> {
    this._subscription.add(
      collectionViewer.viewChange.subscribe((range: ListRange) => {
        // console.log('Range: ', range);
        const startPage = this._getPageForIndex(range.start);
        // console.log('Start page: ', startPage);
        const endPage = this._getPageForIndex(range.end - 1);
        // console.log('End page: ', endPage);
        for (let pageNumber = startPage; pageNumber <= endPage; pageNumber++) {
          // console.log('pageNumber: ', pageNumber);
          this._fetchPage(pageNumber);
        }
      })
    );
    return this._dataStream;
  }

  disconnect(): void {
    // console.log(
    //   'disconnected, obrati paznju na *ngIf nad cdk-virtual-scroll-viewport diskonektovaces se, koristi hidden ili napisi svoju metodu za diskonektovanje'
    // );
    // this._subscription.unsubscribe();
  }

  myDisconnect(): void {
    this._subscription.unsubscribe();
  }

  protected _getPageForIndex(index: number): number {
    return Math.floor(index / this._pageSize);
  }

  protected _fetchPage(pageNumber: number, criteria?: string) {
    if (this._fetchedPages.has(pageNumber)) {
      return;
    }
    // console.log(
    //   'Fetching a next page, page number: ',
    //   pageNumber,
    //   ', criteria: ',
    //   criteria
    // );
    this._fetchedPages.add(pageNumber);

    this._sentRequests += 1;
    // console.log('zahtevi na cekanju: ', this._sentRequests);
    this._isLoadingSubject.next(this._sentRequests > 0);
    this.pageableCrudService
      .findPageWithPageNumberAndSizeAndSort(
        pageNumber,
        this._pageSize,
        this._sortParams,
        criteria
      )
      .subscribe(
        (page: Page<T, ID>) => {
          // console.log(page);
          // trenutno tu da bismo napravili loading
          setTimeout(() => {
            if (!(this._cachedData.length > 0)) {
              this._cachedData = Array.from<OptionItem>({
                length: page.totalElements,
              });

              this._hasFetchedDataSubject.next(true);
              this._hasDataToShowSubject.next(page.totalElements > 0);
              this._totalItemsSubject.next(page.totalElements);
            }

            if (page.content) {
              this._cachedData.splice(
                pageNumber * this._pageSize,
                this._pageSize,
                ...page.content.map((g: G) => {
                  return this.pageableCrudService.buildEntitet(g);
                })
              );
            }
            this._dataStream.next(this._cachedData);
            // console.log(this._cachedData);

            this._sentRequests -= 1;
            // console.log('zahtevi na cekanju: ', this._sentRequests);
            this._isLoadingSubject.next(this._sentRequests > 0);
          }, 200);
        },
        (err) => {
          this._hasFetchedDataSubject.next(true);

          console.log(err);

          this._sentRequests -= 1;
          // console.log('zahtevi na cekanju: ', this._sentRequests);
          this._isLoadingSubject.next(this._sentRequests > 0);
        },
        () => {
          // completed subscription
        }
      );
  }

  public isLoading = (): Subject<boolean> => {
    return this._isLoadingSubject;
  };

  public getHasDataToShowSubject = (): Subject<boolean> => {
    return this._hasDataToShowSubject;
  };

  public getHasFetchedDataSubject = (): Subject<boolean> => {
    return this._hasFetchedDataSubject;
  };

  public getTotalItemsSubject = (): Subject<number> => {
    return this._totalItemsSubject;
  };

  public changePath = (path: string): void => {
    this.pageableCrudService.setPath(path);
  };

  public changeCriteriaPath = (criteriaPath: string): void => {
    this.pageableCrudService.setCriteriaPath(criteriaPath);
  };

  public resetAndFetch = (
    criteria: string,
    sortParams?: SortParam[],
    pageSize?: number
  ): void => {
    if (pageSize) this._pageSize = pageSize;

    if (sortParams) this._sortParams = sortParams;

    this._cachedData.splice(0, this._cachedData.length);
    this._fetchedPages.clear(); // skup brojeva koji predstavljaju brojeve dohvacenih stranica
    this._dataStream.next(this._cachedData);

    this._hasFetchedDataSubject.next(false);

    // console.log(criteria, this._pageSize);
    this._fetchPage(0, criteria);
  };
}
