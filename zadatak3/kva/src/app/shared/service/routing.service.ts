import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

// repository
import { UserRepository } from '../repository/user.repository';

@Injectable()
export class RoutingService {

    constructor(private userRepository: UserRepository, private router: Router){
        this.userRepository.attach(this.userObserver);
    }

    // observer function
    userObserver = (observable: UserRepository): void => {
        // console.log("RoutingService notified about changes in UserRepository");
        // console.log(observable);        

        if(observable.getLoggedUser() && this.router.url === '/')
            this.router.navigate(['articles']);
        
        if(!observable.getLoggedUser())
            this.router.navigate(['/']);
    }
}