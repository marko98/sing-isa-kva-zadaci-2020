package isa.app.aspect;

import java.time.LocalDateTime;

import javax.jms.Queue;
import javax.jms.Topic;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import isa.app.dto.LogMessageDTO;
import isa.app.dto.LogMessageDTOType;

@Aspect
@Component
public class Aspects {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    @Qualifier("logErrorQueue")
    private Queue logErrorQueue;

    @Autowired
    @Qualifier("logWarningQueue")
    private Queue logWarningQueue;

    @Autowired
    @Qualifier("logInformationTopic")
    private Topic logInformationTopic;

    @Pointcut("within(isa.app.controller..*)")
    public void withinControllers() {
    }

    @Around("withinControllers() && execution(public * isa.app.controller.*.*(..))")
    public ResponseEntity<?> receivedRequest(ProceedingJoinPoint jp) {
	ResponseEntity<?> result = new ResponseEntity<String>("Something went wrong", HttpStatus.NOT_FOUND);

	System.out.println("\n---------------");
	System.out.println("request received");

	this.convertAndSendWarning("Metoda koja ce se izvrsiti: " + jp.getSignature());
	try {
	    result = (ResponseEntity<?>) jp.proceed();
	    System.out.println("Rezultat izvrsavanja: ");
	    System.out.println(result);
	    this.convertAndSendInformation("Metoda koja se izvrsila: " + jp.getSignature());
	} catch (Throwable e) {
	    this.convertAndSendError("Metoda zbog koje je doslo do greske: " + jp.getSignature());
	    e.printStackTrace();
	}
	System.out.println("---------------\n");

	return result;
    }

    public void convertAndSendWarning(String message) {
//	create warning
	LogMessageDTO warning = new LogMessageDTO();
	warning.setDatetime(LocalDateTime.now());
	warning.setLogMessageDTOType(LogMessageDTOType.WARNING);
	warning.setMessageTitle("Warning");
	warning.setMessageContent(message);
//	send warning
	jmsTemplate.convertAndSend(logWarningQueue, warning);
    }

    public void convertAndSendInformation(String message) {
//	create information
	LogMessageDTO information = new LogMessageDTO();
	information.setDatetime(LocalDateTime.now());
	information.setLogMessageDTOType(LogMessageDTOType.INFORMATION);
	information.setMessageTitle("Information");
	information.setMessageContent(message);
//	send information
	jmsTemplate.convertAndSend(logInformationTopic, information);
    }

    public void convertAndSendError(String message) {
//	create error
	LogMessageDTO error = new LogMessageDTO();
	error.setDatetime(LocalDateTime.now());
	error.setLogMessageDTOType(LogMessageDTOType.ERROR);
	error.setMessageTitle("Error");
	error.setMessageContent(message);
//	send error
	jmsTemplate.convertAndSend(logErrorQueue, error);
    }

}