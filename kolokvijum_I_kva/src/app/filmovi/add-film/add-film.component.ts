import { Component, OnInit } from '@angular/core';

import { Film } from '../filmovi.component';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MainService } from 'src/app/shared/service/main.service';

@Component({
  selector: 'app-add-film',
  templateUrl: './add-film.component.html',
  styleUrls: ['./add-film.component.css']
})
export class AddFilmComponent implements OnInit {

  constructor(private httpClient: HttpClient, private router: Router, private mainService: MainService) { }

  onSubmit = (form: NgForm): void => {
    console.log(form);
    if(form.valid) {

      let film = {
        naziv: form.value.naziv,
        reziser: form.value.reziser,
        ocena: form.value.ocena
      }

      // console.log(film);

      this.httpClient
        .post(
          "http://localhost:3000/film",
          film
        )
        .subscribe(
          result => {
            this.mainService.findAllFilm();
            this.router.navigate(['/filmovi']);
          },
          err => {

          }
        )
    }
  }

  ngOnInit(): void {
    
  }

}
