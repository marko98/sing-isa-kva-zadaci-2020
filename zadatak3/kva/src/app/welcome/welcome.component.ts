import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// service
import { AuthService } from '../shared/service/auth.service';
import { UIService } from '../shared/service/ui.service';

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.component.html',
    styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit, OnDestroy {
    public showPassword: boolean = false;

    constructor(private httpClient: HttpClient,
                private authService: AuthService,
                private uiService: UIService) {}

    onShowPassword = (): void => {
        this.showPassword = true;

        setInterval(() => {
            this.showPassword = false;
        }, 1500);
    }

    onSubmit = (form: NgForm): void => {
        if(form.valid) {
            // console.log(form);

            let login = {
                "korisnickoIme": form.value.username,
                "lozinka": form.value.password
            };

            this.httpClient.post(
                "http://localhost:8080/isa.zadatak.3/api/korisnici/login",
                JSON.stringify(login)
                )
                .subscribe(
                    (result: number) => {
                        this.authService.onLogin(result);
                    },
                    err => {
                        console.log(err);
                        // this.uiService.showSnackbar(err.error, null, 1500);
                    }
                );

        }
    }

    // interface
    ngOnInit() {
        // console.log("WelcomeComponent init");
    }

    ngOnDestroy() {
        // console.log("WelcomeComponent destroyed");
    }
}