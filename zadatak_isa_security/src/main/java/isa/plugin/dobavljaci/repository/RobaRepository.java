package isa.plugin.dobavljaci.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import isa.model.Dobavljaci.Roba;
import isa.repository.CrudRepository;

@Repository
@Scope("singleton")

public class RobaRepository extends CrudRepository<Roba, Long> {

}