import {
  Directive,
  Input,
  OnInit,
  AfterViewInit,
  ElementRef,
} from '@angular/core';

@Directive({
  selector: '[appWidth]',
})
export class WidthDirective implements AfterViewInit {
  /**
   * @width - u vw
   */
  @Input() width: number;

  constructor(private elRef: ElementRef) {}

  ngAfterViewInit(): void {
    this.elRef.nativeElement.style.width = this.width + 'vw';
  }
}
