import { Drzava } from '../entity/drzava.model';
import { ENTITET_MODEL_INTERFACE } from 'projects/autocomplete/src/public-api';

export declare interface GRAD_MODEL_INTERFACE extends ENTITET_MODEL_INTERFACE {
  drzavaDTO: Drzava;
  naziv: string;
}
