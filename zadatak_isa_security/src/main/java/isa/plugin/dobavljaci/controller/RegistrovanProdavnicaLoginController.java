//package isa.plugin.dobavljaci.controller;
//
//import java.util.HashMap;
//import java.util.HashSet;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.annotation.Secured;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import rs.ac.singidunum.banka.dto.UserDto;
//import rs.ac.singidunum.banka.model.User;
//import rs.ac.singidunum.banka.model.UserPermission;
//import rs.ac.singidunum.banka.repositories.PermissionRepository;
//import rs.ac.singidunum.banka.repositories.UserRepository;
//import rs.ac.singidunum.banka.services.UserService;
//import rs.ac.singidunum.banka.utils.TokenUtils;
//
//@Controller
//@RequestMapping("/api/")
//public class RegistrovanProdavnicaLoginController {
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    UserRepository ur;
//
//    @Autowired
//    PermissionRepository pr;
//
//    @Autowired
//    private AuthenticationManager authenticationManager;
//
//    @Autowired
//    private UserDetailsService userDetailsService;
//
//    @Autowired
//    private TokenUtils tokenUtils;
//
//    @Autowired
//    private PasswordEncoder passwordEncoder;
//
//    @RequestMapping(path = "/login", method = RequestMethod.POST)
//    public ResponseEntity<HashMap<String, String>> login(@RequestBody User user) {
//	try {
//	    /**
//	     * kreiramo specificniju autentifikaciju(UsernamePasswordAuthenticationToken,
//	     * koja implementira interfejs -> Authentication)
//	     */
//	    UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user.getUsername(),
//		    user.getPassword());
//
//	    /**
//	     * authenticationManager(AuthenticationManager) sluzi da autentifikuje neku
//	     * autentifikaciju koju mu mi prosledimo
//	     * 
//	     * vraca najopstiji oblik, Authentication, autentifikacije u koje sve
//	     * implementacije(konkretnije implmentacije interfejs-a Authentication) mogu da
//	     * se svrstaju
//	     */
//	    Authentication authentication = authenticationManager.authenticate(token);
//	    /**
//	     * SecurityContextHolder je vezan za thread, mi stoga dohvatamo njegov sadrzaj i
//	     * setujemo nasu autentifikaciju koju je vratio authenticationManager
//	     * 
//	     * SecurityContextHolder podesen je da radi na globalnom nivou zbog anotacije
//	     * (@EnableGlobalMethodSecurity(securedEnabled = true) u
//	     * rs.ac.singidunum.banka.security.SecurityConfiguration)
//	     */
//	    SecurityContextHolder.getContext().setAuthentication(authentication);
//
//	    /**
//	     * UserDetailsService je interfejs koji sluzi za dobavljanje podataka o user-u,
//	     * izgleda da ce ovde ipak biti injektovana nasa implementacija tog interfejsa
//	     * (rs.ac.singidunum.banka.services.UserDetailsServiceImpl)
//	     */
//	    UserDetails details = userDetailsService.loadUserByUsername(user.getUsername());
//	    String userToken = tokenUtils.generateToken(details);
//
//	    HashMap<String, String> data = new HashMap<String, String>();
//	    data.put("token", userToken);
//
//	    return new ResponseEntity<HashMap<String, String>>(data, HttpStatus.OK);
//
//	} catch (Exception e) {
//	    return new ResponseEntity<HashMap<String, String>>(HttpStatus.UNAUTHORIZED);
//	}
//    }
//
//    @RequestMapping(path = "/register", method = RequestMethod.POST)
//    public ResponseEntity<UserDto> register(@RequestBody User user) {
//	user.setPassword(passwordEncoder.encode(user.getPassword()));
//
//	user = ur.save(user);
//	user.setUserPermissions(new HashSet<UserPermission>());
//	user.getUserPermissions().add(new UserPermission(null, user, pr.findById(1l).get()));
//	ur.save(user);
//
//	return new ResponseEntity<UserDto>(new UserDto(user), HttpStatus.OK);
//    }
//
//    @RequestMapping("/test")
//    @Secured("ROLE_ADMIN")
//    public ResponseEntity<String> test() {
//	/**
//	 * mozemo pristupiti Authentication-u jer je on vezan pri samom ulasku request-a
//	 * na server pomocu naseg filtera koji smo definisali i setovali da se
//	 * koristi(metoda: rs.ac.singidunum.banka.security.SecurityConfiguration.authenticationTokenFilterBean()),
//	 * on je vezan za SecurityContextHolder.getContext(), dakle za njegov sadrzaj
//	 * 
//	 * SecurityContextHolder je vezan za izvrsavajuci thread
//	 */
//	Authentication a = SecurityContextHolder.getContext().getAuthentication();
//	return new ResponseEntity<String>("Test success!", HttpStatus.OK);
//    }
//}
