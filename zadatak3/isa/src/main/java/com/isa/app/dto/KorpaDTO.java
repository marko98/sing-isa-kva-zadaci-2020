package com.isa.app.dto;

import java.util.List;

import com.isa.app.model.Korisnik;
import com.isa.app.model.Stavka;

public class KorpaDTO extends DTO {
	private Integer id;
	private Korisnik vlasnik;
	private List<Stavka> stavke;

	public KorpaDTO() {
		super();
	}

	public KorpaDTO(int id, Korisnik vlasnik, List<Stavka> stavke) {
		super();
		this.id = id;
		this.vlasnik = vlasnik;
		this.stavke = stavke;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Korisnik getVlasnik() {
		return vlasnik;
	}

	public void setVlasnik(Korisnik vlasnik) {
		this.vlasnik = vlasnik;
	}

	public List<Stavka> getStavke() {
		return stavke;
	}

	public void setStavke(List<Stavka> stavke) {
		this.stavke = stavke;
	}

	@Override
	public String toString() {
		return "Korpa [id=" + id + "]";
	}
}
