package com.isa.app.dto;

import java.util.List;

import com.isa.app.model.Stavka;

public class ArtikalDTO extends DTO {
	
	private Integer id;
	private String naziv;
	private String opis;
	private Double cena;
	private List<Stavka> stavke;
	
	public ArtikalDTO() {
		super();
	}
	
	public ArtikalDTO(Integer id, String naziv, String opis, Double cena, List<Stavka> stavke) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.opis = opis;
		this.cena = cena;
		this.stavke = stavke;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public List<Stavka> getStavke() {
		return stavke;
	}

	public void setStavke(List<Stavka> stavke) {
		this.stavke = stavke;
	}

	@Override
	public String toString() {
		return "ArtikalDTO [id=" + id + ", naziv=" + naziv + ", opis=" + opis + ", cena=" + cena + ", stavke=" + stavke
				+ "]";
	}
	
	
}
