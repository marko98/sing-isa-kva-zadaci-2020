package com.isa.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.isa.app.dto.KorisnikDTO;

@Entity
@Table(name = "korisnik")
public class Korisnik implements ModelEntity<Korisnik> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "java.lang.Integer")
	@Column(name = "id")
	private Integer id;
	
	@Type(type = "string")
	@Column(name = "ime")
	private String ime;
	
	@Type(type = "string")
	@Column(name = "prezime")
	private String prezime;
	
	@Type(type = "string")
	@Column(name = "korisnicko_ime", unique = true)
	private String korisnickoIme;
	
	@Type(type = "string")
	@Column(name = "lozinka")
	private String lozinka;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "uloga")
	private Uloga uloga;
	
	@OneToMany(mappedBy = "vlasnik",
				fetch = FetchType.LAZY,
				cascade = {
						CascadeType.DETACH,
						CascadeType.MERGE,
						CascadeType.PERSIST,
						CascadeType.REFRESH
				})
	private List<Racun> racuni;
	
//	@Transient
	@Type(type = "java.lang.Boolean")
	@Column(name = "ulogovan")
	private boolean ulogovan = false;
	
	@OneToOne(mappedBy = "vlasnik",
				fetch = FetchType.EAGER,
				cascade = {
						CascadeType.DETACH,
						CascadeType.MERGE,
						CascadeType.PERSIST,
						CascadeType.REFRESH
				})
	private Korpa korpa;

	public Korisnik() {
		super();
	}

	public Korisnik(String ime, String prezime, String korisnickoIme, Uloga uloga, String lozinka) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.uloga = uloga;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public Uloga getUloga() {
		return uloga;
	}

	public void setUloga(Uloga uloga) {
		this.uloga = uloga;
	}

	public boolean isUlogovan() {
		return ulogovan;
	}

	public void setUlogovan(boolean ulogovan) {
		this.ulogovan = ulogovan;
	}
	
	public List<Racun> getRacuni() {
		return racuni;
	}

	public void setRacuni(List<Racun> racuni) {
		this.racuni = racuni;
	}

	public boolean addRacun(Racun racun) {
		if(this.racuni == null)
			this.racuni = new ArrayList<Racun>();
		
		this.racuni.add(racun);
		
		racun.setVlasnik(this);
		
		return true;
	}

	public Korpa getKorpa() {
		return korpa;
	}

	public void setKorpa(Korpa korpa) {
		this.korpa = korpa;
	}

	@Override
	public String toString() {
		return "Korisnik [id=" + id + ", ime=" + ime + ", prezime=" + prezime + ", korisnickoIme=" + korisnickoIme
				+ ", lozinka=" + lozinka + ", uloga=" + uloga + ", ulogovan=" + ulogovan + "]";
	}
	
//	------------------------ izgenerisi -----------------------------------
//	koristice se prilikom dodavanja u hashmapu npr.
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Korisnik other = (Korisnik) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
//	------------------------------------------------------------------------

	//	interface
	public Integer getIdentifikator() {
		return this.id;
	}

	public void update(Korisnik korisnik) {
		this.setIme(korisnik.getIme());
		this.setPrezime(korisnik.getPrezime());
		this.setUloga(korisnik.getUloga());
		this.setLozinka(korisnik.getLozinka());
		this.setUlogovan(korisnik.isUlogovan());
	}
	
	public KorisnikDTO getDTO() {
		KorisnikDTO kDTO = new KorisnikDTO();
		kDTO.setIme(this.ime);
		kDTO.setPrezime(this.prezime);
		kDTO.setKorisnickoIme(this.korisnickoIme);
		kDTO.setUloga(this.uloga);
		kDTO.setLozinka(this.lozinka);
		kDTO.setId(this.id);
		return kDTO;
	}

}
