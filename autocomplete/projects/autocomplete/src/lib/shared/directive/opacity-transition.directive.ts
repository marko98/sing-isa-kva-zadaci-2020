import { Directive, ElementRef, AfterViewInit, Input } from '@angular/core';
import * as gsap from 'gsap';

@Directive({
  selector: '[appOpacityTransition]',
})
export class OpacityTransitionDirective implements AfterViewInit {
  @Input() appOpacityDuration: number = 1;
  private _timeline = new gsap.TimelineMax();

  constructor(private _el: ElementRef) {}

  ngAfterViewInit(): void {
    this._timeline.fromTo(
      this._el.nativeElement,
      this.appOpacityDuration,
      { opacity: 0 },
      {
        opacity: 1,
        onComplete: () => {
          // console.log('animacija gotova :)');
        },
      }
    );
  }
}
