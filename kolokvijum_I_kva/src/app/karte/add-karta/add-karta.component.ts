import { Component, OnInit, OnDestroy } from '@angular/core';
import { Projekcija } from 'src/app/projekcije/projekcije.component';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MainService } from 'src/app/shared/service/main.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-karta',
  templateUrl: './add-karta.component.html',
  styleUrls: ['./add-karta.component.css']
})
export class AddKartaComponent implements OnInit, OnDestroy {
  public projekcije: Projekcija[] = [];

  constructor(private httpClient: HttpClient, private router: Router, private mainService: MainService) { }

  findAllProjekcije = (): void => {
    this.httpClient
        .get(
            "http://localhost:3000/projekcija"
        )
        .subscribe(
            (projekcije: Projekcija[]) => {
            projekcije.forEach((projekcija: Projekcija) => {
                //   console.log(projekcija);
                });

                this.projekcije = projekcije;
            },
            err => {
                console.log(err);
            }
        );
    }

  onSubmit = (form: NgForm): void => {
    console.log(form);
    if(form.valid) {

      let karta = {
        projekcijaId: form.value.projekcijaId,
        cena: form.value.cena
      }

      this.httpClient
        .post(
          "http://localhost:3000/karta",
          karta
        )
        .subscribe(
          result => {
            this.mainService.findAllKarte();
            this.router.navigate(['/karte']);
          },
          err => {

          }
        )
    }
  }

  ngOnInit(): void {
    this.findAllProjekcije();
  }

  ngOnDestroy(): void {
    
  }

}
