export declare interface ClassInterface {
  name: string;
  attributes: AttributeInterface[];
  operations: OperationInterface[];
}

export declare interface AttributeInterface {
  visibility: VISIBILITY;
  name: string;
  dataType: string;
}

export declare interface OperationInterface {
  visibility: VISIBILITY;
  name: string;
  returnDataType: string;
  arguments: ArgumentInterface[];
}

export declare interface ArgumentInterface {
  name: string;
  dataType: string;
}

export enum VISIBILITY {
  PUBLIC,
  PRIVATE,
  PROTECTED,
}

export const CLASS_DIAGRAM_DATA: ClassInterface[] = [
  {
    name: "House",
    attributes: [
      {
        visibility: VISIBILITY.PUBLIC,
        name: "numOfRooms",
        dataType: "Integer",
      },
      {
        visibility: VISIBILITY.PRIVATE,
        name: "numOfSquareMeters",
        dataType: "Integer",
      },
    ],
    operations: [
      {
        visibility: VISIBILITY.PUBLIC,
        name: "House",
        returnDataType: "House",
        arguments: [
          {
            name: "numOfRooms",
            dataType: "Integer",
          },
          {
            name: "numOfSquareMeters",
            dataType: "Integer",
          },
        ],
      },
      {
        visibility: VISIBILITY.PRIVATE,
        name: "build",
        returnDataType: "void",
        arguments: [],
      },
    ],
  },
  {
    name: "Class",
    attributes: [
      {
        visibility: VISIBILITY.PUBLIC,
        name: "atr1",
        dataType: "Integer",
      },
      {
        visibility: VISIBILITY.PRIVATE,
        name: "atr2",
        dataType: "Integer",
      },
    ],
    operations: [
      {
        visibility: VISIBILITY.PUBLIC,
        name: "Class",
        returnDataType: "Class",
        arguments: [
          {
            name: "atr1",
            dataType: "Integer",
          },
          {
            name: "atr2",
            dataType: "Integer",
          },
        ],
      },
    ],
  },
  {
    name: "Class3",
    attributes: [
      {
        visibility: VISIBILITY.PROTECTED,
        name: "atr3",
        dataType: "Integer",
      },
      {
        visibility: VISIBILITY.PRIVATE,
        name: "atr3",
        dataType: "Integer",
      },
    ],
    operations: [],
  },
];
