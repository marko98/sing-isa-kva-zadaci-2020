// interface
import { SortParam, PageKey, Page } from '../pageable-crud.service';
import { Observable as CustomObservable } from '../../pattern/behavioural/observer/observable.model';
import { Observable } from 'rxjs';
import { Entitet } from '../../model/interface/entitet.interface';

export declare interface PageableCrudServiceInterface<
  T extends Entitet<T, ID>,
  ID
> extends CustomObservable {
  findPageWithPageNumber(page: number): void;
  findPageWithPageNumberAndSize(
    page: number,
    pageSize: number
  ): Observable<Page<T, ID>>;

  findPageWithPageNumberAndSizeAndSort(
    page: number,
    pageSize: number,
    sortParams: SortParam[]
  ): void;
  getPages(): Map<PageKey, Page<T, ID>>;
  getPageSize(): number;
  setPageSize(pageSize: number);
}
