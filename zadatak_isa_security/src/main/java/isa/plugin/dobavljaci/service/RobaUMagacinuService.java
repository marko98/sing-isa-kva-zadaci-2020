package isa.plugin.dobavljaci.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import isa.model.Dobavljaci.RobaUMagacinu;
import isa.service.CrudService;

@Service
@Scope("singleton")

public class RobaUMagacinuService extends CrudService<RobaUMagacinu, Long> {

}