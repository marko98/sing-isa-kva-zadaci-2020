// interface
import { Entity } from './interface/entity.interface';
import { Prototype } from './patterns/creational/prototype/prototype.interface';
        
export declare interface ArticleDTO {
    "naziv": string,
    "opis": string,
    "cena": number,
    "id": string
}

export class Article implements Entity<Article>, Prototype<Article> {
    private name: string;
    private description: string;
    private identificator: string;
    private price: number;
    
    constructor(){}

    getName = (): string => {
        return this.name;
    }

    setName = (name: string): void => {
        this.name = name;
    }

    getDescription = (): string => {
        return this.description;
    }

    setDescription = (description: string): void => {
        this.description = description;
    }

    setIdentificator = (identificator: string): void => {
        this.identificator = identificator;
    }

    getPrice = (): number => {
        return this.price;
    }

    setPrice = (price: number): void => {
        this.price = price;
    }

    // interface
    getIdentificator = (): string => {
        return this.identificator;
    }

    update = (article: Article): void => {
        this.setName(article.getName());
        this.setDescription(article.getDescription());
        this.setPrice(article.getPrice());
    }

    fromDict = (articleDTO: ArticleDTO): void => {
        this.setName(articleDTO.naziv);
        this.setDescription(articleDTO.opis);
        this.setPrice(articleDTO.cena);
        this.setIdentificator(articleDTO.id);
    };

    toDict = (): ArticleDTO => {            
        return {
            "naziv": this.getName(),
            "opis": this.getDescription(),
            "cena": this.getPrice(),
            "id": this.getIdentificator()
        };
    }

    // interface prototype
    clone = (): Article => {
        let clone = new Article();
        clone.setName(this.getName());
        clone.setDescription(this.getDescription());
        clone.setIdentificator(this.getIdentificator());
        clone.setPrice(this.getPrice());
        return clone;
    }

    prototype = (article: Article): void => {
        article.setName(this.getName());
        article.setDescription(this.getDescription());
        article.setIdentificator(this.getIdentificator());
        article.setPrice(this.getPrice());
    }
}