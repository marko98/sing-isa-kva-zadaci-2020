import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class UIService {
    constructor(private matSnackBar: MatSnackBar){}

    showSnackbar = (message: string, action: string, duration: number) => {
        this.matSnackBar.open(message, action, {
            duration: duration
        });
    }
}