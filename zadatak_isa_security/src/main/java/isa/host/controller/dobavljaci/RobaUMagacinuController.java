package isa.host.controller.dobavljaci;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.host.controller.HostCrudController;
import isa.model.Dobavljaci.RobaUMagacinu;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/roba_u_magacinu")
@CrossOrigin(origins = "*")

public class RobaUMagacinuController extends HostCrudController<RobaUMagacinu, Long> {

    public RobaUMagacinuController() {
	this.category = "roba_u_magacinu";
    }

}