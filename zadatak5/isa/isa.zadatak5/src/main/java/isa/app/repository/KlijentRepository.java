package isa.app.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import isa.app.model.Klijent;

@Repository
@Scope("singleton")
public class KlijentRepository extends CrudRepository<Klijent, Long> {

}