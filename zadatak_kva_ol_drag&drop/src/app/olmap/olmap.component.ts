import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
} from '@angular/core';

import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import { View, Map, Feature } from 'ol';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import Point from 'ol/geom/Point';
import { fromLonLat } from 'ol/proj';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';

declare interface DropItem {
  naziv: string;
  geom: number[];
}

@Component({
  selector: 'app-olmap',
  templateUrl: './olmap.component.html',
  styleUrls: ['./olmap.component.css'],
})
export class OlmapComponent implements OnInit, AfterViewInit {
  @ViewChild('mapDiv', { static: false }) mapDiv: ElementRef;
  public gradovi: DropItem[] = [
    {
      naziv: 'Singidunum - Novi Sad',
      geom: [19.8443557, 45.2530641],
    },
    {
      naziv: 'Singidunum - Beograd',
      geom: [20.4838907, 44.7681199],
    },
  ];

  public oznaceniZaMapu = [];

  public dodatiNaMapi = [];

  public map;
  public raster;
  public vectorSource: VectorSource;
  public vector: VectorLayer;
  public view;

  constructor() {}

  dropOnMap(event: CdkDragDrop<string[]>) {
    // console.log(event);
    if (event.previousContainer !== event.container) {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // console.log(event.container.data[event.currentIndex]);
      let dropedItem: DropItem = <DropItem>(
        (<any>event.container.data[event.currentIndex])
      );

      this.vectorSource.addFeature(
        new Feature({
          geometry: new Point(fromLonLat(dropedItem.geom)),
        })
      );
    }
    this.consoleLogLists();
  }

  consoleLogLists(): void {
    console.log(this.gradovi);
    console.log(this.oznaceniZaMapu);
    console.log(this.dodatiNaMapi);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
    this.consoleLogLists();
  }

  ngOnInit(): void {
    this.raster = new TileLayer({
      source: new OSM(),
    });

    this.vectorSource = new VectorSource({
      wrapX: false,
    });

    // this.vectorSource.addFeature(
    //   new Feature({
    //     geometry: new Point(fromLonLat([19.8443557, 45.2530641])),
    //   })
    // );

    this.vector = new VectorLayer({
      source: this.vectorSource,
    });

    this.view = new View({
      center: fromLonLat([19.8443557, 45.2530641]),
      zoom: 18,
    });
  }

  ngAfterViewInit(): void {
    this.map = new Map({
      target: this.mapDiv.nativeElement,
      layers: [this.raster, this.vector],
      view: this.view,
    });
  }
}
