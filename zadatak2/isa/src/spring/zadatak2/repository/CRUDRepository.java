package spring.zadatak2.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import spring.zadatak2.model.Entity;

public abstract class CRUDRepository<T extends Entity<T>> {
	protected ArrayList<T> models;
	
//	Read
	public List<T> findAll() {
		return this.models;
	}
	
//	Read
	public T findOne(String identifikator) {
		for (T model : models) {
			if(model.getIdentifikator().equals(identifikator)) {
				return model;
			}
		}
		
		return null;
	}
	
//	Create and Update
	public void save(T model) {
		T t = this.findOne(model.getIdentifikator());
		
		if(t == null) {
			this.models.add(model);
		} else {
			t.update(model);
		}
	}
	
//	Delete
	public void delete(String identifikator) {
		Iterator<T> iterator = this.models.iterator();
		while (iterator.hasNext()) {
			T t = iterator.next();
			if(t.getIdentifikator().equals(identifikator)) {
				iterator.remove();
				return;
			}
		}
	}
	
//	Delete
	public void delete(T model) {
		this.delete(model.getIdentifikator());
	}
}
