package isa.host.controller.prodavnice;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.host.controller.HostCrudController;
import isa.model.Prodavnice.Prodavnica;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/prodavnica")
@CrossOrigin(origins = "*")

public class ProdavnicaController extends HostCrudController<Prodavnica, Long> {

    public ProdavnicaController() {
	this.category = "prodavnica";
    }

}