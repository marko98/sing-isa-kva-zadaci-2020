package isa.app.dto;

import java.time.LocalDateTime;

public class KreditKlijentDTO extends DTO {

    /**
     * 
     */
    private static final long serialVersionUID = -1589452796645164154L;

    private Long id;

    private LocalDateTime datum;

    private KreditDTO kreditDTO;

    private KlijentDTO klijentDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getDatum() {
	return this.datum;
    }

    public void setDatum(LocalDateTime datum) {
	this.datum = datum;
    }

    public KreditDTO getKreditDTO() {
	return this.kreditDTO;
    }

    public void setKreditDTO(KreditDTO kreditDTO) {
	this.kreditDTO = kreditDTO;
    }

    public KlijentDTO getKlijentDTO() {
	return this.klijentDTO;
    }

    public void setKlijentDTO(KlijentDTO klijentDTO) {
	this.klijentDTO = klijentDTO;
    }

    public KreditKlijentDTO() {
	super();
    }

}