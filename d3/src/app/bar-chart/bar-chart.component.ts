import { Component, OnInit, Input, ElementRef } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {
  @Input("data")
  data = []

  private svg;
  private bars;
  private width = 500;
  private height = 500;

  constructor(private element: ElementRef) { }

  ngOnInit(): void {
    let x = d3.scaleBand().domain(d3.range(this.data.length).map(d=>d.toString())).range([25, this.width]).padding(0.5);
    let y = d3.scaleLinear().domain([0, d3.max(this.data, d => d.y)+50]).range([this.height-50, 0]);

    this.svg = d3.select(this.element.nativeElement).append("svg");
    this.svg.attr("height", this.height);
    this.svg.attr("width", this.width);
    this.bars = this.svg.append("g");
    this.bars.selectAll("rect").data(this.data).join("rect")
    .attr("x", (d, i) => x(i))
    .attr("y", d => y(d.y))
    .attr("height", d => y(0)-y(d.y))
    .attr("width", x.bandwidth())
    console.log(y(0));

    let xAxis = g => g.call(d3.axisBottom(x).tickFormat(i => this.data[i].x).tickSizeOuter(0));
    let yAxis = g => g.call(d3.axisLeft(y));
    this.svg.append("g").attr("transform", "translate(0, 450)").call(xAxis);
    this.svg.append("g").attr("transform", "translate(25, 0)").call(yAxis);
  }

}
