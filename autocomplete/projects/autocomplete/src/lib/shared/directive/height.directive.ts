import { Directive, ElementRef, Input, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appHeight]',
})
export class HeightDirective implements AfterViewInit {
  /**
   * @height - u px
   */
  @Input() height: number;

  constructor(private elRef: ElementRef) {}

  ngAfterViewInit(): void {
    this.elRef.nativeElement.style.height = this.height + 'px';
  }
}
