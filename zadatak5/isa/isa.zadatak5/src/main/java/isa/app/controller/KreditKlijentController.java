package isa.app.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.app.model.KreditKlijent;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/kredit_klijent")
@CrossOrigin(origins = "*")
public class KreditKlijentController extends CrudController<KreditKlijent, Long> {

}