package isa.app.model;

import java.util.ArrayList;
import java.util.List;

import isa.app.dto.BankaDTO;
import isa.app.dto.KlijentDTO;
import isa.app.dto.KreditDTO;

@javax.persistence.Entity
public class Banka implements Entity<Banka, Long> {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Long id;

    private String naziv;

    @javax.persistence.OneToMany(mappedBy = "banka")
    private java.util.List<Kredit> krediti;

    @javax.persistence.OneToMany(mappedBy = "banka")
    private java.util.List<Klijent> klijenti;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public java.util.List<Kredit> getKrediti() {
	return this.krediti;
    }

    public void setKrediti(java.util.List<Kredit> krediti) {
	this.krediti = krediti;
    }

    public java.util.List<Klijent> getKlijenti() {
	return this.klijenti;
    }

    public void setKlijenti(java.util.List<Klijent> klijenti) {
	this.klijenti = klijenti;
    }

    public Banka() {
	super();
	this.krediti = new ArrayList<Kredit>();
	this.klijenti = new ArrayList<Klijent>();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((klijenti == null) ? 0 : klijenti.hashCode());
	result = prime * result + ((krediti == null) ? 0 : krediti.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Banka other = (Banka) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (klijenti == null) {
	    if (other.klijenti != null)
		return false;
	} else if (!klijenti.equals(other.klijenti))
	    return false;
	if (krediti == null) {
	    if (other.krediti != null)
		return false;
	} else if (!krediti.equals(other.krediti))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	return true;
    }

    @Override
    public BankaDTO getDTO() {
	/*
	 * BITNO!!!
	 * 
	 * za slozene tipove koji su opcioni za entitet, a nisu liste, pre poziva bilo
	 * kakvih metoda nad njima(npr. getDTOinsideDTO()) proveriti da li su razliciti
	 * od null
	 */

	BankaDTO bankaDTO = new BankaDTO();
	bankaDTO.setId(id);
	bankaDTO.setNaziv(naziv);

	List<KlijentDTO> klijentiDTO = new ArrayList<KlijentDTO>();
	for (Klijent klijent : klijenti) {
	    klijentiDTO.add(klijent.getDTOinsideDTO());
	}
	bankaDTO.setKlijentiDTO(klijentiDTO);

	List<KreditDTO> kreditiDTO = new ArrayList<KreditDTO>();
	for (Kredit kredit : krediti) {
	    kreditiDTO.add(kredit.getDTOinsideDTO());
	}
	bankaDTO.setKreditiDTO(kreditiDTO);

	return bankaDTO;
    }

    @Override
    public BankaDTO getDTOinsideDTO() {
	BankaDTO bankaDTO = new BankaDTO();
	bankaDTO.setId(id);
	bankaDTO.setNaziv(naziv);

	return bankaDTO;
    }

    @Override
    public void update(Banka entity) {
	/*
	 * BITNO!!!
	 * 
	 * kod polja koja su liste imamo problem -> recimo da zelimo da update-ujemo
	 * banku, ali samo njen naziv mi onda moramo da dostavimo i sve klijente i sve
	 * kredite, ako sa druge strane izbacimo iz ove metode setovanje polja koje su
	 * liste, onda mozemo bez problema update-ovati samo naziv banke ili jos neka
	 * polja ako ih bude imala bez da moramo da dovlacimo sve klijente i kredite
	 * 
	 * ako zelimo da iz banke izbacimo neki krediti onda buduci da banka ima vise
	 * kredita, a kredit ima jednu banku, update-ujemo taj kredit odnosno kod njega
	 * za polje banka stavljamo null ili ga u potpunosti brisemo isto vazi i za
	 * klijene, medjutim da bi to moglo tako da radi ne smemo imati vezu izmedju dve
	 * klase vise na prema vise, vec ubaciti izmedju klasu koja ce ih 'spajati', ta
	 * dodatna klasa bi trebala da ima kardinalitete prema ove dve klase 1 na prema
	 * vise ili slicno, ali nikako vise na prema vise
	 * 
	 * dakle: kod update-a ne update-ujemo liste SLOZENIH tipova(entiteta), ako ima
	 * potrebe za izbacivanjem nekih ili dodavanjem nekih u listu obracamo se ili
	 * njima(slucaj jedan na prema vise) ili dodatnoj klasi koja je spoj izmedju
	 * entiteta koji 'imaju' kardinalitet vise na prema vise
	 */

	this.setNaziv(entity.getNaziv());
    }

}