import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  Input,
  ElementRef,
} from "@angular/core";
import {
  ClassInterface,
  ArgumentInterface,
  VISIBILITY,
} from "../shared/model/patterns/structural/composite/class/interfaces";
import * as d3 from "d3";

@Component({
  selector: "app-class",
  templateUrl: "./class.component.html",
  styleUrls: ["./class.component.css"],
})
export class ClassComponent implements OnInit, OnChanges {
  @Input() data: ClassInterface[];

  private svg: d3.Selection<any, unknown, null, undefined>;
  private height = 600;
  private width = 800;
  private marginBetweenMainElements = 10;

  constructor(private elementRef: ElementRef) {}

  createClass = (): void => {
    // https://developer.mozilla.org/en-US/docs/Web/SVG/Element

    this.svg = d3
      .select(this.elementRef.nativeElement)
      .append("svg")
      .attr("height", this.height)
      .attr("width", this.width);

    // ------------------------- create g for class -------------------------
    let classG = this.svg.selectAll("g").data(this.data).join("g");

    // ------------------------- create rect for class -------------------------
    let classRect = classG.append("rect").attr("class", "class-rect");

    // ------------------------- create text for class name -------------------------
    let classNameText = classG
      .append("text")
      .attr("alignment-baseline", "hanging")
      .text((d) => d.name)
      .attr("x", 0)
      .attr("y", this.marginBetweenMainElements);

    // let classNameTextWHXY = classNameText.node().getBoundingClientRect();
    // console.log(classNameText.node().getBoundingClientRect().y);

    // ------------------------- create path(line) after text for class -------------------------
    let firstPathLine = classG.append("path");

    // ------------------------- create g for attributes -------------------------
    let attrG = classG
      .append("g")
      .attr(
        "transform",
        (d, i) =>
          `translate(${0} ${Math.ceil(
            classNameText.nodes()[i].getBoundingClientRect().y +
              classNameText.nodes()[i].getBoundingClientRect().height +
              this.marginBetweenMainElements
          )})`
      );

    // create text for every attribute inside g for attributes
    attrG
      .selectAll("text")
      .data((d) => d.attributes)
      .join("text")
      .text((d, i) => {
        let visibility;
        if (d.visibility == VISIBILITY.PUBLIC) visibility = "+";
        else if (d.visibility == VISIBILITY.PRIVATE) visibility = "-";
        else if (d.visibility == VISIBILITY.PROTECTED) visibility = "#";
        return "    " + visibility + " " + d.name + ": " + d.dataType + "    ";
      })
      .attr("xml:space", "preserve")
      .attr("alignment-baseline", "hanging")
      .attr("y", (d, i) => i * 20);

    // let attrGWHXY = attrG.node().getBoundingClientRect();

    // console.log(classG.node().getBoundingClientRect().x);
    // console.log(attrG.node().getBoundingClientRect().x);

    // ------------------------- create path(line) after g for attributes -------------------------
    let secondPathLine = classG.append("path");

    // ------------------------- create g for operations -------------------------
    let operationsG = classG
      .append("g")
      .attr(
        "transform",
        (d, i) =>
          `translate(${0} ${
            attrG.nodes()[i].getBoundingClientRect().y +
            attrG.nodes()[i].getBoundingClientRect().height +
            this.marginBetweenMainElements
          })`
      );

    // create text for every operation inside g for operations
    operationsG
      .selectAll("text")
      .append("text")
      .data((d) => d.operations)
      .join("text")
      .text((d, i) => {
        let visibility;
        if (d.visibility == VISIBILITY.PUBLIC) visibility = "+";
        else if (d.visibility == VISIBILITY.PRIVATE) visibility = "-";
        else if (d.visibility == VISIBILITY.PROTECTED) visibility = "#";

        let argsText = "";
        for (let i = 0; i < d.arguments.length; i++) {
          argsText += d.arguments[i].name + ": " + d.arguments[i].dataType;
          if (i != d.arguments.length - 1) {
            argsText += ", ";
          }
        }

        return (
          "    " +
          visibility +
          " " +
          d.name +
          "(" +
          argsText +
          "): " +
          d.returnDataType +
          "    "
        );
      })
      .attr("xml:space", "preserve")
      .attr("alignment-baseline", "hanging")
      .attr("y", (d, i) => i * 20);

    // let operationsGWHXY = operationsG.node().getBoundingClientRect();

    // // ------------------------- check what width and height should our rect be -------------------------
    // let rectWidth = attrG.nodes()[i].getBoundingClientRect().width;

    // if (operationsGWHXY.width > rectWidth) rectWidth = operationsGWHXY.width;

    // rectWidth = Math.ceil(rectWidth);

    // console.log(rectWidth);

    // let classGWHXY = (<any>classG.node()).getBoundingClientRect();

    // ------------------------- update attr for firstPathLine -------------------------
    firstPathLine
      .attr(
        "d",
        (d, i) =>
          `M ${0} ${
            classNameText.nodes()[i].getBoundingClientRect().y +
            classNameText.nodes()[i].getBoundingClientRect().height
          } L ${(<any>classG.nodes()[i]).getBoundingClientRect().width} ${
            classNameText.nodes()[i].getBoundingClientRect().y +
            classNameText.nodes()[i].getBoundingClientRect().height
          }`
      )
      .attr("stroke", "black");

    // ------------------------- update attr for secondPathLine -------------------------
    secondPathLine
      .attr(
        "d",
        (d, i) =>
          `M ${0} ${
            attrG.nodes()[i].getBoundingClientRect().y +
            attrG.nodes()[i].getBoundingClientRect().height
          } L ${(<any>classG.nodes()[i]).getBoundingClientRect().width} ${
            attrG.nodes()[i].getBoundingClientRect().y +
            attrG.nodes()[i].getBoundingClientRect().height
          }`
      )
      .attr("stroke", "black");

    // ------------------------- set text(class name) in the middle -------------------------
    classNameText.attr("x", (d, i) => {
      let x =
        (<any>classG.nodes()[i]).getBoundingClientRect().width / 2 -
        classNameText.nodes()[i].getBoundingClientRect().width / 2;
      return x;
    });

    // ------------------------- correct width and height for rect for class -------------------------
    classRect
      .attr(
        "width",
        (d, i) => (<any>classG.nodes()[i]).getBoundingClientRect().width
      )
      .attr(
        "height",
        (d, i) =>
          classNameText.nodes()[i].getBoundingClientRect().height +
          attrG.nodes()[i].getBoundingClientRect().height +
          operationsG.nodes()[i].getBoundingClientRect().height +
          5 * this.marginBetweenMainElements
      );

    classG.attr("transform", (d, i) => {
      let y = 0;

      for (let j = 0; j < i; j++) {
        // console.log((<any>classG.nodes()[i]).getBoundingClientRect().height);

        y += (<any>classG.nodes()[i]).getBoundingClientRect().height + 50;
      }

      return `translate(20 ${y})`;
    });
  };

  ngOnInit(): void {
    console.log("ClassComponent init");
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.data) {
      // console.log(changes.data);
      this.createClass();
    }
  }
}
