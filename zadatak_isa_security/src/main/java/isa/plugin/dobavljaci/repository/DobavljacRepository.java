package isa.plugin.dobavljaci.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import isa.model.Dobavljaci.Dobavljac;
import isa.repository.CrudRepository;

@Repository
@Scope("singleton")

public class DobavljacRepository extends CrudRepository<Dobavljac, Long> {

}