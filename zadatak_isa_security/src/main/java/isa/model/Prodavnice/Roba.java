package isa.model.Prodavnice;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import isa.dto.Prodavnice.RobaDTO;
import isa.model.interfaces.Entitet;

@javax.persistence.Entity

@Table(name = "roba")
public class Roba implements Entitet<Roba, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 4434319610814403185L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "roba_id")
    private Long id;

    private String naziv;

    private Double kolicina;

    private Double cena;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "prodavnica_id")
    private Prodavnica prodavnica;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public Double getKolicina() {
	return this.kolicina;
    }

    public void setKolicina(Double kolicina) {
	this.kolicina = kolicina;
    }

    public Double getCena() {
	return this.cena;
    }

    public void setCena(Double cena) {
	this.cena = cena;
    }

    public Prodavnica getProdavnica() {
	return this.prodavnica;
    }

    public void setProdavnica(Prodavnica prodavnica) {
	this.prodavnica = prodavnica;
    }

    public Roba() {
	super();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((cena == null) ? 0 : cena.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((kolicina == null) ? 0 : kolicina.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((prodavnica == null) ? 0 : prodavnica.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Roba other = (Roba) obj;
	if (cena == null) {
	    if (other.cena != null)
		return false;
	} else if (!cena.equals(other.cena))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (kolicina == null) {
	    if (other.kolicina != null)
		return false;
	} else if (!kolicina.equals(other.kolicina))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (prodavnica == null) {
	    if (other.prodavnica != null)
		return false;
	} else if (!prodavnica.equals(other.prodavnica))
	    return false;
	return true;
    }

    @Override
    public RobaDTO getDTO() {
	RobaDTO robaDTO = new RobaDTO();
	robaDTO.setId(id);
	robaDTO.setCena(cena);
	robaDTO.setKolicina(kolicina);
	robaDTO.setNaziv(naziv);

	if (this.prodavnica != null)
	    robaDTO.setProdavnicaDTO(this.prodavnica.getDTOinsideDTO());

	return robaDTO;
    }

    @Override
    public RobaDTO getDTOinsideDTO() {
	RobaDTO robaDTO = new RobaDTO();
	robaDTO.setId(id);
	robaDTO.setCena(cena);
	robaDTO.setKolicina(kolicina);
	robaDTO.setNaziv(naziv);
	return robaDTO;
    }

    @Override
    public void update(Roba entitet) {
	this.setCena(entitet.getCena());
	this.setKolicina(entitet.getKolicina());
	this.setNaziv(entitet.getNaziv());

	this.setProdavnica(entitet.getProdavnica());

    }

}