package isa.app.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import isa.app.model.Kredit;

@Repository
@Scope("singleton")
public class KreditRepository extends CrudRepository<Kredit, Long> {

}