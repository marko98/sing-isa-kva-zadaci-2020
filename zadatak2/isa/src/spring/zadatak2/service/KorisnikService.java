package spring.zadatak2.service;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import spring.zadatak2.model.Korisnik;
import spring.zadatak2.model.Uloga;
import spring.zadatak2.repository.KorisnikRepository;

@Service
@Scope("singleton")
public class KorisnikService extends CRUDService<Korisnik> {
	
	public List<Korisnik> findByRole(Uloga uloga) {
		return ((KorisnikRepository) this.cRUDRepository).findByRole(uloga);
	}
	
	public boolean login(Korisnik korisnik, String lozinka) {
		if (korisnik.getLozinka().equals(lozinka)) {
			korisnik.setUlogovan(true);
			return true;
		}
		return false;
	}
	
	public void logout(Korisnik korisnik) {
		korisnik.setUlogovan(false);
	}
	
	public boolean isAuth(String identifikator) {
		Korisnik k = this.cRUDRepository.findOne(identifikator);
		if(k == null)
			return false;
		
		if(k.isUlogovan())
			return true;
		
		return false;
	}
	
}
