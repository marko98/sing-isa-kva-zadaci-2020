import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

// service
import { UserService } from '../service/user.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private userService: UserService){}

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Observable<UrlTree> | Promise<boolean> | Promise<UrlTree> | boolean | UrlTree {
        if(this.userService.getLoggedUser())
            return true;
        return this.router.createUrlTree(['']);
    }
}
