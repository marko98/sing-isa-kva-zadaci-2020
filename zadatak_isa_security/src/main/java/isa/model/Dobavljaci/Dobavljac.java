package isa.model.Dobavljaci;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Table;

import isa.dto.Dobavljaci.DobavljacDTO;
import isa.dto.Dobavljaci.RegistrovanaProdavnicaDTO;
import isa.dto.Dobavljaci.RobaUMagacinuDTO;
import isa.model.interfaces.Entitet;

@javax.persistence.Entity

@Table(name = "dobavljac")
public class Dobavljac implements Entitet<Dobavljac, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -6369789938499163041L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "dobavljac_id")
    private Long id;
        
    private String naziv;
        
    @javax.persistence.OneToMany(mappedBy="dobavljac")
    private java.util.List<RegistrovanaProdavnica> registrovaneProdavnice;
        
    @javax.persistence.OneToMany(mappedBy="dobavljac")
    private java.util.List<RobaUMagacinu> robeUMagacinu;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public java.util.List<RegistrovanaProdavnica> getRegistrovaneProdavnice () {
        return this.registrovaneProdavnice;
    }

    public void setRegistrovaneProdavnice (java.util.List<RegistrovanaProdavnica> registrovaneProdavnice) {
        this.registrovaneProdavnice = registrovaneProdavnice;
    }
    
    public java.util.List<RobaUMagacinu> getRobeUMagacinu () {
        return this.robeUMagacinu;
    }

    public void setRobeUMagacinu (java.util.List<RobaUMagacinu> robeUMagacinu) {
        this.robeUMagacinu = robeUMagacinu;
    }
    


    public Dobavljac () {
        super();
        this.registrovaneProdavnice = new java.util.ArrayList<>();
        this.robeUMagacinu = new java.util.ArrayList<>();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((registrovaneProdavnice == null) ? 0 : registrovaneProdavnice.hashCode());
	result = prime * result + ((robeUMagacinu == null) ? 0 : robeUMagacinu.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Dobavljac other = (Dobavljac) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (registrovaneProdavnice == null) {
	    if (other.registrovaneProdavnice != null)
		return false;
	} else if (!registrovaneProdavnice.equals(other.registrovaneProdavnice))
	    return false;
	if (robeUMagacinu == null) {
	    if (other.robeUMagacinu != null)
		return false;
	} else if (!robeUMagacinu.equals(other.robeUMagacinu))
	    return false;
	return true;
    }
    
    @Override
    public DobavljacDTO getDTO() {
	DobavljacDTO dobavljacDTO = new DobavljacDTO();
	dobavljacDTO.setId(id);
	dobavljacDTO.setNaziv(naziv);
	
	ArrayList<RegistrovanaProdavnicaDTO> registrovaneProdavniceDTOs = new ArrayList<RegistrovanaProdavnicaDTO>();
	for (RegistrovanaProdavnica registrovanaProdavnica : registrovaneProdavnice) {
	    registrovaneProdavniceDTOs.add(registrovanaProdavnica.getDTOinsideDTO());
	}
	dobavljacDTO.setRegistrovaneProdavniceDTO(registrovaneProdavniceDTOs);
	
	ArrayList<RobaUMagacinuDTO> robeUMagacinuDTOs = new ArrayList<RobaUMagacinuDTO>();
	for (RobaUMagacinu robaUMagacinu : this.robeUMagacinu) {
	    robeUMagacinuDTOs.add(robaUMagacinu.getDTOinsideDTO());
	}
	dobavljacDTO.setRobeUMagacinuDTO(robeUMagacinuDTOs);
	
	return dobavljacDTO;
    }
    
    @Override
    public DobavljacDTO getDTOinsideDTO() {
	DobavljacDTO dobavljacDTO = new DobavljacDTO();
	dobavljacDTO.setId(id);
	dobavljacDTO.setNaziv(naziv);
        return dobavljacDTO;
    }
    
    @Override
    public void update(Dobavljac entitet) {
	this.setNaziv(entitet.getNaziv());
        
    }

}