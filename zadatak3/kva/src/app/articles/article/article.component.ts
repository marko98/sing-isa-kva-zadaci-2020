import { OnInit, OnDestroy, Component, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';

// model
import { Article } from 'src/app/shared/model/article.model';

// service
import { ArticleService } from 'src/app/shared/service/article.service';

@Component({
    selector: 'app-article',
    templateUrl: './article.component.html',
    styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit, OnDestroy {
    @Input() shouldCreate: boolean = false;
    @Input() article: Article;
    @Output() onDone: EventEmitter<any> = new EventEmitter();
    public form: FormGroup;

    constructor(private articleService: ArticleService){}

    onSubmit = (): void => {
        // console.log(this.form);

        if(this.form.valid){
            let article = new Article();
            article.setName(this.form.value.name);
            article.setDescription(this.form.value.description);
            article.setPrice(this.form.value.price);
            if(!this.shouldCreate)
                article.setIdentificator(this.article.getIdentificator());
            // console.log(article);
            this.articleService.save(article);
            this.onDone.emit();
        }
        
    }

    // interfaces
    ngOnInit() {
        if(!this.shouldCreate) {
            this.form = new FormGroup({
                'name': new FormControl(this.article.getName(), [Validators.required]),
                'description': new FormControl(this.article.getDescription(), [Validators.required, Validators.minLength(5)]),
                'price': new FormControl(this.article.getPrice(), [Validators.required, Validators.min(0)])
            })
        } else {
            this.form = new FormGroup({
                'name': new FormControl('', [Validators.required]),
                'description': new FormControl('', [Validators.required]),
                'price': new FormControl('', [Validators.required])
            })
        }
        

        // console.log("ArticleComponent init");
    }

    ngOnDestroy() {
        // console.log("ArticleComponent destroyed");
    }

}