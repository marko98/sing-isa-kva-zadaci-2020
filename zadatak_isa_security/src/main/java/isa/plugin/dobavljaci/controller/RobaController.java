package isa.plugin.dobavljaci.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import isa.controller.CrudController;
import isa.dto.interfaces.DTO;
import isa.model.Dobavljaci.Roba;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/roba")
@CrossOrigin(origins = "*")

public class RobaController extends CrudController<Roba, Long> {

    @Secured("ROLE_REGISTROVANA_PRODAVNICA")
    @RequestMapping(path = "", method = RequestMethod.GET)
    @Override
    public ResponseEntity<Page<DTO<Long>>> findAll(Pageable pageable) {
	System.out.println("Pageable: " + pageable); // Page request [number: 0, size 20, sort: UNSORTED]

//	get Page<T>
	Page<Roba> page = this.service.findAll(pageable);

//	map T to DTO and get Page<DTO>
	Page<DTO<Long>> pageDTO = page.map(t -> t.getDTO());	

	return new ResponseEntity<Page<DTO<Long>>>(pageDTO, HttpStatus.OK);
    }
    
}