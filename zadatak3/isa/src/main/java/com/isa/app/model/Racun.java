package com.isa.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.isa.app.dto.RacunDTO;

@Entity
@Table(name = "racun")
public class Racun implements ModelEntity<Racun> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "java.lang.Integer")
	@Column(name = "id")
	private Integer id;
	
	@Type(type = "string")
	@Column(name = "broj_racuna", length = 32, unique = true)
	private String brojRacuna;
	
	@Type(type = "java.lang.Double")
	@Column(name = "stanje")
	private Double stanje;
	
	@ManyToOne(fetch = FetchType.EAGER,
				cascade = {
						CascadeType.DETACH,
						CascadeType.MERGE,
						CascadeType.PERSIST,
						CascadeType.REFRESH
				},
				optional = true)
	@JoinColumn(name = "vlasnik_id", nullable = true)
	private Korisnik vlasnik;
	
	@OneToMany(mappedBy = "racun",
				fetch = FetchType.LAZY,
				cascade = CascadeType.ALL)
	private List<Stavka> stavke;

	public Racun() {
		super();
	}

	public Racun(String brojRacuna, Double stanje) {
		super();
		this.brojRacuna = brojRacuna;
		this.stanje = stanje;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public Double getStanje() {
		return stanje;
	}

	public void setStanje(Double stanje) {
		this.stanje = stanje;
	}

	public Korisnik getVlasnik() {
		return vlasnik;
	}

	public void setVlasnik(Korisnik vlasnik) {
		this.vlasnik = vlasnik;
	}
	
	public List<Stavka> getStavke() {
		return stavke;
	}

	public void setStavke(List<Stavka> stavke) {
		this.stavke = stavke;
	}
	
	public boolean addStavka(Stavka stavka) {
		if(this.stavke == null)
			this.stavke = new ArrayList<Stavka>();
		
		this.stavke.add(stavka);
		
		stavka.setRacun(this);
		
		return true;
	}

	@Override
	public String toString() {
		return "Racun [id=" + id + ", brojRacuna=" + brojRacuna + ", stanje=" + stanje + ", vlasnik=" + vlasnik + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Racun other = (Racun) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
//	interface
	public Integer getIdentifikator() {
		return this.id;
	};
	
	public void update(Racun racun) {
		this.setBrojRacuna(racun.getBrojRacuna());
		this.setStanje(racun.getStanje());
		this.setVlasnik(racun.getVlasnik());
	};
	
	public RacunDTO getDTO() {
		RacunDTO racunDTO = new RacunDTO();
		
		racunDTO.setBrojRacuna(this.getBrojRacuna());
		racunDTO.setStanje(this.getStanje());
		racunDTO.setVlasnik(this.getVlasnik());
		racunDTO.setId(this.getId());
		
		return racunDTO;
	};
	
}
