package isa.plugin.prodavnice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.controller.CrudController;
import isa.model.Prodavnice.Prodavnica;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/prodavnica")
@CrossOrigin(origins = "*")

public class ProdavnicaController extends CrudController<Prodavnica, Long> {

}