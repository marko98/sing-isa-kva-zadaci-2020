import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// model
import { Article } from '../model/article.model';

// repository
import { CRUDRepository } from './CRUD.repository';

// service
import { UserService } from '../service/user.service';
import { UIService } from '../service/ui.service';

declare interface ArticleInterface {
    naziv: string,
    opis: string,
    id: string,
    cena: number
}

@Injectable()
export class ArticleRepository extends CRUDRepository<Article> {
    private path: string = 'http://localhost:8080/isa.zadatak.3/api/artikli/';

    constructor(protected httpClient: HttpClient,
                protected uiService: UIService,
                private userService: UserService){
        super(httpClient, uiService);

        this.userService.attach(this.userServiceObserver);
    }

    // observer method
    private userServiceObserver = (observable: UserService): void => {
        // console.log("ArticleRepository notified about changes in UserService");
        // console.log(observable);

        if (!observable.getLoggedUser()) {
            this.models = [];
        } else if (observable.getLoggedUser() && this.models.length == 0) {
            this.getArticles();
        }
    }

    private getArticles = (): void => {
        this.httpClient.get(
            this.path
        )
        .subscribe(
            (page: any) => {
                (<ArticleInterface[]>page.content).forEach(article => {
                    let a = new Article();
                    a.setName(article.naziv);
                    a.setDescription(article.opis);
                    a.setPrice(article.cena);
                    a.setIdentificator(article.id);
                    // console.log(a);
                    this.models.push(a);
                });

                // console.log(this.models);
                this.notify();
            },
            err => {
                // console.log(err);
                this.uiService.showSnackbar(err.error, null, 1500);
            }
        );
    }
}