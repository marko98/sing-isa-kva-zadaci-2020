package isa.app.dto;

import java.util.ArrayList;

public class BankaDTO extends DTO {

    /**
     * 
     */
    private static final long serialVersionUID = 5906340831224084617L;

    private Long id;

    private String naziv;

    private java.util.List<KlijentDTO> klijentiDTO;

    private java.util.List<KreditDTO> kreditiDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public java.util.List<KlijentDTO> getKlijentiDTO() {
	return this.klijentiDTO;
    }

    public void setKlijentiDTO(java.util.List<KlijentDTO> klijentiDTO) {
	this.klijentiDTO = klijentiDTO;
    }

    public java.util.List<KreditDTO> getKreditiDTO() {
	return this.kreditiDTO;
    }

    public void setKreditiDTO(java.util.List<KreditDTO> kreditiDTO) {
	this.kreditiDTO = kreditiDTO;
    }

    public BankaDTO() {
	super();
	this.klijentiDTO = new ArrayList<KlijentDTO>();
	this.kreditiDTO = new ArrayList<KreditDTO>();
    }

}