package isa.dto.Dobavljaci;

import isa.dto.interfaces.DTO;

public class RobaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 3170677243143064565L;

    private Long id;

    private String naziv;

    private java.util.List<RobaUMagacinuDTO> robeUMagacinuDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public java.util.List<RobaUMagacinuDTO> getRobeUMagacinuDTO() {
	return this.robeUMagacinuDTO;
    }

    public void setRobeUMagacinuDTO(java.util.List<RobaUMagacinuDTO> robeUMagacinuDTO) {
	this.robeUMagacinuDTO = robeUMagacinuDTO;
    }

    public RobaDTO() {
	super();
	this.robeUMagacinuDTO = new java.util.ArrayList<>();
    }

}