import { Injectable } from '@angular/core';
import { PageableCrudService } from 'projects/autocomplete/src/public-api';
import { Grad } from '../model/entity/grad.model';
import { HttpClient } from '@angular/common/http';
import { GradFactory } from '../model/pattern/creational/factory/grad-factory.model';

@Injectable()
export class GradService extends PageableCrudService<Grad, number> {
  constructor(protected httpClient: HttpClient) {
    super(
      httpClient,
      'http',
      'localhost',
      8080,
      'api/grad',
      'api/grad/naziv/',
      new GradFactory()
    );
  }
}
