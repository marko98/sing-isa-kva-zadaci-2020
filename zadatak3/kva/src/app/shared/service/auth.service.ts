import { Injectable } from '@angular/core';

// repository
import { UserRepository } from '../repository/user.repository';

@Injectable()
export class AuthService {

    constructor(private userRepository: UserRepository){}

    onLogout = (): void => {
        this.userRepository.logout();
    }

    onLogin = (identificator: number): void => {
        this.userRepository.login(identificator);
    }

}