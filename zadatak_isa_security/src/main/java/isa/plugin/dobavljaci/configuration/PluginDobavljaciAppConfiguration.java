package isa.plugin.dobavljaci.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@Configuration

public class PluginDobavljaciAppConfiguration {

    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(
		new ClassPathResource("/isa/plugin/dobavljaci/configuration/plugin.dobavljaci.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}