package isa.app.model;

import java.io.Serializable;

import isa.app.dto.DTO;

public interface Entity<T, ID extends Serializable> {

    public ID getId();

    public DTO getDTO();

    public DTO getDTOinsideDTO();

    public void update(T entity);

}