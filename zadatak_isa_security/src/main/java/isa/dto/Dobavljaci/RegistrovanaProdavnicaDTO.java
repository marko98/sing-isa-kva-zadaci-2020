package isa.dto.Dobavljaci;

import isa.dto.interfaces.DTO;

public class RegistrovanaProdavnicaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 8447014228436299360L;

    private Long id;
        
    private String pib;
        
    private String maticniBroj;
        
    private String naziv;
        
    private String lozinka;
        
    private DobavljacDTO dobavljacDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getPib () {
        return this.pib;
    }

    public void setPib (String pib) {
        this.pib = pib;
    }
    
    public String getMaticniBroj () {
        return this.maticniBroj;
    }

    public void setMaticniBroj (String maticniBroj) {
        this.maticniBroj = maticniBroj;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public String getLozinka () {
        return this.lozinka;
    }

    public void setLozinka (String lozinka) {
        this.lozinka = lozinka;
    }
    
    public DobavljacDTO getDobavljacDTO () {
        return this.dobavljacDTO;
    }

    public void setDobavljacDTO (DobavljacDTO dobavljacDTO) {
        this.dobavljacDTO = dobavljacDTO;
    }
    


    public RegistrovanaProdavnicaDTO () {
        super();
    }

}