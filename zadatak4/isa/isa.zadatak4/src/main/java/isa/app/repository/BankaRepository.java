package isa.app.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import isa.app.model.Banka;

@Repository
@Scope("singleton")
public class BankaRepository extends CrudRepository<Banka, Long> {

}