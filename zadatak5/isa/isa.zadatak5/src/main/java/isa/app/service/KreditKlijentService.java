package isa.app.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import isa.app.model.KreditKlijent;

@Service
@Scope("singleton")
public class KreditKlijentService extends CrudService<KreditKlijent, Long> {

}