import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {
  PageDataSource,
  OptionItem,
} from 'projects/autocomplete/src/public-api';
import { DrzavaService } from './shared/service/drzava.service';
import { GradService } from './shared/service/grad.service';
import { AutocompleteInterface } from 'projects/autocomplete/src/lib/shared/model/interface/autocomplete.interface';
import { Subject, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    // vise DrzavaService nece biti singleton na nivou cele aplikacije, vec samo ove komponente
    DrzavaService,
    GradService,
  ],
})
export class AppComponent implements OnInit {
  title = 'my-angular-autocomplete';

  public formGroup: FormGroup;

  public countryAutocomplete: AutocompleteInterface;
  public cityAutocomplete: AutocompleteInterface;

  constructor(
    private drzavaService: DrzavaService,
    private gradService: GradService
  ) {}

  ngOnInit() {
    this.countryAutocomplete = {
      pageDataSource: new PageDataSource(this.drzavaService),
      criteria: 's',
      inputPlaceHolder: 'pick country',
      sortingOptions: ['naziv'],
      disabled: false,
      appearance: 'outline',
      valueSubject: new Subject(),
      hiddenSubject: new BehaviorSubject(false),
    };

    this.cityAutocomplete = {
      pageDataSource: new PageDataSource(this.gradService),
      criteria: 's',
      inputPlaceHolder: 'pick city',
      sortingOptions: ['naziv'],
      disabled: false,
      valueSubject: new Subject(),
      hiddenSubject: new BehaviorSubject(true),
    };

    this.countryAutocomplete.valueSubject.subscribe(
      (optionItem: OptionItem) => {
        // console.log(optionItem);
        if (optionItem) {
          this.cityAutocomplete.pageDataSource.changePath(
            'api/grad/drzava/' + optionItem.getEntitet().getId().toString()
          );
          this.cityAutocomplete.pageDataSource.changeCriteriaPath(
            'api/grad/drzava/' +
              optionItem.getEntitet().getId().toString() +
              '/naziv/'
          );
          this.cityAutocomplete.hiddenSubject.next(false);
        } else {
          this.gradService.resetPathAndCriteriaPathToDefault();
          this.cityAutocomplete.value = undefined;
          this.cityAutocomplete.valueSubject.next(this.cityAutocomplete.value);
          this.cityAutocomplete.criteria = '';
          this.cityAutocomplete.hiddenSubject.next(true);
        }
      }
    );

    // this.cityAutocomplete.parentAutocomplete = {
    //   parentValueSubject: this.countryAutocomplete.valueSubject,
    //   onValueChange: (optionItem: OptionItem) => {
    //     // console.log(optionItem);
    //     if (optionItem) {
    //       this.cityAutocomplete.pageDataSource.changePath(
    //         'api/grad/drzava/' + optionItem.getEntitet().getId().toString()
    //       );
    //       this.cityAutocomplete.pageDataSource.changeCriteriaPath(
    //         'api/grad/drzava/' +
    //           optionItem.getEntitet().getId().toString() +
    //           '/naziv/'
    //       );
    //     } else {
    //       this.gradService.resetPathAndCriteriaPathToDefault();
    //       this.cityAutocomplete.value = undefined;
    //       this.cityAutocomplete.valueSubject.next(this.cityAutocomplete.value);
    //       this.cityAutocomplete.criteria = '';
    //     }
    //   },
    // };
  }
}
