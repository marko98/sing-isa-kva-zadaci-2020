package spring.zadatak2.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import spring.zadatak2.dto.DTO;
import spring.zadatak2.model.Artikal;
import spring.zadatak2.service.ArtikalService;

@Controller
@Scope("singleton")
@RequestMapping(path = "/api/artikli")
@CrossOrigin(origins = "http://localhost:4200")
public class ArtikalController extends CRUDController<Artikal> {
	
	@RequestMapping(path = "/{donjaCena}/{gornjaCena}", method = RequestMethod.GET)
	public ResponseEntity<List<DTO>> getAllByPrice(@PathVariable("donjaCena") Double donjaCena,
														@PathVariable("gornjaCena") Double gornjaCena){
		return new ResponseEntity<List<DTO>>(((ArtikalService)this.service).findByPrice(donjaCena, gornjaCena).stream().map(t -> t.getDTO()).collect(Collectors.toList()), HttpStatus.OK);
	}
	
}
