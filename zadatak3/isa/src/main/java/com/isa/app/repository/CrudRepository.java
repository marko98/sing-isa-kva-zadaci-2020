package com.isa.app.repository;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.isa.app.hbsf.HibernateSessionFactory;
import com.isa.app.model.ModelEntity;

public abstract class CrudRepository<T extends ModelEntity<T>, ID extends Serializable> {
	
	@Autowired
	protected PagingAndSortingRepository<T, ID> pagingAndSortingRepository;
	
	@Autowired
	protected HibernateSessionFactory hibernateSessionFactory;
	
//	Read
	public Iterable<T> findAll() {
		return this.pagingAndSortingRepository.findAll();
	}
	
//	Read
	public Page<T> findAll(Pageable pageable) {
		return this.pagingAndSortingRepository.findAll(pageable);
	}
	
//	Read
	public Optional<T> findOne(ID id) {
		return this.pagingAndSortingRepository.findById(id);
	}
	
//	Create and Update
	public void save(T model) {
		if (model.getId() != null) {
			Optional<T> t = this.pagingAndSortingRepository.findById((ID) model.getId());
			
			if(t.isEmpty()) {
				this.pagingAndSortingRepository.save(model);
			} else {
				t.get().update(model);
				this.pagingAndSortingRepository.save(t.get());
			}
		} else {
			this.pagingAndSortingRepository.save(model);
		}		
	}
	
//	Delete
	public void delete(Integer id) {
		this.pagingAndSortingRepository.deleteById((ID) id);
	}
	
//	Delete
	public void delete(T model) {
		this.delete(model.getId());
	}
	
}
