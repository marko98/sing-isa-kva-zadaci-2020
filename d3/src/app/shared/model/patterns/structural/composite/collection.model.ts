export abstract class Collection {
  protected children: Collection[] = [];
  protected parent: Collection = undefined;

  abstract addChild(child: Collection): boolean;

  abstract removeChild(child: Collection): boolean;

  abstract shouldHaveChildren(): boolean;

  getChildren = (): Collection[] => {
    // vracamo kopiju
    // return this.children.slice();
    return this.children;
  };

  getParent = (): Collection => {
    return this.parent;
  };

  setParent = (parent: Collection): boolean => {
    this.parent = parent;
    return true;
  };
}
