package com.isa.app.dto;

import java.util.List;

import com.isa.app.model.Korpa;
import com.isa.app.model.Racun;
import com.isa.app.model.Uloga;

public class KorisnikDTO extends DTO {
	private Integer id;
	private String ime, prezime, korisnickoIme, lozinka;
	private Uloga uloga;
	private List<Racun> racuni;
	private Korpa korpa;
	
	public KorisnikDTO() {}

	public KorisnikDTO(Integer id, String ime, String prezime, String korisnickoIme, String lozinka, Uloga uloga, List<Racun> racuni, Korpa korpa) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.uloga = uloga;
		this.racuni = racuni;
		this.korpa = korpa;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public Uloga getUloga() {
		return uloga;
	}

	public void setUloga(Uloga uloga) {
		this.uloga = uloga;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public List<Racun> getRacuni() {
		return racuni;
	}

	public void setRacuni(List<Racun> racuni) {
		this.racuni = racuni;
	}

	public Korpa getKorpa() {
		return korpa;
	}

	public void setKorpa(Korpa korpa) {
		this.korpa = korpa;
	}

	@Override
	public String toString() {
		return "KorisnikDTO [id=" + id + ", ime=" + ime + ", prezime=" + prezime + ", korisnickoIme=" + korisnickoIme
				+ ", lozinka=" + lozinka + ", uloga=" + uloga + ", racuni=" + racuni + ", korpa=" + korpa + "]";
	}
	
}
