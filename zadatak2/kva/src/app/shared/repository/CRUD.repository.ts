import { HttpClient } from '@angular/common/http';

// model
import { Observable } from '../model/patterns/behavioural/observer/observable.model';

// interface
import { Entity } from '../model/interface/entity.interface';

// service
import { UIService } from '../service/ui.service';

export class CRUDRepository<T> extends Observable {
    protected models: T[] = [];

    constructor(protected httpClient: HttpClient,
                protected uiService: UIService){
        super();
    }

    // read
    findAll = (): T[] => {
        return this.models;
    }

    findOne = (identificator: string): T => {
        let e;
        this.models.forEach(entity => {
            if((<Entity<T>>(<unknown>entity)).getIdentificator() === identificator)
                e = entity;
        });
        return e;
    }

    // create and update
    save = (url: string, model: T): void => {
        let m = this.findOne((<Entity<T>>(<unknown>model)).getIdentificator());
        
        if (!m){
            this.httpClient.post(
                url,
                JSON.stringify((<Entity<T>>(<unknown>model)).toDict())
            )
            .subscribe(
                (t: T) =>{
                    (<Entity<T>>(<unknown>model)).fromDict(t);
                    this.models.push(model);
                    this.notify();
                },
                err => {
                    console.log(err);
                    this.uiService.showSnackbar(err.error, null, 1500);
                }
            );
        } else {
            this.httpClient.put(
                url,
                JSON.stringify((<Entity<T>>(<unknown>model)).toDict())
            )
            .subscribe(
                (t: T) =>{
                    (<Entity<T>>(<unknown>m)).update(model);
                    this.notify();
                },
                err => {
                    console.log(err);
                    this.uiService.showSnackbar(err.error, null, 1500);
                }
            );
        }
        
    }

    // delete
    deleteByIdentificator = (url: string, identificator: string): void => {
        this.httpClient.delete(url + identificator)
            .subscribe(
                result => {
                    // console.log(result);
                    this.models = this.models.filter((m: unknown) => (<Entity<T>>m).getIdentificator() !== identificator);
                    this.notify();
                },
                err => {
                    console.log(err);
                    this.uiService.showSnackbar(err.error, null, 1500);
                }
            );
    }

    delete = (url: string, entity: T): void => {
        this.deleteByIdentificator(url, (<Entity<T>>(<unknown>entity)).getIdentificator());
    }

    // override-ujemo
    notify = () => {
        this.observers.forEach(observer => {
            observer(this);
        });
    }

}