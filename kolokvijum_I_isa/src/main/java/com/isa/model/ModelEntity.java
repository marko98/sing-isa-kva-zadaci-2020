package com.isa.model;

import java.io.Serializable;

import com.isa.dto.DTO;

public interface ModelEntity<T, ID extends Serializable> {
	public ID getId();
	public void update(T t);
	public DTO getDTO();
}
