import { Component, OnInit, OnDestroy, HostListener, ViewChild } from '@angular/core';

// model
import { Article } from '../shared/model/article.model';
import { Table, Row } from 'my-angular-table';

// service
import { ArticleService } from '../shared/service/article.service';

@Component({
    selector: 'app-articles',
    templateUrl: './articles.component.html',
    styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit, OnDestroy {
  public articles: Article[] = [];

  public articleToUpdate: Article;
  public shouldUpdate: boolean = false;
  public shouldCreate: boolean = false;
  public canClose: boolean = true;

  public table: Table;

  constructor(private articleService: ArticleService){}

  onAddArticle = (): void => {
    this.shouldCreate = true;
  }

  onDone = (): void => {
    this.shouldCreate = false;
    this.shouldUpdate = false;
    this.articleToUpdate = null;
  }

  onUpdateArticle = (articles: Article[]): void => {
    this.articleToUpdate = articles[0];
    this.shouldUpdate = true;
  }

  onDeleteArticles = (articles: Article[]): void => {
    // console.log(articles);
    articles.forEach(article => {
      // console.log(article);
      this.articleService.deleteByIdentificatior(article.getIdentificator());
    });
  }

  /*
    observers nisu nista drugo do same funkcije koja treba da se pozove kada nad
    objektom tipa Observable na koju smo subscribe-ovani dodje do promena
  */
  articleServiceObserver = (observable: ArticleService): void => {
    // console.log("AppComponent notified about changes in ArticleService");
    // console.log(observable);
    this.articles = observable.findAll();

    this.fillUpdateTable();
  }

  fillUpdateTable = (): void => {
    // deleting
    this.table.getChildren().forEach((r: Row) => this.table.removeChild(r));

    // adding
    this.articles.forEach((article: Article) => {
      // console.log(article);

      this.table.addChildValue({
        data: article,
        rowItems: [
          {context: article.getName()},
          {context: article.getDescription()},
          {context: article.getPrice(), imgSrc: "https://img.icons8.com/dusk/2x/cheap.png"},
          {context: "update", button: {function: this.onUpdateArticle}, imgSrc: "https://img.icons8.com/nolan/2x/approve-and-update.png"},
          {context: "delete", button: {function: this.onDeleteArticles}, imgSrc: "https://img.icons8.com/dusk/2x/filled-trash.png"}
        ]
      });
      
    });
  }

  // interfaces
  ngOnInit() {
    this.articleService.attach(this.articleServiceObserver);

    this.articles = this.articleService.findAll();

    // create table
    this.table = new Table({
      header: {
        headerItems: [
          {context: "name"},
          {context: "description"},
          {context: "price"},
          {context: ""},
          {context: ""}
        ]
      },
      contextMenu: {
        contextMenuItems: [
          {context: "update", function: this.onUpdateArticle, imgSrc: "https://img.icons8.com/nolan/2x/approve-and-update.png"},
          {context: "delete", function: this.onDeleteArticles, imgSrc: "https://img.icons8.com/dusk/2x/filled-trash.png"}
        ]
      }
    });

    this.fillUpdateTable();

    console.log("ArticlesComponent init");
  }

  ngOnDestroy() {
    if (this.articleService)
      this.articleService.dettach(this.articleServiceObserver);

    console.log("ArticlesComponent destroyed");
  }
}