import { EntitetFactory } from 'projects/autocomplete/src/public-api';
import { Grad } from '../../../entity/grad.model';
import { GRAD_MODEL_INTERFACE } from '../../../interface/grad.model-interface';

export class GradFactory implements EntitetFactory<Grad, number> {
  build = (gradModelInterfejs?: GRAD_MODEL_INTERFACE): Grad => {
    let grad: Grad = new Grad();

    if (gradModelInterfejs) {
      grad.setId(gradModelInterfejs.id);
      grad.setNaziv(gradModelInterfejs.naziv);
      grad.setStanje(gradModelInterfejs.stanje);
      grad.setVersion(gradModelInterfejs.version);
      grad.setDrzava(gradModelInterfejs.drzavaDTO);
    }

    return grad;
  };
}
