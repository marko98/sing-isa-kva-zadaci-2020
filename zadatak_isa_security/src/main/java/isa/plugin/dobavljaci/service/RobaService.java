package isa.plugin.dobavljaci.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import isa.model.Dobavljaci.Roba;
import isa.service.CrudService;

@Service
@Scope("singleton")

public class RobaService extends CrudService<Roba, Long> {

}