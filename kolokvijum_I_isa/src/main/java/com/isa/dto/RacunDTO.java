package com.isa.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class RacunDTO extends DTO {

	private Long id;

	private LocalDateTime datumIzdavanja;
	
	private List<DTO> stavkeRacuna;

	public RacunDTO() {
		super();
		this.stavkeRacuna = new ArrayList<DTO>();
	}

	public RacunDTO(Long id, LocalDateTime datumIzdavanja, List<DTO> stavkeRacuna) {
		super();
		this.id = id;
		this.datumIzdavanja = datumIzdavanja;
		this.stavkeRacuna = stavkeRacuna;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDatumIzdavanja() {
		return datumIzdavanja;
	}

	public void setDatumIzdavanja(LocalDateTime datumIzdavanja) {
		this.datumIzdavanja = datumIzdavanja;
	}

	public List<DTO> getStavkeRacuna() {
		return stavkeRacuna;
	}

	public void setStavkeRacuna(List<DTO> stavkeRacuna) {
		this.stavkeRacuna = stavkeRacuna;
	}

	@Override
	public String toString() {
		return "RacunDTO [id=" + id + ", datumIzdavanja=" + datumIzdavanja + ", stavkeRacuna=" + stavkeRacuna + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datumIzdavanja == null) ? 0 : datumIzdavanja.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((stavkeRacuna == null) ? 0 : stavkeRacuna.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RacunDTO other = (RacunDTO) obj;
		if (datumIzdavanja == null) {
			if (other.datumIzdavanja != null)
				return false;
		} else if (!datumIzdavanja.equals(other.datumIzdavanja))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (stavkeRacuna == null) {
			if (other.stavkeRacuna != null)
				return false;
		} else if (!stavkeRacuna.equals(other.stavkeRacuna))
			return false;
		return true;
	}
	
}
