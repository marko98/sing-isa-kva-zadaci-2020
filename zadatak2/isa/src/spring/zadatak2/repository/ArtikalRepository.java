package spring.zadatak2.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import spring.zadatak2.model.Artikal;

@Repository
@Scope("singleton")
public class ArtikalRepository extends CRUDRepository<Artikal> {	
	public ArtikalRepository() {
		super();
		this.models = new ArrayList<Artikal>();
		this.models.add(new Artikal("Keks sa cokoladom", 49.99, "Najbolji keks sa cokoladom"));
		this.models.add(new Artikal("Voda", 25.99, "Gazirana mineralna voda"));
		this.models.add(new Artikal("Cips", 69.99, "Cips sa paprikom"));
		this.models.add(new Artikal("Krompir", 189.99, "Beli krompir"));
		this.models.add(new Artikal("Sapun", 16.99, "Sapun"));
		this.models.add(new Artikal("Maska", 9.99, "Maska za zastitu"));
	}
	
	public List<Artikal> findByPrice(Double gornjaCena, Double donjaCena) {
		ArrayList<Artikal> pronadjeni = new ArrayList<Artikal>();
		
		for (Artikal artikal : models) {
			if(gornjaCena >= artikal.getCena() && artikal.getCena() >= donjaCena) {
				pronadjeni.add(artikal);
			}
		}
		
		return pronadjeni;
	}
	
//	Create and Update
	@Override
	public void save(Artikal model) {
		Artikal t = this.findOne(model.getIdentifikator());
		
		if(t == null) {
			model.setIdentifikator(Integer.toString(Artikal.id++));
			this.models.add(model);
		} else {
			t.update(model);
		}
	}
}

