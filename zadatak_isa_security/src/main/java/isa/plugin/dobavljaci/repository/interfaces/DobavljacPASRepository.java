package isa.plugin.dobavljaci.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import isa.model.Dobavljaci.Dobavljac;

@Repository
@Scope("singleton")

public interface DobavljacPASRepository extends PagingAndSortingRepository<Dobavljac, Long> {

}