package com.isa.app.dto;

import java.util.List;

import com.isa.app.model.Korisnik;
import com.isa.app.model.Stavka;

public class RacunDTO extends DTO {
	
	private int id;	
	private String brojRacuna;	
	private Double stanje;	
	private Korisnik vlasnik;
	private List<Stavka> stavke;
	
	public RacunDTO() {
		super();
	}

	public RacunDTO(Integer id, String brojRacuna, Double stanje, Korisnik vlasnik, List<Stavka> stavke) {
		super();
		this.id = id;
		this.brojRacuna = brojRacuna;
		this.stanje = stanje;
		this.vlasnik = vlasnik;
		this.stavke = stavke;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public Double getStanje() {
		return stanje;
	}

	public void setStanje(Double stanje) {
		this.stanje = stanje;
	}

	public Korisnik getVlasnik() {
		return vlasnik;
	}

	public void setVlasnik(Korisnik vlasnik) {
		this.vlasnik = vlasnik;
	}

	public List<Stavka> getStavke() {
		return stavke;
	}

	public void setStavke(List<Stavka> stavke) {
		this.stavke = stavke;
	}

	@Override
	public String toString() {
		return "RacunDTO [id=" + id + ", brojRacuna=" + brojRacuna + ", stanje=" + stanje + ", vlasnik=" + vlasnik
				+ ", stavke=" + stavke + "]";
	}

}
