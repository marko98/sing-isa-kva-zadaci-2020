package com.isa.app.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.isa.app.model.Artikal;

@Repository
@Scope("singleton")
public interface ArtikalPASRepository extends PagingAndSortingRepository<Artikal, Integer> {

}
