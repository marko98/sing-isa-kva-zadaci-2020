import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

// repository
import { ArticleRepository } from './shared/repository/article.repository';
import { UserRepository } from './shared/repository/user.repository';

// service
import { ArticleService } from './shared/service/article.service';
import { UserService } from './shared/service/user.service';
import { AuthService } from './shared/service/auth.service';
import { RoutingService } from './shared/service/routing.service';
import { AuthInterceptorService } from './shared/service/auth-interceptor.service';
import { UIService } from './shared/service/ui.service';

// guard
import { AuthGuard } from './shared/guard/auth.guard';

@NgModule({
    providers: [
        ArticleRepository,
        ArticleService,
        UserRepository,
        UserService,
        AuthService,
        RoutingService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptorService,
            multi: true
        },
        AuthGuard,
        UIService
    ]
})
export class CoreModule{}