import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// model
import { User, Role, UserDTO } from '../model/user.model';

// repository
import { CRUDRepository } from './CRUD.repository';

// service
import { UIService } from '../service/ui.service';

// service

@Injectable()
export class UserRepository extends CRUDRepository<User> {
    private loggedUser: User;
    private path: string = "http://localhost:8080/spring-zadatak2/api/korisnici/";

    constructor(protected httpClient: HttpClient,
                protected uiService: UIService){
        super(httpClient, uiService);
    }

    getLoggedUser = (): User => {
        if(this.loggedUser)
            return this.loggedUser.clone();
        return undefined;
    }

    setLoggedUser = (user: User): void => {
        this.loggedUser = user;
    }

    logout = (): void => {
        this.loggedUser = undefined;
        this.models = [];
        this.notify();
    }

    login = (identificator: string): void => {
        this.httpClient.get(
            this.path + identificator
        )
        .subscribe(
            (userDTO: UserDTO) => {
                let u = new User();
                u.fromDict(userDTO);
                // console.log(u);
                this.loggedUser = u;
                this.notify();
                this.getUsers();
            },
            err => {
                console.log(err);
            }
        );
    }

    private getUsers = (): void => {
        this.httpClient.get(
            this.path
        )
        .subscribe(
            (users: UserDTO[]) => {
                users.forEach(user => {

                    if(user.korisnickoIme !== this.loggedUser.getIdentificator()) {
                        let u = new User();
                        u.setFirstName(user.ime);
                        u.setLastName(user.prezime);
                        u.setUsername(user.korisnickoIme);
                        u.setPassword(user.lozinka);
    
                        let uloga;
                        if (user.uloga == "ADMINISTRATOR")
                            uloga = Role.ADMINISTRATOR;
                        else if (user.uloga == "KUPAC")
                            uloga = Role.KUPAC;
                        u.setRole(uloga);

                        this.models.push(u);
                    }
                    
                });

                // console.log(this.models);
                this.notify();
            },
            err => {
                console.log(err);
            }
        );
    }
}