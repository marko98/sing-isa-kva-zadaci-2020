package isa.app.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import isa.app.model.Banka;

@Service
@Scope("singleton")
public class BankaService extends CrudService<Banka, Long> {

}