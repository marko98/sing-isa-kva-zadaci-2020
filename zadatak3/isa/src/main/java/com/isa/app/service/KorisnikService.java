package com.isa.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.isa.app.model.Korisnik;
import com.isa.app.model.Uloga;
import com.isa.app.repository.KorisnikRepository;

@Service
@Scope("singleton")
public class KorisnikService extends CrudService<Korisnik, Integer> {
	
	public List<Korisnik> findByRole(Uloga uloga) {
		return ((KorisnikRepository) this.repository).findByRole(uloga);
	}
	
	public Korisnik findByUsername(String korisnickoIme) {
		return ((KorisnikRepository) this.repository).findByUsername(korisnickoIme);
	}
	
	public boolean login(Korisnik korisnik, String lozinka) {
		if (korisnik.getLozinka().equals(lozinka)) {
			korisnik.setUlogovan(true);
			this.repository.save(korisnik);
			return true;
		}
		return false;
	}
	
	public void logout(Korisnik korisnik) {
		korisnik.setUlogovan(false);
		this.repository.save(korisnik);
	}
	
	public boolean isAuth(Integer identifikator) {
		Optional<Korisnik> k = this.repository.findOne(identifikator);
		if(k.isEmpty())
			return false;
		
		if(k.get().isUlogovan())
			return true;
		
		return false;
	}
	
}