package isa.app.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class TopicListeners {

    @JmsListener(destination = "receivedInformation")
    public void onReceivedInformation(String message) {
	System.out.println(message);
    }

}