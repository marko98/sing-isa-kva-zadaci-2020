package com.isa.app.dto;

import com.isa.app.model.Artikal;
import com.isa.app.model.Korpa;
import com.isa.app.model.Racun;

public class StavkaDTO extends DTO {

	private Integer id;
	private Korpa korpa;
	private Artikal artikal;
	private Integer kolicina;
	private Racun racun;
	
	public StavkaDTO() {
		super();
	}
	
	public StavkaDTO(Integer id, Korpa korpa, Artikal artikal, Integer kolicina, Racun racun) {
		super();
		this.id = id;
		this.korpa = korpa;
		this.artikal = artikal;
		this.kolicina = kolicina;
		this.racun = racun;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Korpa getKorpa() {
		return korpa;
	}
	
	public void setKorpa(Korpa korpa) {
		this.korpa = korpa;
	}
	
	public Artikal getArtikal() {
		return artikal;
	}
	
	public void setArtikal(Artikal artikal) {
		this.artikal = artikal;
	}
	
	public Integer getKolicina() {
		return kolicina;
	}
	
	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}
	
	public Racun getRacun() {
		return racun;
	}

	public void setRacun(Racun racun) {
		this.racun = racun;
	}

	@Override
	public String toString() {
		return "StavkaDTO [id=" + id + ", korpa=" + korpa + ", artikal=" + artikal + ", kolicina=" + kolicina
				+ ", racun=" + racun + "]";
	}
	
}
