package isa.app.dto;

import java.time.LocalDateTime;

public class LogMessageDTO extends DTO {

    /**
     * 
     */
    private static final long serialVersionUID = -715981891613783538L;

    private String messageTitle;

    private String messageContent;

    private LocalDateTime datetime;

    private LogMessageDTOType logMessageDTOType;

    public String getMessageTitle() {
	return this.messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
	this.messageTitle = messageTitle;
    }

    public String getMessageContent() {
	return this.messageContent;
    }

    public void setMessageContent(String messageContent) {
	this.messageContent = messageContent;
    }

    public LocalDateTime getDatetime() {
	return this.datetime;
    }

    public void setDatetime(LocalDateTime datetime) {
	this.datetime = datetime;
    }

    public LogMessageDTOType getLogMessageDTOType() {
	return this.logMessageDTOType;
    }

    public void setLogMessageDTOType(LogMessageDTOType logMessageDTOType) {
	this.logMessageDTOType = logMessageDTOType;
    }

    public LogMessageDTO() {
	super();
    }

}