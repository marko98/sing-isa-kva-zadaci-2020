package isa.plugin.prodavnice.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import isa.model.Prodavnice.Prodavnica;

@Repository
@Scope("singleton")

public interface ProdavnicaPASRepository extends PagingAndSortingRepository<Prodavnica, Long> {

}