import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

// service
import { AuthService } from './shared/service/auth.service';
import { RoutingService } from './shared/service/routing.service';
import { UserService } from './shared/service/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'moj-primer1';
  public isLoggedIn: boolean = false;

  // routingService je ovde samo inicijalizovan nista vise, necemo ga ovde koristiti
  constructor(private routingService: RoutingService,
              private authService: AuthService, 
              private userService: UserService){}

  onToggleSidenav = (sidenav: MatSidenav) => {
    sidenav.toggle();
  }

  onLogout = (): void => {
    this.authService.onLogout();
  }

  private userServiceObserver = (observable: UserService) => {
    // console.log("AppComponent notified about changes in UserService");
    // console.log(observable);

    if(observable.getLoggedUser())
      this.isLoggedIn = true;
    else
      this.isLoggedIn = false;
  }

  // interfaces
  ngOnInit() {
    this.userService.attach(this.userServiceObserver);
    // console.log("AppComponent init");
  }

  ngOnDestroy() {
    if(this.userService)
      this.userService.dettach(this.userServiceObserver);
    // console.log("AppComponent destroyed");
  }
}
