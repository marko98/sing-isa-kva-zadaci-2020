import { Injectable } from '@angular/core';

// repository
import { ArticleRepository } from '../repository/article.repository';

// model
import { Article } from '../model/article.model';
import { CRUDService } from './CRUD.service';

@Injectable()
export class ArticleService extends CRUDService<Article> {

    constructor(private articleRepository: ArticleRepository){
        super(articleRepository, 'http://localhost:8080/isa.zadatak.3/api/artikli/');

        // buduci da je u pitanju servis on se nikada ne unistava
        // tako da nema potrebe da se unsubscribe-ujemo(dettach())
        this.articleRepository.attach(this.articleObserver);
    }

    articleObserver = (observable: ArticleRepository): void => {
        // console.log("ArticleService notified about changes in ArticleRepository");
        // console.log(observable);
        this.notify();
    }

    // override-ujemo
    notify = () => {
        this.observers.forEach(observer => {
            observer(this);
        });
    }
}