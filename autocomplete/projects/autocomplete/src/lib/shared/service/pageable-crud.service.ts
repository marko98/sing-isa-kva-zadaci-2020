import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

// interface
import { PageableCrudServiceInterface } from './interface/pageable-service.interface';
import { EntitetFactory } from '../pattern/creational/factory/entitet-factory.interface';
import { Entitet } from '../model/interface/entitet.interface';

// model interface
import { ENTITET_MODEL_INTERFACE } from '../model/interface/entitet.model-interface';

// decorator
import { PageMap } from '../pattern/structural/decorator/pagemap.decorator';

// model
import { Observable as CustomObservebale } from '../pattern/behavioural/observer/observable.model';

export declare interface Page<T extends Entitet<T, ID>, ID> {
  content: ENTITET_MODEL_INTERFACE[];
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  pageable: Pageable;
  size: number;
  sort: Sort;
  totalElements: number;
  totalPages: number;
}

export declare interface Pageable {
  offset: number;
  pageNumber: number;
  pageSize: number;
  paged: boolean;
  sort: Sort;
  unpaged: boolean;
}

export declare interface Sort {
  empty: boolean;
  sorted: boolean;
  unsorted: boolean;
}

export declare interface SortParam {
  field: string;
  order: 'asc' | 'desc';
}

export declare interface PageKey {
  pageNumber: number;
  pageSize: number;
  sort: Sort;
}

export abstract class PageableCrudService<T extends Entitet<T, ID>, ID>
  extends CustomObservebale
  implements PageableCrudServiceInterface<T, ID> {
  protected url: string;
  protected path: string;
  private _defaultPath: string;
  protected criteriaPath: string;
  private _defaultCriteriaPath: string;
  protected pages: PageMap<T, ID> = new PageMap();

  constructor(
    protected httpClient: HttpClient,
    schema: string,
    host: string,
    port: string | number,
    defaultPath: string,
    defaultCriteriaPath: string,
    protected factory: EntitetFactory<T, ID>,
    protected pageSize: number = 20
  ) {
    super();
    this.url = schema + '://' + host + ':' + port.toString() + '/';

    this._defaultPath = defaultPath;
    this.path = defaultPath;

    this._defaultCriteriaPath = defaultCriteriaPath;
    this.criteriaPath = defaultCriteriaPath;
  }

  // ----------------- interface PageableCrudService -----------------

  //   read pages
  public findPageWithPageNumber = (page: number = 0): void => {
    let url = this.url + this.path;

    this.httpClient
      .get<Page<T, ID>>(url, {
        params: new HttpParams()
          .set('page', page.toString())
          .set('size', this.pageSize.toString()),
      })
      .subscribe(
        (page) => {
          if (this._addPage(page)) this.notify();
        },
        (err) => {
          this.showError(err);
        }
      );
  };

  //   read pages
  public findPageWithPageNumberAndSize = (
    page: number = 0,
    pageSize: number = 20,
    criteriaValue?: string
  ): Observable<Page<T, ID>> => {
    let url = this.url + this.path;
    if (
      criteriaValue &&
      criteriaValue !== '' &&
      criteriaValue.replace(/\s/g, '')
    )
      url = this.url + this.criteriaPath + criteriaValue;

    let observable: Observable<Page<T, ID>> = this.httpClient.get<Page<T, ID>>(
      url,
      {
        params: new HttpParams()
          .set('page', page.toString())
          .set('size', pageSize.toString()),
      }
    );

    observable.subscribe(
      (page) => {
        if (this._addPage(page)) this.notify();
      },
      (err) => {
        this.showError(err);
      }
    );

    return observable;
  };

  // read pages
  public findPageWithPageNumberAndSizeAndSort = (
    page: number = 0,
    pageSize: number = 20,
    sortParams: SortParam[] = [],
    criteriaValue?: string
  ): Observable<Page<T, ID>> => {
    let url = this.url + this.path;
    if (
      criteriaValue &&
      criteriaValue !== '' &&
      criteriaValue.replace(/\s/g, '')
    )
      url = this.url + this.criteriaPath + criteriaValue;

    let httpParams: HttpParams = new HttpParams()
      .set('page', page.toString())
      .set('size', pageSize.toString());
    sortParams.forEach((sortParam: SortParam) => {
      httpParams = httpParams.append(
        'sort',
        sortParam.field + ',' + sortParam.order
      );
    });

    let observable = this.httpClient.get<Page<T, ID>>(url, {
      params: httpParams,
    });

    observable.subscribe(
      (page) => {
        if (this._addPage(page)) this.notify();
      },
      (err) => {
        this.showError(err);
      }
    );

    return observable;
  };

  protected showError = (err: any): void => {
    console.log(err);
  };

  public getPages = (): Map<PageKey, Page<T, ID>> => {
    // vracamo clone
    return this.pages.clone();
  };

  private _addPage = (page: Page<T, ID>): boolean => {
    if (
      !this.pages.has({
        pageNumber: page.pageable.pageNumber,
        pageSize: page.pageable.pageSize,
        sort: page.pageable.sort,
      })
    ) {
      this.pages.getMap().set(
        {
          pageNumber: page.pageable.pageNumber,
          pageSize: page.pageable.pageSize,
          sort: page.pageable.sort,
        },
        page
      );
      return true;
    }
    return false;
  };

  public getPageSize = (): number => {
    return this.pageSize;
  };

  public setPageSize = (pageSize: number): void => {
    this.pageSize = pageSize;
  };

  public setPath = (path: string): void => {
    this.path = path;
  };

  public setCriteriaPath = (criteriaPath: string): void => {
    this.criteriaPath = criteriaPath;
  };

  public resetPathAndCriteriaPathToDefault = (): void => {
    this.path = this._defaultPath;
    this.criteriaPath = this._defaultCriteriaPath;
  };

  public buildEntitet = (e: ENTITET_MODEL_INTERFACE): Entitet<T, ID> => {
    return this.factory.build(e);
  };
}
