package isa.app.configuration;

import javax.jms.Topic;

import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TopicConfiguration {
    
    @Bean
    public Topic receivedInformation() {
	return new ActiveMQTopic("receivedInformation");
    }

}
