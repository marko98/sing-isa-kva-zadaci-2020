package com.isa.app.service;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.isa.app.model.ModelEntity;
import com.isa.app.repository.CrudRepository;

public abstract class CrudService<T extends ModelEntity<T>, ID extends Serializable> {
	
	@Autowired
	protected CrudRepository<T, ID> repository;

	public Page<T> findAll(Pageable pageable) {
		return this.repository.findAll(pageable);
	}
	
	public Optional<T> findOne(Integer identifikator) {
		return this.repository.findOne((ID) identifikator);
	}
	
	public void save(T model) {
		this.repository.save(model);
	}
	
	public void delete(T model) {
		this.repository.delete(model);
	}
	
	public void delete(int identifikator) {
		this.repository.delete(identifikator);
	}
}
