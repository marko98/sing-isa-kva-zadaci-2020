package isa.plugin.prodavnice.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.controller.CrudController;
import isa.model.Prodavnice.Roba;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/prodavnice_roba")
@CrossOrigin(origins = "*")

public class RobaController extends CrudController<Roba, Long> {

}