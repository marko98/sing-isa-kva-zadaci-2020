import { Entitet } from '../../../model/interface/entitet.interface';
import { Prototype } from '../../creational/prototype/prototype.interface';
import { PageKey, Page } from '../../../service/pageable-crud.service';

export class PageMap<T extends Entitet<T, ID>, ID> implements Prototype {
  private map: Map<PageKey, Page<T, ID>> = new Map();

  /**
   * prva implementacija metode nikada ne prazni mapu iako se promeni kriterijum za
   * dobavljanje entiteta
   */
  // has = (key: PageKey): boolean => {
  //   // iteriramo kroz sve elemente
  //   for (const [existingKey, value] of this.map) {
  //     if (
  //       existingKey.pageNumber === key.pageNumber &&
  //       existingKey.pageSize === key.pageSize &&
  //       existingKey.sort.empty === key.sort.empty &&
  //       existingKey.sort.sorted === key.sort.sorted &&
  //       existingKey.sort.unsorted === key.sort.unsorted
  //     )
  //       return true;
  //   }
  //   return false;
  // };

  /**
   * druga implementacija metode prazni mapu ako dodje do promene kriterijuma za dobavljanje entiteta
   */
  has = (key: PageKey): boolean => {
    if (!this._hasCriteriaChanged(key)) {
      for (const [existingKey, value] of this.map) {
        if (existingKey.pageNumber === key.pageNumber) return true;
      }
    } else {
      this.map.clear();
      return false;
    }
  };

  private _hasCriteriaChanged = (key: PageKey): boolean => {
    for (const [existingKey, value] of this.map) {
      if (
        existingKey.pageSize === key.pageSize &&
        existingKey.sort.empty === key.sort.empty &&
        existingKey.sort.sorted === key.sort.sorted &&
        existingKey.sort.unsorted === key.sort.unsorted
      )
        return false;
      else return true;
    }
  };

  getMap = (): Map<PageKey, Page<T, ID>> => {
    return this.map;
  };

  clone = (): Map<PageKey, Page<T, ID>> => {
    // vracamo clone
    return new Map(this.map);
  };

  clear = (): void => {
    this.map.clear();
  };
}
