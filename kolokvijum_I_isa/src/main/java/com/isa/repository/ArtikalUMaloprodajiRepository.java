package com.isa.repository;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.isa.model.ArtikalUMaloprodaji;

@Repository
@Scope("singleton")
public class ArtikalUMaloprodajiRepository extends CrudRepository<ArtikalUMaloprodaji, Long> {

	public List<ArtikalUMaloprodaji> findAllOnDiscount() {
//		get session
		Session session = this.hibernateSessionFactory.getSession();
		
//		begin transaction
		session.beginTransaction();
		
//		crud
		
		Query<ArtikalUMaloprodaji> queryA = session.createQuery("select a from ArtikalUMaloprodaji as a ", ArtikalUMaloprodaji.class);
		List<ArtikalUMaloprodaji> artikli = queryA.getResultList();
		
		List<ArtikalUMaloprodaji> naPopustu = new ArrayList<ArtikalUMaloprodaji>();
		for (ArtikalUMaloprodaji a : artikli) {
			Query<ArtikalUMaloprodaji> qA = session.createQuery("select a from ArtikalUMaloprodaji as a "
																+ "JOIN a.popusti p "
																+ "WHERE p.artikal_u_maloprodaji_id = :id", ArtikalUMaloprodaji.class);
			qA.setParameter("id", a.getId());
			naPopustu.add(qA.getSingleResult());
		}
		
//		Query<ArtikalUMaloprodaji> qA = session.createQuery("select a from ArtikalUMaloprodaji as a "
//				+ "JOIN a.popusti p "
//				+ "WHERE p.artikal_u_maloprodaji_id = a.id", ArtikalUMaloprodaji.class);
//		List<ArtikalUMaloprodaji> naPopustu = qA.getResultList();
		
//		commit transaction
		session.getTransaction().commit();
		
//		close session
		session.close();
		
		return naPopustu;
	}

}
