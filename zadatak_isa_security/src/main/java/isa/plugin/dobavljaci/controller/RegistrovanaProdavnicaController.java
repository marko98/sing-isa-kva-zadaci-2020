package isa.plugin.dobavljaci.controller;

import java.util.HashMap;
import java.util.HashSet;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import isa.controller.CrudController;
import isa.dto.Dobavljaci.RegistrovanaProdavnicaDTO;
import isa.hbsf.HibernateSessionFactory;
import isa.model.Dobavljaci.RegistrovanaProdavnica;
import isa.model.Dobavljaci.RegistrovanaProdavnicaRola;
import isa.model.Dobavljaci.Rola;
import isa.plugin.dobavljaci.service.RegistrovanaProdavnicaService;
import isa.plugin.dobavljaci.utils.TokenUtils;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/registrovana_prodavnica")
@CrossOrigin(origins = "*")

public class RegistrovanaProdavnicaController extends CrudController<RegistrovanaProdavnica, Long> {
    
    @Autowired
    private RegistrovanaProdavnicaService registrovanaProdavnicaService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;
    
  @Autowired
  private TokenUtils tokenUtils;

    @Autowired
    private HibernateSessionFactory hibernateSessionFactory;

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity<RegistrovanaProdavnicaDTO> register(
	    @RequestBody RegistrovanaProdavnica registrovanaProdavnica) {

	registrovanaProdavnica.setLozinka(passwordEncoder.encode(registrovanaProdavnica.getLozinka()));

	this.service.save(registrovanaProdavnica);
	registrovanaProdavnica.setRegistrovanaProdavnicaRole(new HashSet<RegistrovanaProdavnicaRola>());

//	get session
	Session session = this.hibernateSessionFactory.getSession();
//	begin transaction
	session.beginTransaction();

	registrovanaProdavnica.getRegistrovanaProdavnicaRole()
		.add(new RegistrovanaProdavnicaRola(null, registrovanaProdavnica, session.get(Rola.class, 1l)));

//	commit transaction
	session.getTransaction().commit();

//	close session
	session.close();

	this.service.save(registrovanaProdavnica);

	return new ResponseEntity<RegistrovanaProdavnicaDTO>(registrovanaProdavnica.getDTO(), HttpStatus.OK);
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, String>> login(@RequestBody RegistrovanaProdavnica registrovanaProdavnica) {
	try {
	    /**
	     * kreiramo specificniju autentifikaciju(UsernamePasswordAuthenticationToken,
	     * koja implementira interfejs -> Authentication)
	     */
	    UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
		    registrovanaProdavnica.getId().toString(), registrovanaProdavnica.getLozinka());

	    /**
	     * authenticationManager(AuthenticationManager) sluzi da autentifikuje neku
	     * autentifikaciju koju mu mi prosledimo
	     * 
	     * vraca najopstiji oblik, Authentication, autentifikacije u koje sve
	     * implementacije(konkretnije implmentacije interfejs-a Authentication) mogu da
	     * se svrstaju
	     */
	    Authentication authentication = authenticationManager.authenticate(token);
	    /**
	     * SecurityContextHolder je vezan za thread, mi stoga dohvatamo njegov sadrzaj i
	     * setujemo nasu autentifikaciju koju je vratio authenticationManager
	     * 
	     * SecurityContextHolder podesen je da radi na globalnom nivou zbog anotacije
	     * (@EnableGlobalMethodSecurity(securedEnabled = true) u
	     * rs.ac.singidunum.banka.security.SecurityConfiguration)
	     */
	    SecurityContextHolder.getContext().setAuthentication(authentication);

	    /**
	     * UserDetailsService je interfejs koji sluzi za dobavljanje podataka o user-u,
	     * izgleda da ce ovde ipak biti injektovana nasa implementacija tog interfejsa
	     * (rs.ac.singidunum.banka.services.UserDetailsServiceImpl)
	     */
	    UserDetails details = registrovanaProdavnicaService.loadUserByUsername(registrovanaProdavnica.getId().toString());
	    String userToken = tokenUtils.generateToken(details);

	    HashMap<String, String> data = new HashMap<String, String>();
	    data.put("token", userToken);

	    return new ResponseEntity<HashMap<String, String>>(data, HttpStatus.OK);

	} catch (Exception e) {
	    return new ResponseEntity<HashMap<String, String>>(HttpStatus.UNAUTHORIZED);
	}
    }

}