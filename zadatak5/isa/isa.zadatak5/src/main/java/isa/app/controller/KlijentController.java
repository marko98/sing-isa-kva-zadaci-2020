package isa.app.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.app.model.Klijent;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/klijent")
@CrossOrigin(origins = "*")
public class KlijentController extends CrudController<Klijent, Long> {

}