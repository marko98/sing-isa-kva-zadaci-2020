package isa.plugin.dobavljaci.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import isa.model.Dobavljaci.RegistrovanaProdavnica;
import isa.model.Dobavljaci.RegistrovanaProdavnicaRola;
import isa.service.CrudService;

@Service
@Scope("singleton")

public class RegistrovanaProdavnicaService extends CrudService<RegistrovanaProdavnica, Long> implements UserDetailsService {

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
	Optional<RegistrovanaProdavnica> registrovanaProdavnica = this.findOne(Long.parseLong(id));

	if (registrovanaProdavnica.isPresent()) {
	    /**
	     * buduci da u UserDetails interfejsu imamo username, password i listu
	     * GrantedAuthority-a(interfejs koji ima metodu getAuthority koja vraca String),
	     */
	    ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
	    for (RegistrovanaProdavnicaRola registrovanaProdavnicaRola : registrovanaProdavnica.get().getRegistrovanaProdavnicaRole()) {
		/**
		 * kreiramo instancu klase SimpleGrantedAuthority koja implemetira
		 * GrantedAuthority interfejs i kao argument prima role tipa String
		 */
		grantedAuthorities.add(new SimpleGrantedAuthority(registrovanaProdavnicaRola.getRola().getTitle()));
	    }

	    /**
	     * kreiramo instancu klase User, koja implmentira interfejs UserDetails i kao
	     * argumente prima username, password i listu GrantedAuthority-a, koju smo
	     * prethodno kreirali
	     */
	    return new org.springframework.security.core.userdetails.User(registrovanaProdavnica.get().getId().toString(),
		    registrovanaProdavnica.get().getLozinka(), grantedAuthorities);
	}

	return null;
    }
    
}