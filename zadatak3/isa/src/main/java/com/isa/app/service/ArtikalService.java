package com.isa.app.service;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.isa.app.model.Artikal;
import com.isa.app.repository.ArtikalRepository;

@Service
@Scope("singleton")
public class ArtikalService extends CrudService<Artikal, Integer> {
	
	public List<Artikal> findByPrice(Double donjaCena, Double gornjaCena) {
		return ((ArtikalRepository) this.repository).findByPrice(gornjaCena, donjaCena);
	}
	
}