package isa.app.model;

import java.util.ArrayList;
import java.util.List;

import isa.app.dto.KreditDTO;
import isa.app.dto.KreditKlijentDTO;

@javax.persistence.Entity
public class Kredit implements Entity<Kredit, Long> {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Long id;

    private Double kolicina;

    private Integer trajanjeUMesecima;

    private Double kamatnaStopa;

    @javax.persistence.OneToMany(mappedBy = "kredit")
    private java.util.List<KreditKlijent> kreditiKlijenti;

    @javax.persistence.ManyToOne
    private Banka banka;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Double getKolicina() {
	return this.kolicina;
    }

    public void setKolicina(Double kolicina) {
	this.kolicina = kolicina;
    }

    public Integer getTrajanjeUMesecima() {
	return this.trajanjeUMesecima;
    }

    public void setTrajanjeUMesecima(Integer trajanjeUMesecima) {
	this.trajanjeUMesecima = trajanjeUMesecima;
    }

    public Double getKamatnaStopa() {
	return this.kamatnaStopa;
    }

    public void setKamatnaStopa(Double kamatnaStopa) {
	this.kamatnaStopa = kamatnaStopa;
    }

    public java.util.List<KreditKlijent> getKreditiKlijenti() {
	return this.kreditiKlijenti;
    }

    public void setKreditiKlijenti(java.util.List<KreditKlijent> kreditiKlijenti) {
	this.kreditiKlijenti = kreditiKlijenti;
    }

    public Banka getBanka() {
	return this.banka;
    }

    public void setBanka(Banka banka) {
	this.banka = banka;
    }

    public Kredit() {
	super();
	this.kreditiKlijenti = new ArrayList<KreditKlijent>();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((banka == null) ? 0 : banka.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((kamatnaStopa == null) ? 0 : kamatnaStopa.hashCode());
	result = prime * result + ((kolicina == null) ? 0 : kolicina.hashCode());
	result = prime * result + ((kreditiKlijenti == null) ? 0 : kreditiKlijenti.hashCode());
	result = prime * result + ((trajanjeUMesecima == null) ? 0 : trajanjeUMesecima.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Kredit other = (Kredit) obj;
	if (banka == null) {
	    if (other.banka != null)
		return false;
	} else if (!banka.equals(other.banka))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (kamatnaStopa == null) {
	    if (other.kamatnaStopa != null)
		return false;
	} else if (!kamatnaStopa.equals(other.kamatnaStopa))
	    return false;
	if (kolicina == null) {
	    if (other.kolicina != null)
		return false;
	} else if (!kolicina.equals(other.kolicina))
	    return false;
	if (kreditiKlijenti == null) {
	    if (other.kreditiKlijenti != null)
		return false;
	} else if (!kreditiKlijenti.equals(other.kreditiKlijenti))
	    return false;
	if (trajanjeUMesecima == null) {
	    if (other.trajanjeUMesecima != null)
		return false;
	} else if (!trajanjeUMesecima.equals(other.trajanjeUMesecima))
	    return false;
	return true;
    }

    @Override
    public KreditDTO getDTO() {
	KreditDTO kreditDTO = new KreditDTO();
	kreditDTO.setId(id);
	kreditDTO.setKamatnaStopa(kamatnaStopa);
	kreditDTO.setKolicina(kolicina);
	kreditDTO.setTrajanjeUMesecima(trajanjeUMesecima);

	/*
	 * BITNO!!!
	 * 
	 * za slozene tipove koji su opcioni za entitet, a nisu liste, pre poziva bilo
	 * kakvih metoda nad njima(npr. getDTOinsideDTO()) proveriti da li su razliciti
	 * od null
	 */
	if (banka != null)
	    kreditDTO.setBankaDTO(banka.getDTOinsideDTO());

	List<KreditKlijentDTO> kreditiKlijentiDTO = new ArrayList<KreditKlijentDTO>();
	for (KreditKlijent kreditKlijent : kreditiKlijenti) {
	    kreditiKlijentiDTO.add(kreditKlijent.getDTOinsideDTO());
	}
	kreditDTO.setKreditiKlijentiDTO(kreditiKlijentiDTO);

	return kreditDTO;
    }

    @Override
    public KreditDTO getDTOinsideDTO() {
	KreditDTO kreditDTO = new KreditDTO();
	kreditDTO.setId(id);
	kreditDTO.setKamatnaStopa(kamatnaStopa);
	kreditDTO.setKolicina(kolicina);
	kreditDTO.setTrajanjeUMesecima(trajanjeUMesecima);

	return kreditDTO;
    }

    @Override
    public void update(Kredit entity) {
	/*
	 * BITNO!!! (primer za entitet Banka)
	 * 
	 * kod polja koja su liste imamo problem -> recimo da zelimo da update-ujemo
	 * banku, ali samo njen naziv mi onda moramo da dostavimo i sve klijente i sve
	 * kredite, ako sa druge strane izbacimo iz ove metode setovanje polja koje su
	 * liste, onda mozemo bez problema update-ovati samo naziv banke ili jos neka
	 * polja ako ih bude imala bez da moramo da dovlacimo sve klijente i kredite
	 * 
	 * ako zelimo da iz banke izbacimo neki krediti onda buduci da banka ima vise
	 * kredita, a kredit ima jednu banku, update-ujemo taj kredit odnosno kod njega
	 * za polje banka stavljamo null ili ga u potpunosti brisemo isto vazi i za
	 * klijene, medjutim da bi to moglo tako da radi ne smemo imati vezu izmedju dve
	 * klase vise na prema vise, vec ubaciti izmedju klasu koja ce ih 'spajati', ta
	 * dodatna klasa bi trebala da ima kardinalitete prema ove dve klase 1 na prema
	 * vise ili slicno, ali nikako vise na prema vise
	 * 
	 * dakle: kod update-a ne update-ujemo liste SLOZENIH tipova(entiteta), ako ima
	 * potrebe za izbacivanjem nekih ili dodavanjem nekih u listu obracamo se ili
	 * njima(slucaj jedan na prema vise) ili dodatnoj klasi koja je spoj izmedju
	 * entiteta koji 'imaju' kardinalitet vise na prema vise
	 */

	this.setBanka(entity.getBanka());
	this.setKamatnaStopa(entity.getKamatnaStopa());
	this.setKolicina(entity.getKolicina());
	this.setTrajanjeUMesecima(entity.getTrajanjeUMesecima());
    }

}