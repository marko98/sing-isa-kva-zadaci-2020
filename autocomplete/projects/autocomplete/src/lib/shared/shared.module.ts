import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material.module';
import { HeightDirective } from './directive/height.directive';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { WidthDirective } from './directive/width.directive';
import { OpacityTransitionDirective } from './directive/opacity-transition.directive';

@NgModule({
  declarations: [HeightDirective, WidthDirective, OpacityTransitionDirective],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    ScrollingModule,
    DragDropModule,
  ],
  exports: [
    HeightDirective,
    WidthDirective,
    OpacityTransitionDirective,

    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    ScrollingModule,
    DragDropModule,
  ],
  entryComponents: [],
})
export class SharedModule {}
