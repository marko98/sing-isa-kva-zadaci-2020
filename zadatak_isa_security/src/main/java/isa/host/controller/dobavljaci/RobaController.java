package isa.host.controller.dobavljaci;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.host.controller.HostCrudController;
import isa.model.Dobavljaci.Roba;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/roba")
@CrossOrigin(origins = "*")
public class RobaController extends HostCrudController<Roba, Long> {

    public RobaController() {
	this.category = "roba";
    }

}