package com.isa.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.isa.model.StavkaRacuna;

@Service
@Scope("singleton")
public class StavkaRacunaService extends CrudService<StavkaRacuna, Long> {

}
