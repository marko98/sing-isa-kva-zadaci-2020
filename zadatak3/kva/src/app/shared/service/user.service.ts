import { Injectable } from '@angular/core';

// repository
import { UserRepository } from '../repository/user.repository';

// model
import { User } from '../model/user.model';
import { CRUDService } from './CRUD.service';

@Injectable()
export class UserService extends CRUDService<User> {
    
    constructor(private userRepository: UserRepository){
        super(userRepository, 'http://localhost:8080/isa.zadatak.3/api/korisnici/');

        // buduci da je u pitanju servis on se nikada ne unistava
        // tako da nema potrebe da se unsubscribe-ujemo(dettach())
        this.userRepository.attach(this.userObserver);
    }

    getLoggedUser = (): User => {
        return this.userRepository.getLoggedUser();
    }

    setLoggedUser = (user: User): void => {
        this.userRepository.setLoggedUser(user);
    }

    // observer function
    userObserver = (observable: UserRepository): void => {
        // console.log("UserService notified about changes in UserRepository");
        // console.log(observable);
        this.notify();
    }

    // override-ujemo
    notify = () => {
        this.observers.forEach(observer => {
            observer(this);
        });
    }

}