package lms.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.transaction.Transactional;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import lms.dto.AdresaDTO;
import lms.enums.StanjeModela;
import lms.enums.TipAdrese;
import lms.model.interfaces.Entitet;

@javax.persistence.Entity

@SQLDelete(sql = "UPDATE adresa SET stanje = 'OBRISAN' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Table(name = "adresa")
@Transactional
public class Adresa implements Entitet<Adresa, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -9169584430543219253L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "adresa_id")
    private Long id;

    private String ulica;

    private String broj;

    @Enumerated(EnumType.STRING)
    private StanjeModela stanje;

    @Version
    private Long version;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "grad_id")
    private Grad grad;

    @Enumerated(EnumType.STRING)
    private TipAdrese tip;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getUlica() {
	return this.ulica;
    }

    public void setUlica(String ulica) {
	this.ulica = ulica;
    }

    public String getBroj() {
	return this.broj;
    }

    public void setBroj(String broj) {
	this.broj = broj;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public Grad getGrad() {
	return this.grad;
    }

    public void setGrad(Grad grad) {
	this.grad = grad;
    }

    public TipAdrese getTip() {
	return this.tip;
    }

    public void setTip(TipAdrese tip) {
	this.tip = tip;
    }

    public Adresa() {
	super();
    }

    @PreRemove
    public void onDelete() {
	this.stanje = StanjeModela.OBRISAN;
    }

    @PrePersist
    public void onPersist() {
	this.stanje = StanjeModela.AKTIVAN;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((broj == null) ? 0 : broj.hashCode());
	result = prime * result + ((grad == null) ? 0 : grad.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((tip == null) ? 0 : tip.hashCode());
	result = prime * result + ((ulica == null) ? 0 : ulica.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Adresa other = (Adresa) obj;
	if (broj == null) {
	    if (other.broj != null)
		return false;
	} else if (!broj.equals(other.broj))
	    return false;
	if (grad == null) {
	    if (other.grad != null)
		return false;
	} else if (!grad.equals(other.grad))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (stanje != other.stanje)
	    return false;
	if (tip != other.tip)
	    return false;
	if (ulica == null) {
	    if (other.ulica != null)
		return false;
	} else if (!ulica.equals(other.ulica))
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }

    @Override
    public AdresaDTO getDTO() {
	AdresaDTO adresaDTO = new AdresaDTO();
	adresaDTO.setId(id);
	adresaDTO.setBroj(broj);
	adresaDTO.setStanje(stanje);
	adresaDTO.setTip(tip);
	adresaDTO.setUlica(ulica);
	adresaDTO.setVersion(version);

	if (this.grad != null)
	    adresaDTO.setGradDTO(this.grad.getDTOinsideDTO());

	return adresaDTO;
    }

    @Override
    public AdresaDTO getDTOinsideDTO() {
	AdresaDTO adresaDTO = new AdresaDTO();
	adresaDTO.setId(id);
	adresaDTO.setBroj(broj);
	adresaDTO.setStanje(stanje);
	adresaDTO.setTip(tip);
	adresaDTO.setUlica(ulica);
	adresaDTO.setVersion(version);

	return adresaDTO;
    }

    @Override
    public void update(Adresa entitet) {
	this.setBroj(broj);
	this.setStanje(stanje);
	this.setTip(tip);
	this.setUlica(ulica);
	this.setVersion(version);

	this.setGrad(entitet.getGrad());
    }

}