package spring.zadatak2.aspect;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import spring.zadatak2.service.KorisnikService;

@Aspect
@Component
public class AuthLogger {
	@Autowired
	private KorisnikService korisnikService;
	
	@Pointcut("within(spring.zadatak2.controller..*)")
	public void withinControllers() { }
	
	@Pointcut("within(spring.zadatak2.controller.KorisnikController)")
	public void withinKorisnikController() { }
	
	@Pointcut("!within(spring.zadatak2.controller.KorisnikController)")
	public void notWithinKorisnikController() { }
	
	@Around("withinControllers() && notWithinKorisnikController() &&"
			+ " execution(public * spring.zadatak2.controller.*.*(..))")
	public ResponseEntity<?> authCheck(ProceedingJoinPoint jp) {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		String identifikator = request.getHeader("identificator");		
		
		System.out.println("------------------");
		System.out.println("Pocetak izvrsavanja: ");
		System.out.println(jp.getSignature());
		
		ResponseEntity<?> result;
		
		try {
			if (this.korisnikService.isAuth(identifikator)) {
				System.out.println("Korisnik sa identifikatorom: " + identifikator + " je autentifikovan");
				result =  (ResponseEntity<?>)jp.proceed();
				System.out.println("Rezultat izvrsavanja: ");
				System.out.println(result);
			} else {
				System.out.println("Korisnik sa identifikatorom: " + identifikator + " nije autentifikovan");
				result = new ResponseEntity<Object>("Unauthorized", HttpStatus.UNAUTHORIZED);
			}
		} catch (Throwable e) {
			e.printStackTrace();
			result = new ResponseEntity<Object>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} finally {
			System.out.println("Kraj izvrsavanja: ");
			System.out.println(jp.getSignature());
			System.out.println("------------------");
		}
		
		return result;
	}

}
