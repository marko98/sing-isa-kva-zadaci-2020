package isa.plugin.dobavljaci.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import isa.model.Dobavljaci.RobaUMagacinu;
import isa.repository.CrudRepository;

@Repository
@Scope("singleton")

public class RobaUMagacinuRepository extends CrudRepository<RobaUMagacinu, Long> {

}