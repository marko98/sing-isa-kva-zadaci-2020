package isa.model.Dobavljaci;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Table;

import isa.dto.Dobavljaci.RobaDTO;
import isa.dto.Dobavljaci.RobaUMagacinuDTO;
import isa.model.interfaces.Entitet;

@javax.persistence.Entity

@Table(name = "roba")
public class Roba implements Entitet<Roba, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -8425767709943996571L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "roba_id")
    private Long id;

    private String naziv;

    @javax.persistence.OneToMany(mappedBy = "roba")
    private java.util.List<RobaUMagacinu> robeUMagacinu;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return this.naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public java.util.List<RobaUMagacinu> getRobeUMagacinu() {
	return this.robeUMagacinu;
    }

    public void setRobeUMagacinu(java.util.List<RobaUMagacinu> robeUMagacinu) {
	this.robeUMagacinu = robeUMagacinu;
    }

    public Roba() {
	super();
	this.robeUMagacinu = new java.util.ArrayList<>();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
	result = prime * result + ((robeUMagacinu == null) ? 0 : robeUMagacinu.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Roba other = (Roba) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (naziv == null) {
	    if (other.naziv != null)
		return false;
	} else if (!naziv.equals(other.naziv))
	    return false;
	if (robeUMagacinu == null) {
	    if (other.robeUMagacinu != null)
		return false;
	} else if (!robeUMagacinu.equals(other.robeUMagacinu))
	    return false;
	return true;
    }

    @Override
    public RobaDTO getDTO() {
	RobaDTO robaDTO = new RobaDTO();
	robaDTO.setId(id);
	robaDTO.setNaziv(naziv);

	ArrayList<RobaUMagacinuDTO> robeUMagacinuDTOs = new ArrayList<RobaUMagacinuDTO>();
	for (RobaUMagacinu robaUMagacinu : this.robeUMagacinu) {
	    robeUMagacinuDTOs.add(robaUMagacinu.getDTOinsideDTO());
	}
	robaDTO.setRobeUMagacinuDTO(robeUMagacinuDTOs);

	return robaDTO;
    }

    @Override
    public RobaDTO getDTOinsideDTO() {
	RobaDTO robaDTO = new RobaDTO();
	robaDTO.setId(id);
	robaDTO.setNaziv(naziv);
	return robaDTO;
    }

    @Override
    public void update(Roba entitet) {
	this.setNaziv(entitet.getNaziv());

    }

}