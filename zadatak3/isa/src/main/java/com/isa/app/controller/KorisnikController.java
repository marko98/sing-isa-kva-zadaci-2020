package com.isa.app.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.isa.app.dto.DTO;
import com.isa.app.dto.LoginDTO;
import com.isa.app.model.Korisnik;
import com.isa.app.model.Uloga;
import com.isa.app.service.KorisnikService;

@Controller
@Scope("singleton")
@RequestMapping(path = "/api/korisnici")
//@CrossOrigin(origins = "http://localhost:4200/")
//@CrossOrigin
@CrossOrigin(origins = "*")
public class KorisnikController extends CrudController<Korisnik, Integer> {
		
//	obrati paznju da li postoji u generickom CRUDControlleru vec zadata putanja i metoda
	@RequestMapping(path = "/roles/{uloga}", method = RequestMethod.GET)
	public ResponseEntity<?> getAllByRole(@PathVariable("uloga") String ulogaString){
		Uloga uloga = null;
		if (ulogaString.equals("administrator") || ulogaString.equals("ADMINISTRATOR"))
			uloga = Uloga.ADMINISTRATOR;
		else if (ulogaString.equals("kupac") || ulogaString.equals("KUPAC")) 
			uloga = Uloga.KUPAC;
		
		if (uloga == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<DTO>>(((KorisnikService)this.service).findByRole(uloga).stream().map(t -> t.getDTO()).collect(Collectors.toList()), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> login(@RequestBody LoginDTO loginDTO){
		Korisnik k = ((KorisnikService) this.service).findByUsername(loginDTO.getKorisnickoIme());
		
		if (k == null)
			return new ResponseEntity<String>("User not found", HttpStatus.NOT_FOUND);
		
		boolean result = ((KorisnikService)this.service).login(k, loginDTO.getLozinka());
		
		if (result)
			return new ResponseEntity<Integer>(k.getId(), HttpStatus.OK);
		
		return new ResponseEntity<String>("Username/password is invalid", HttpStatus.EXPECTATION_FAILED);
	}
	
	@RequestMapping(path = "/logout/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> login(@PathVariable("id") int id){
		Optional<Korisnik> k = this.service.findOne(id);
		if (k.isEmpty())
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		
		((KorisnikService) this.service).logout(k.get());
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
}
