package isa.host.controller.dobavljaci;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import isa.host.controller.HostCrudController;
import isa.model.Dobavljaci.RegistrovanaProdavnica;
import plugin.model.Plugin;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/registrovana_prodavnica")
@CrossOrigin(origins = "*")

public class RegistrovanaProdavnicaController extends HostCrudController<RegistrovanaProdavnica, Long> {

    public RegistrovanaProdavnicaController() {
	this.category = "registrovana_prodavnica";
    }
    
    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody RegistrovanaProdavnica model, @RequestHeader HttpHeaders httpHeaders) {
	Plugin plugin = pr.getPlugins(this.category).get(0);
	return plugin.create(this.category, HttpMethod.POST, "register", model, httpHeaders);
    }
    
    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody RegistrovanaProdavnica model, @RequestHeader HttpHeaders httpHeaders) {
	Plugin plugin = pr.getPlugins(this.category).get(0);
	return plugin.create(this.category, HttpMethod.POST, "login", model, httpHeaders);
    }

}