import { Component, OnInit, OnDestroy } from '@angular/core';

// model
import { User } from '../shared/model/user.model';

// service
import { UserService } from '../shared/service/user.service';

@Component({
    selector: 'app-articles',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {
  public users: User[] = [];
  public userToUpdate: User;
  public shouldUpdate: boolean = false;
  public shouldCreate: boolean = false;

  constructor(private userService: UserService){} 

  onAddUser = (): void => {
    this.shouldCreate = true;
  }

  onUpdateUser = (user: User): void => {
    this.userToUpdate = user;
    this.shouldUpdate = true;
  }

  onDone = (): void => {
    this.shouldCreate = false;
    this.shouldUpdate = false;
    this.userToUpdate = null;
  }

  onDeleteUser = (identificator: string): void => {
    this.userService.deleteByIdentificatior(identificator);
  }

  /*
    observers nisu nista drugo do same funkcije koja treba da se pozove kada nad
    objektom tipa Observable na koju smo subscribe-ovani dodje do promena
  */
  userServiceObserver = (observable: UserService): void => {
    // console.log("AppComponent notified about changes in UserService");
    // console.log(observable);
    this.users = observable.findAll();
  }

  // interfaces
  ngOnInit() {
    this.userService.attach(this.userServiceObserver);

    this.users = this.userService.findAll();
    // console.log("UsersComponent init");
  }

  ngOnDestroy() {
    if (this.userService)
      this.userService.dettach(this.userServiceObserver);

    // console.log("UsersComponent destroyed");
  }
}