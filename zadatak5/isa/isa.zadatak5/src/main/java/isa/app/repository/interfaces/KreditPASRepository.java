package isa.app.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import isa.app.model.Kredit;

@Repository
@Scope("singleton")
public interface KreditPASRepository extends PagingAndSortingRepository<Kredit, Long> {

}