import { ENTITET_MODEL_INTERFACE } from 'projects/autocomplete/src/public-api';

export declare interface DRZAVA_MODEL_INTERFACE
  extends ENTITET_MODEL_INTERFACE {
  naziv: string;
}
