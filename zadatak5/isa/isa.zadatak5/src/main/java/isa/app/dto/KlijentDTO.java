package isa.app.dto;

import java.util.ArrayList;

public class KlijentDTO extends DTO {

    /**
     * 
     */
    private static final long serialVersionUID = -4677609508102008777L;

    private Long id;

    private String ime;

    private String prezime;

    private Boolean zaposlen;

    private Double mesecnoPrimanje;

    private BankaDTO bankaDTO;

    private java.util.List<KreditKlijentDTO> kreditiKlijentiDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getIme() {
	return this.ime;
    }

    public void setIme(String ime) {
	this.ime = ime;
    }

    public String getPrezime() {
	return this.prezime;
    }

    public void setPrezime(String prezime) {
	this.prezime = prezime;
    }

    public Boolean getZaposlen() {
	return this.zaposlen;
    }

    public void setZaposlen(Boolean zaposlen) {
	this.zaposlen = zaposlen;
    }

    public Double getMesecnoPrimanje() {
	return this.mesecnoPrimanje;
    }

    public void setMesecnoPrimanje(Double mesecnoPrimanje) {
	this.mesecnoPrimanje = mesecnoPrimanje;
    }

    public BankaDTO getBankaDTO() {
	return this.bankaDTO;
    }

    public void setBankaDTO(BankaDTO bankaDTO) {
	this.bankaDTO = bankaDTO;
    }

    public java.util.List<KreditKlijentDTO> getKreditiKlijentiDTO() {
	return this.kreditiKlijentiDTO;
    }

    public void setKreditiKlijentiDTO(java.util.List<KreditKlijentDTO> kreditiKlijentiDTO) {
	this.kreditiKlijentiDTO = kreditiKlijentiDTO;
    }

    public KlijentDTO() {
	super();
	this.kreditiKlijentiDTO = new ArrayList<KreditKlijentDTO>();
    }

}