import { Component, OnInit, OnDestroy } from '@angular/core';

// model
import { User } from '../shared/model/user.model';
import { Table, Row } from 'my-angular-table';

// service
import { UserService } from '../shared/service/user.service';

@Component({
    selector: 'app-articles',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {
  public users: User[] = [];
  public userToUpdate: User;
  public shouldUpdate: boolean = false;
  public shouldCreate: boolean = false;

  public table: Table;

  constructor(private userService: UserService){} 

  onAddUser = (): void => {
    this.shouldCreate = true;
  }

  onUpdateUser = (users: User[]): void => {
    this.userToUpdate = users[0];
    this.shouldUpdate = true;
  }

  onDone = (): void => {
    this.shouldCreate = false;
    this.shouldUpdate = false;
    this.userToUpdate = null;
  }

  onDeleteUser = (users: User[]): void => {
    // console.log(users);
    users.forEach(user => {
      // console.log(user);
      this.userService.deleteByIdentificatior(user.getIdentificator());
    });
  }

  /*
    observers nisu nista drugo do same funkcije koja treba da se pozove kada nad
    objektom tipa Observable na koju smo subscribe-ovani dodje do promena
  */
  userServiceObserver = (observable: UserService): void => {
    // console.log("AppComponent notified about changes in UserService");
    // console.log(observable);
    this.users = observable.findAll();

    this.fillUpdateTable();
  }

  fillUpdateTable = (): void => {
    // deleting
    this.table.getChildren().forEach((r: Row) => this.table.removeChild(r));

    // adding
    this.users.forEach((user: User) => {
      // console.log(article);

      this.table.addChildValue({
        data: user,
        rowItems: [
          {context: user.getFirstName(), imgSrc: "https://img.icons8.com/officel/2x/user.png"},
          {context: user.getLastName()},
          {context: user.getStringRole()},
          {context: user.getPassword(), imgSrc: "https://img.icons8.com/dusk/2x/password.png"},
          {context: "update", button: {function: this.onUpdateUser}, imgSrc: "https://img.icons8.com/nolan/2x/approve-and-update.png"},
          {context: "delete", button: {function: this.onDeleteUser}, imgSrc: "https://img.icons8.com/dusk/2x/filled-trash.png"}
        ]
      });
      
    });
  }

  // interfaces
  ngOnInit() {
    this.userService.attach(this.userServiceObserver);

    this.users = this.userService.findAll().filter((u: User) => u.getId() !== this.userService.getLoggedUser().getId());

    // create table
    this.table = new Table({
      header: {
        headerItems: [
          {context: "first name"},
          {context: "last name"},
          {context: "role"},
          {context: "password"},
          {context: ""},
          {context: ""}
        ]
      },
      contextMenu: {
        contextMenuItems: [
          {context: "update", function: this.onUpdateUser, imgSrc: "https://img.icons8.com/nolan/2x/approve-and-update.png"},
          {context: "delete", function: this.onDeleteUser, imgSrc: "https://img.icons8.com/dusk/2x/filled-trash.png"}
        ]
      }
    });

    this.fillUpdateTable();
    
    // console.log("UsersComponent init");
  }

  ngOnDestroy() {
    if (this.userService)
      this.userService.dettach(this.userServiceObserver);

    // console.log("UsersComponent destroyed");
  }
}