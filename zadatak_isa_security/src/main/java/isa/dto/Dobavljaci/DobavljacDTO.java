package isa.dto.Dobavljaci;

import isa.dto.interfaces.DTO;

public class DobavljacDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -5412077748059623475L;

    private Long id;
        
    private String naziv;
        
    private java.util.List<RegistrovanaProdavnicaDTO> registrovaneProdavniceDTO;
        
    private java.util.List<RobaUMagacinuDTO> robeUMagacinuDTO;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public java.util.List<RegistrovanaProdavnicaDTO> getRegistrovaneProdavniceDTO () {
        return this.registrovaneProdavniceDTO;
    }

    public void setRegistrovaneProdavniceDTO (java.util.List<RegistrovanaProdavnicaDTO> registrovaneProdavniceDTO) {
        this.registrovaneProdavniceDTO = registrovaneProdavniceDTO;
    }
    
    public java.util.List<RobaUMagacinuDTO> getRobeUMagacinuDTO () {
        return this.robeUMagacinuDTO;
    }

    public void setRobeUMagacinuDTO (java.util.List<RobaUMagacinuDTO> robeUMagacinuDTO) {
        this.robeUMagacinuDTO = robeUMagacinuDTO;
    }
    


    public DobavljacDTO () {
        super();
        this.registrovaneProdavniceDTO = new java.util.ArrayList<>();
        this.robeUMagacinuDTO = new java.util.ArrayList<>();
    }

}