package isa;

import isa.host.HostApp;
import isa.plugin.dobavljaci.PluginDobavljaciApp;
import isa.plugin.prodavnice.PluginProdavniceApp;

public class CoreApp {

    public static void main(String args[]) {
//	host
	HostApp.main(args);

//	plugins

	PluginProdavniceApp.main(args);
	PluginDobavljaciApp.main(args);
    }

}