package com.isa.app.model;

import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.isa.app.dto.ArtikalDTO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@Table(name = "artikal")
public class Artikal implements ModelEntity<Artikal> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "java.lang.Integer")
	@Column(name = "id")
	private Integer id;
	
	@Type(type = "string")
	@Column(name = "name")
	private String naziv;
	
	@Type(type = "string")
	@Column(name = "opis")
	private String opis;
	
	@Type(type = "java.lang.Double")
	@Column(name = "cena")
	private Double cena;
	
	@OneToMany(mappedBy = "artikal",
				fetch = FetchType.LAZY,
				cascade = CascadeType.ALL)
	private List<Stavka> stavke;
	
	public Artikal() {
		super();
	}

	public Artikal(String naziv, Double cena, String opis) {
		super();
		this.naziv = naziv;
		this.opis = opis;
		this.cena = cena;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public List<Stavka> getStavke() {
		return stavke;
	}

	public void setStavke(List<Stavka> stavke) {
		this.stavke = stavke;
	}
	
	public boolean addStavka(Stavka stavka) {
		if(this.stavke == null)
			this.stavke = new ArrayList<Stavka>();
		
		this.stavke.add(stavka);
		
		stavka.setArtikal(this);
		
		return true;
	}

	@Override
	public String toString() {
		return "Artikal [id=" + id + ", naziv=" + naziv + ", opis=" + opis + ", cena=" + cena + "]";
	}

//	------------------------ izgenerisi -----------------------------------
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artikal other = (Artikal) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
//	------------------------------------------------------------------------

	//	interface
	public Integer getIdentifikator() {
		return this.id;
	}
	
	public void update(Artikal artikal) {
		this.setNaziv(artikal.getNaziv());
		this.setCena(artikal.getCena());
		this.setOpis(artikal.getOpis());
	}
	
	public ArtikalDTO getDTO() {
		ArtikalDTO aDTO = new ArtikalDTO();
		aDTO.setNaziv(this.naziv);
		aDTO.setOpis(this.opis);
		aDTO.setCena(this.cena);
		aDTO.setId(this.id);
		return aDTO;
	}
}
