package spring.zadatak2.model;

import spring.zadatak2.dto.ArtikalDTO;

public class Artikal implements Entity<Artikal> {
	private String naziv, opis, identifikator;
	private Double cena;
	public static Integer id = 0;
	
	public Artikal() {}
	
	public Artikal(String naziv, Double cena, String opis) {
		this.naziv = naziv;
		this.opis = opis;
		this.cena = cena;
		this.identifikator = Integer.toString(Artikal.id++);
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public void setIdentifikator(String identifikator) {
		this.identifikator = identifikator;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}
	
//	interface
	public String getIdentifikator() {
		return identifikator;
	}
	
	public void update(Artikal artikal) {
		this.setNaziv(artikal.getNaziv());
		this.setCena(artikal.getCena());
		this.setOpis(artikal.getOpis());
	}
	
	public ArtikalDTO getDTO() {
		ArtikalDTO aDTO = new ArtikalDTO();
		aDTO.setNaziv(this.naziv);
		aDTO.setOpis(this.opis);
		aDTO.setCena(this.cena);
		aDTO.setIdentifikator(this.identifikator);
		return aDTO;
	}
}
