package isa.app.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.app.model.Banka;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/banka")
@CrossOrigin(origins = "*")
public class BankaController extends CrudController<Banka, Long> {

}