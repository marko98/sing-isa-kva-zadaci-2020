package com.isa.app.repository;

import java.util.List;

import javax.annotation.PostConstruct;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.isa.app.model.Artikal;

@Repository
@Scope("singleton")
public class ArtikalRepository extends CrudRepository<Artikal, Integer> {
	
	public ArtikalRepository() {
		super();
	}
	
	@PostConstruct
	private void onInit() {
//		get session
		Session session = this.hibernateSessionFactory.getSession();
		
//		begin transaction
		session.beginTransaction();
		
//		CRUD...
//		create user if no one exists
		
		Query<Artikal> query = session.createQuery("select a from Artikal a", Artikal.class);
		if(query.getResultList().isEmpty()) {
			session.save(new Artikal("Keks sa cokoladom", 49.99, "Najbolji keks sa cokoladom"));
			session.save(new Artikal("Voda", 19.99, "Voda voda"));
			session.save(new Artikal("Jabuka", 29.99, "Zelena jabuka"));
			session.save(new Artikal("Sok od kajsije", 79.99, "Sok od kajsije"));
		}
		
//		commit transaction
		session.getTransaction().commit();
		
//		close session
		session.close();
	}
	
	public List<Artikal> findByPrice(Double gornjaCena, Double donjaCena) {
//		get session
		Session session = this.hibernateSessionFactory.getSession();
		
//		begin transaction
		session.beginTransaction();
		
		Query<Artikal> query = session.createQuery("select a from Artikal a " +
													"where a.cena >= :donjaCena and a.cena <= :gornjaCena",
													Artikal.class);
		
		query.setParameter("donjaCena", donjaCena);
		query.setParameter("gornjaCena", gornjaCena);
		
		List<Artikal> artikali = query.getResultList();
		
//		commit transaction
		session.getTransaction().commit();
		
//		close session
		session.close();
		
		return artikali;
	}
}

