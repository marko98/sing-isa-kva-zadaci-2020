package isa.plugin.dobavljaci.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import isa.model.Dobavljaci.Roba;

@Repository
@Scope("singleton")

public interface RobaPASRepository extends PagingAndSortingRepository<Roba, Long> {

}