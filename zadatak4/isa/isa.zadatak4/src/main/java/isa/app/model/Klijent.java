package isa.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.GenerationType;

import isa.app.dto.KlijentDTO;
import isa.app.dto.KreditKlijentDTO;

@javax.persistence.Entity
public class Klijent implements Entity<Klijent, Long> {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String ime;

    private String prezime;

    private Boolean zaposlen;

    private Double mesecnoPrimanje;

    @javax.persistence.ManyToOne
    private Banka banka;

    @javax.persistence.OneToMany(mappedBy = "klijent")
    private java.util.List<KreditKlijent> kreditiKlijenti;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getIme() {
	return this.ime;
    }

    public void setIme(String ime) {
	this.ime = ime;
    }

    public String getPrezime() {
	return this.prezime;
    }

    public void setPrezime(String prezime) {
	this.prezime = prezime;
    }

    public Boolean getZaposlen() {
	return this.zaposlen;
    }

    public void setZaposlen(Boolean zaposlen) {
	this.zaposlen = zaposlen;
    }

    public Double getMesecnoPrimanje() {
	return this.mesecnoPrimanje;
    }

    public void setMesecnoPrimanje(Double mesecnoPrimanje) {
	this.mesecnoPrimanje = mesecnoPrimanje;
    }

    public Banka getBanka() {
	return this.banka;
    }

    public void setBanka(Banka banka) {
	this.banka = banka;
    }

    public java.util.List<KreditKlijent> getKreditiKlijenti() {
	return this.kreditiKlijenti;
    }

    public void setKreditiKlijenti(java.util.List<KreditKlijent> kreditiKlijenti) {
	this.kreditiKlijenti = kreditiKlijenti;
    }

    public Klijent() {
	super();
	this.kreditiKlijenti = new ArrayList<KreditKlijent>();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((banka == null) ? 0 : banka.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((ime == null) ? 0 : ime.hashCode());
	result = prime * result + ((kreditiKlijenti == null) ? 0 : kreditiKlijenti.hashCode());
	result = prime * result + ((mesecnoPrimanje == null) ? 0 : mesecnoPrimanje.hashCode());
	result = prime * result + ((prezime == null) ? 0 : prezime.hashCode());
	result = prime * result + ((zaposlen == null) ? 0 : zaposlen.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Klijent other = (Klijent) obj;
	if (banka == null) {
	    if (other.banka != null)
		return false;
	} else if (!banka.equals(other.banka))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (ime == null) {
	    if (other.ime != null)
		return false;
	} else if (!ime.equals(other.ime))
	    return false;
	if (kreditiKlijenti == null) {
	    if (other.kreditiKlijenti != null)
		return false;
	} else if (!kreditiKlijenti.equals(other.kreditiKlijenti))
	    return false;
	if (mesecnoPrimanje == null) {
	    if (other.mesecnoPrimanje != null)
		return false;
	} else if (!mesecnoPrimanje.equals(other.mesecnoPrimanje))
	    return false;
	if (prezime == null) {
	    if (other.prezime != null)
		return false;
	} else if (!prezime.equals(other.prezime))
	    return false;
	if (zaposlen == null) {
	    if (other.zaposlen != null)
		return false;
	} else if (!zaposlen.equals(other.zaposlen))
	    return false;
	return true;
    }

    @Override
    public KlijentDTO getDTO() {
	KlijentDTO klijentDTO = new KlijentDTO();
	klijentDTO.setId(id);

	klijentDTO.setIme(ime);
	klijentDTO.setPrezime(prezime);
	klijentDTO.setZaposlen(zaposlen);
	klijentDTO.setMesecnoPrimanje(mesecnoPrimanje);

	/*
	 * BITNO!!!
	 * 
	 * za slozene tipove koji su opcioni za entitet, a nisu liste, pre poziva bilo
	 * kakvih metoda nad njima(npr. getDTOinsideDTO()) proveriti da li su razliciti
	 * od null
	 */
	if (banka != null)
	    klijentDTO.setBankaDTO(banka.getDTOinsideDTO());

	List<KreditKlijentDTO> kreditiKlijentiDTO = new ArrayList<KreditKlijentDTO>();
	for (KreditKlijent kreditKlijent : kreditiKlijenti) {
	    kreditiKlijentiDTO.add(kreditKlijent.getDTOinsideDTO());
	}
	klijentDTO.setKreditiKlijentiDTO(kreditiKlijentiDTO);

	return klijentDTO;
    }

    @Override
    public KlijentDTO getDTOinsideDTO() {
	KlijentDTO klijentDTO = new KlijentDTO();
	klijentDTO.setId(id);
	klijentDTO.setIme(ime);
	klijentDTO.setPrezime(prezime);
	klijentDTO.setZaposlen(zaposlen);
	klijentDTO.setMesecnoPrimanje(mesecnoPrimanje);

	return klijentDTO;
    }
    
    @Override
    public void update(Klijent entity) {
	/*
	 * BITNO!!! (primer za entitet Banka)
	 * 
	 * kod polja koja su liste imamo problem -> recimo da zelimo da update-ujemo
	 * banku, ali samo njen naziv mi onda moramo da dostavimo i sve klijente i sve
	 * kredite, ako sa druge strane izbacimo iz ove metode setovanje polja koje su
	 * liste, onda mozemo bez problema update-ovati samo naziv banke ili jos neka
	 * polja ako ih bude imala bez da moramo da dovlacimo sve klijente i kredite
	 * 
	 * ako zelimo da iz banke izbacimo neki krediti onda buduci da banka ima vise
	 * kredita, a kredit ima jednu banku, update-ujemo taj kredit odnosno kod njega
	 * za polje banka stavljamo null ili ga u potpunosti brisemo isto vazi i za
	 * klijene, medjutim da bi to moglo tako da radi ne smemo imati vezu izmedju dve
	 * klase vise na prema vise, vec ubaciti izmedju klasu koja ce ih 'spajati', ta
	 * dodatna klasa bi trebala da ima kardinalitete prema ove dve klase 1 na prema
	 * vise ili slicno, ali nikako vise na prema vise
	 * 
	 * dakle: kod update-a ne update-ujemo liste SLOZENIH tipova(entiteta), ako ima
	 * potrebe za izbacivanjem nekih ili dodavanjem nekih u listu obracamo se ili
	 * njima(slucaj jedan na prema vise) ili dodatnoj klasi koja je spoj izmedju
	 * entiteta koji 'imaju' kardinalitet vise na prema vise
	 */
	
        this.setBanka(entity.getBanka());
        this.setIme(entity.getIme());
        this.setMesecnoPrimanje(entity.getMesecnoPrimanje());
        this.setPrezime(entity.getPrezime());
        this.setZaposlen(entity.getZaposlen());
    }
}