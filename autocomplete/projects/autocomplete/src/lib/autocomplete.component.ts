import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  OnDestroy,
  ElementRef,
  HostListener,
} from '@angular/core';
import { SortParam } from './shared/service/pageable-crud.service';
import {
  transferArrayItem,
  moveItemInArray,
  CdkDragDrop,
} from '@angular/cdk/drag-drop';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { OptionItem } from './shared/model/interface/option-item.interface';
import { AutocompleteInterface } from './shared/model/interface/autocomplete.interface';
import { NgModel } from '@angular/forms';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'lib-autocomplete',
  templateUrl: './autocomplete.component.html',
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutocompleteComponent implements OnInit, OnDestroy {
  @Input() autocomplete: AutocompleteInterface;

  public showScrollView: boolean = false;
  public sort: boolean = false;
  public ascList: string[] = [];
  public descList: string[] = [];
  public criteria: string;

  private _timeoutForFetching;
  private _previousCriteria: string = undefined;

  constructor(private elRef: ElementRef) {}

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (this.elRef.nativeElement.contains(event.target)) {
      // console.log('focus on Autocomplete2Component');
    } else {
      // console.log('blur on Autocomplete2Component');
      this.showScrollView = false;
      this.sort = false;
    }
  }

  public onFocus = (el: HTMLInputElement): void => {
    // console.log('focus on ', el);

    this.autocomplete.pageDataSource.resetAndFetch(this.criteria);

    this.showScrollView = true;
  };

  public onModelChange = (criteria: any, ngModel: NgModel): void => {
    this.autocomplete.criteria = this.criteria;

    if (this._timeoutForFetching) clearTimeout(this._timeoutForFetching);
    this._timeoutForFetching = setTimeout(() => {
      if (this._previousCriteria !== this.criteria) {
        this.autocomplete.pageDataSource.resetAndFetch(
          this.criteria,
          this._getSortingParams()
        );
      }

      this._previousCriteria = this.criteria;
    }, 500);

    this.autocomplete.value = undefined;
    this.autocomplete.valueSubject.next(this.autocomplete.value);
    // console.log(this.autocomplete);
  };

  public itemSelected = (optionItem: OptionItem): void => {
    // console.log(optionItem);
    if (optionItem) {
      // console.log(optionItem);
      this.autocomplete.criteria = optionItem.getContextToShow();
      this.autocomplete.value = optionItem;
      this.autocomplete.valueSubject.next(this.autocomplete.value);
      this.criteria = this.autocomplete.criteria;

      this.showScrollView = false;
      this.sort = false;

      // console.log(this.autocomplete);
    }
  };

  public onSlideToggleChange = (
    matSlideToggleChange: MatSlideToggleChange
  ): void => {
    this.sort = matSlideToggleChange.checked;
    // console.log(this.sort);

    if (!this.sort)
      this.autocomplete.pageDataSource.resetAndFetch(this.criteria, []);
  };

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      this.autocomplete.pageDataSource.resetAndFetch(
        this.criteria,
        this._getSortingParams()
      );
    }
  }

  private _getSortingParams = (): SortParam[] => {
    let sortParams: SortParam[] = [];
    this.ascList.forEach((field) => {
      sortParams.push({ field: field, order: 'asc' });
    });

    this.descList.forEach((field) => {
      sortParams.push({ field: field, order: 'desc' });
    });

    return sortParams;
  };

  ngOnInit(): void {
    this.criteria = this.autocomplete.criteria;
    this._previousCriteria = this.criteria;

    // console.log('Autocomplete2Component init');
  }

  ngOnDestroy(): void {
    if (this._timeoutForFetching) clearTimeout(this._timeoutForFetching);
    if (this.autocomplete.pageDataSource)
      this.autocomplete.pageDataSource.myDisconnect();

    // console.log('Autocomplete2Component destroyed');
  }
}
