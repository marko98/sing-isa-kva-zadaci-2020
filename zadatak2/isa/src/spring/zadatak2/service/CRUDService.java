package spring.zadatak2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import spring.zadatak2.model.Entity;
import spring.zadatak2.repository.CRUDRepository;

public abstract class CRUDService<T extends Entity<T>> {
	protected CRUDRepository<T> cRUDRepository;
	
	public CRUDRepository<T> getcRUDRepository() {
		return cRUDRepository;
	}

	@Autowired
	public void setcRUDRepository(CRUDRepository<T> cRUDRepository) {
		this.cRUDRepository = cRUDRepository;
	}

	public List<T> findAll() {
		return this.cRUDRepository.findAll();
	}
	
	public T findOne(String identifikator) {
		return this.cRUDRepository.findOne(identifikator);
	}
	
	public void save(T model) {
		this.cRUDRepository.save(model);
	}
	
	public void delete(T model) {
		this.cRUDRepository.delete(model);
	}
	
	public void delete(String identifikator) {
		this.cRUDRepository.delete(identifikator);
	}
}
