package isa.app.dto;

import java.util.ArrayList;

public class KreditDTO extends DTO {

    /**
     * 
     */
    private static final long serialVersionUID = 5660171311031058622L;

    private Long id;

    private Double kolicina;

    private Integer trajanjeUMesecima;

    private Double kamatnaStopa;

    private java.util.List<KreditKlijentDTO> kreditiKlijentiDTO;

    private BankaDTO bankaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Double getKolicina() {
	return this.kolicina;
    }

    public void setKolicina(Double kolicina) {
	this.kolicina = kolicina;
    }

    public Integer getTrajanjeUMesecima() {
	return this.trajanjeUMesecima;
    }

    public void setTrajanjeUMesecima(Integer trajanjeUMesecima) {
	this.trajanjeUMesecima = trajanjeUMesecima;
    }

    public Double getKamatnaStopa() {
	return this.kamatnaStopa;
    }

    public void setKamatnaStopa(Double kamatnaStopa) {
	this.kamatnaStopa = kamatnaStopa;
    }

    public java.util.List<KreditKlijentDTO> getKreditiKlijentiDTO() {
	return this.kreditiKlijentiDTO;
    }

    public void setKreditiKlijentiDTO(java.util.List<KreditKlijentDTO> kreditiKlijentiDTO) {
	this.kreditiKlijentiDTO = kreditiKlijentiDTO;
    }

    public BankaDTO getBankaDTO() {
	return this.bankaDTO;
    }

    public void setBankaDTO(BankaDTO bankaDTO) {
	this.bankaDTO = bankaDTO;
    }

    public KreditDTO() {
	super();
	this.kreditiKlijentiDTO = new ArrayList<KreditKlijentDTO>();
    }

}