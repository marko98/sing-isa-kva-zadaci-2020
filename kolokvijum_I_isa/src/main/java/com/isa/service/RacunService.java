package com.isa.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.isa.model.Racun;

@Service
@Scope("singleton")
public class RacunService extends CrudService<Racun, Long> {

}
