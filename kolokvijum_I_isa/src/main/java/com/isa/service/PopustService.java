package com.isa.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.isa.model.Popust;

@Service
@Scope("singleton")
public class PopustService extends CrudService<Popust, Long> {

}
