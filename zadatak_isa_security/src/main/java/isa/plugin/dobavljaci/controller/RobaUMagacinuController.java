package isa.plugin.dobavljaci.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import isa.controller.CrudController;
import isa.model.Dobavljaci.RobaUMagacinu;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/roba_u_magacinu")
@CrossOrigin(origins = "*")

public class RobaUMagacinuController extends CrudController<RobaUMagacinu, Long> {

}