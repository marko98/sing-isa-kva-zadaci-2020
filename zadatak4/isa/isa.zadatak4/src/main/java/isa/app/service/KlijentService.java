package isa.app.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import isa.app.model.Klijent;

@Service
@Scope("singleton")
public class KlijentService extends CrudService<Klijent, Long> {

}