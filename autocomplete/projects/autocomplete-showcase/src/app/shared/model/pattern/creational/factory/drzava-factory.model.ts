import { EntitetFactory } from 'projects/autocomplete/src/public-api';
import { Drzava } from '../../../entity/drzava.model';
import { DRZAVA_MODEL_INTERFACE } from '../../../interface/drzava.model-interface';

export class DrzavaFactory implements EntitetFactory<Drzava, number> {
  build = (drzavaModelInterfejs?: DRZAVA_MODEL_INTERFACE): Drzava => {
    let drzava: Drzava = new Drzava();

    if (drzavaModelInterfejs) {
      drzava.setId(drzavaModelInterfejs.id);
      drzava.setNaziv(drzavaModelInterfejs.naziv);
      drzava.setStanje(drzavaModelInterfejs.stanje);
      drzava.setVersion(drzavaModelInterfejs.version);
    }

    return drzava;
  };
}
