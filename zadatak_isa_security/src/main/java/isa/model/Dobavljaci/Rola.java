package isa.model.Dobavljaci;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Rola {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String title;

    @OneToMany(mappedBy = "rola")
    private Set<RegistrovanaProdavnicaRola> registrovaneProdavniceRola;

    public Rola() {

    }

    public Rola(Long id, String title, Set<RegistrovanaProdavnicaRola> registrovaneProdavniceRola) {
	super();
	this.id = id;
	this.title = title;
	this.registrovaneProdavniceRola = registrovaneProdavniceRola;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public Set<RegistrovanaProdavnicaRola> getRegistrovaneProdavniceRola() {
	return registrovaneProdavniceRola;
    }

    public void setRegistrovaneProdavniceRola(Set<RegistrovanaProdavnicaRola> registrovaneProdavniceRola) {
	this.registrovaneProdavniceRola = registrovaneProdavniceRola;
    }

}
