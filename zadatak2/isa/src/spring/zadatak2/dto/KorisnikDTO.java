package spring.zadatak2.dto;

import spring.zadatak2.model.Uloga;

public class KorisnikDTO extends DTO {
	private String ime, prezime, korisnickoIme, lozinka;
	private Uloga uloga;
	
	public KorisnikDTO() {}
	
	public KorisnikDTO(String ime, String prezime, String korisnickoIme, Uloga uloga, String lozinka) {
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoIme = korisnickoIme;
		this.uloga = uloga;
		this.lozinka = lozinka;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public Uloga getUloga() {
		return uloga;
	}

	public void setUloga(Uloga uloga) {
		this.uloga = uloga;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
}
