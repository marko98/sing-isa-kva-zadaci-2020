package isa.app.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import isa.app.model.KreditKlijent;

@Repository
@Scope("singleton")
public interface KreditKlijentPASRepository extends PagingAndSortingRepository<KreditKlijent, Long> {

}