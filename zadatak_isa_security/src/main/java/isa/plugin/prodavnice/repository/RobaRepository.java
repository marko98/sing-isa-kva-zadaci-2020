package isa.plugin.prodavnice.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import isa.model.Prodavnice.Roba;
import isa.repository.CrudRepository;

@Repository
@Scope("singleton")

public class RobaRepository extends CrudRepository<Roba, Long> {

}