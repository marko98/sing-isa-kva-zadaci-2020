package spring.zadatak2.model;

import spring.zadatak2.dto.KorisnikDTO;

public class Korisnik implements Entity<Korisnik> {
	private String ime, prezime, korisnickoIme, lozinka;
	private Uloga uloga;
	private boolean ulogovan = false;
	
	public Korisnik() {}
	
	public Korisnik(String ime, String prezime, String korisnickoIme, Uloga uloga, String lozinka) {
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoIme = korisnickoIme;
		this.uloga = uloga;
		this.lozinka = lozinka;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public Uloga getUloga() {
		return uloga;
	}

	public void setUloga(Uloga uloga) {
		this.uloga = uloga;
	}
	
	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public boolean isUlogovan() {
		return ulogovan;
	}

	public void setUlogovan(boolean ulogovan) {
		this.ulogovan = ulogovan;
	}

	//	interface
	public String getIdentifikator() {
		return korisnickoIme;
	}
	
	public void update(Korisnik korisnik) {
		this.setIme(korisnik.getIme());
		this.setPrezime(korisnik.getPrezime());
		this.setUloga(korisnik.getUloga());
		this.setLozinka(korisnik.getLozinka());
	}
	
	public KorisnikDTO getDTO() {
		KorisnikDTO kDTO = new KorisnikDTO();
		kDTO.setIme(this.ime);
		kDTO.setPrezime(this.prezime);
		kDTO.setKorisnickoIme(this.korisnickoIme);
		kDTO.setUloga(this.uloga);
		kDTO.setLozinka(this.lozinka);
		return kDTO;
	}
}
