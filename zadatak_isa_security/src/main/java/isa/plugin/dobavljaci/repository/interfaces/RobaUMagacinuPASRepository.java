package isa.plugin.dobavljaci.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import isa.model.Dobavljaci.RobaUMagacinu;

@Repository
@Scope("singleton")

public interface RobaUMagacinuPASRepository extends PagingAndSortingRepository<RobaUMagacinu, Long> {

}