import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';

// components
import { ArticlesComponent } from './articles/articles.component';
import { UsersComponent } from './users/users.component';

// guard
import { AuthGuard } from './shared/guard/auth.guard';

const routes: Routes = [
    // lazy loading
    {path: '', loadChildren: () => import('./welcome/welcome.module').then(m => m.WelcomeModule)},
    {path: 'articles', component: ArticlesComponent, canActivate: [AuthGuard]},
    {path: 'users', component: UsersComponent, canActivate: [AuthGuard]},
    {path: '**', redirectTo: ''}
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}