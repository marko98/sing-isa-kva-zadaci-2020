package com.isa.model;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.isa.dto.ArtikalUMaloporodajiDTO;
import com.isa.dto.DTO;
import com.isa.dto.RacunDTO;
import com.isa.dto.StavkaRacunaDTO;

@Entity
@Table(name = "stavka_racuna")
public class StavkaRacuna implements ModelEntity<StavkaRacuna, Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "java.lang.Long")
	@Column(name = "id")
	private Long id;
	
//	@Type(type = "java.lang.Double")
	@Column(name = "kolicina")
	private Double kolicina;
	
//	@Type(type = "java.lang.Double")
	@Column(name = "cena")
	private Double cena;
	
	@ManyToOne(fetch = FetchType.EAGER,
				cascade = {
						CascadeType.DETACH,
						CascadeType.MERGE,
						CascadeType.PERSIST,
						CascadeType.REFRESH
				},
				optional = true)
	@JoinColumn(name = "racun_id", nullable = true)
	private Racun racun;
	
	@ManyToOne(fetch = FetchType.EAGER,
				cascade = {
						CascadeType.DETACH,
						CascadeType.MERGE,
						CascadeType.PERSIST,
						CascadeType.REFRESH
				},
				optional = true)
	@JoinColumn(name = "artikal_u_maloprodaji_id", nullable = true)
	private ArtikalUMaloprodaji artikalUMaloprodaji;

	public StavkaRacuna() {
		super();
	}

	public StavkaRacuna(Double kolicina, ArtikalUMaloprodaji artikalUMaloprodaji) {
		super();
		this.kolicina = kolicina;
		artikalUMaloprodaji.addStavkaRacuna(this);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getKolicina() {
		return kolicina;
	}

	public void setKolicina(Double kolicina) {
		this.kolicina = kolicina;
	}

	public Double getCena() {		
		if (this.artikalUMaloprodaji != null)
			return this.artikalUMaloprodaji.getCena() * this.kolicina;
		
		return 0.0;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}	
	
	public Racun getRacun() {
		return racun;
	}

	public void setRacun(Racun racun) {
		this.racun = racun;
	}
	
	public ArtikalUMaloprodaji getArtikalUMaloprodaji() {
		return artikalUMaloprodaji;
	}

	public void setArtikalUMaloprodaji(ArtikalUMaloprodaji artikalUMaloprodaji) {
		this.artikalUMaloprodaji = artikalUMaloprodaji;
	}

	@Override
	public String toString() {
		return "StavkaRacuna [id=" + id + ", kolicina=" + kolicina + ", cena=" + cena + ", racun=" + racun
				+ ", artikalUMaloprodaji=" + artikalUMaloprodaji + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((artikalUMaloprodaji == null) ? 0 : artikalUMaloprodaji.hashCode());
		result = prime * result + ((cena == null) ? 0 : cena.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((kolicina == null) ? 0 : kolicina.hashCode());
		result = prime * result + ((racun == null) ? 0 : racun.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StavkaRacuna other = (StavkaRacuna) obj;
		if (artikalUMaloprodaji == null) {
			if (other.artikalUMaloprodaji != null)
				return false;
		} else if (!artikalUMaloprodaji.equals(other.artikalUMaloprodaji))
			return false;
		if (cena == null) {
			if (other.cena != null)
				return false;
		} else if (!cena.equals(other.cena))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (kolicina == null) {
			if (other.kolicina != null)
				return false;
		} else if (!kolicina.equals(other.kolicina))
			return false;
		if (racun == null) {
			if (other.racun != null)
				return false;
		} else if (!racun.equals(other.racun))
			return false;
		return true;
	}
	
//	interfaces

	@Override
	public void update(StavkaRacuna t) {
		this.setArtikalUMaloprodaji(t.getArtikalUMaloprodaji());
		t.getArtikalUMaloprodaji().addStavkaRacuna(this);
		
		this.setCena(t.getCena());
		this.setKolicina(t.getKolicina());
		
		this.setRacun(t.getRacun());
		t.getRacun().addStavkaRacuna(this);
	}

	@Override
	public DTO getDTO() {
		
		return new StavkaRacunaDTO(id, kolicina, cena, (RacunDTO)racun.getDTO(), (ArtikalUMaloporodajiDTO)artikalUMaloprodaji.getDTO());
	}

}
