package isa.app.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import isa.app.dto.LogMessageDTO;

@Component
public class QueueListeners {

    @JmsListener(destination = "logWarningQueue")
    public void onLogError(LogMessageDTO message) {
	System.out.println(message.getMessageTitle() + ", " + message.getMessageContent());
    }
    
    @JmsListener(destination = "logInformationTopic")
    public void onLogInformation(LogMessageDTO message) {
	System.out.println(message.getMessageTitle() + ", " + message.getMessageContent());
    }

}
