package com.isa.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.isa.hbsf.HibernateSessionFactory;
import com.isa.model.ArtikalUMaloprodaji;
import com.isa.model.ModelEntity;
import com.isa.model.Popust;
import com.isa.model.Racun;
import com.isa.model.StavkaRacuna;

import java.time.LocalDateTime;

public abstract class CrudRepository<T extends ModelEntity<T, ID>, ID extends Serializable> {
	
	@Autowired
	protected PagingAndSortingRepository<T, ID> repository;
	
	@Autowired
	protected HibernateSessionFactory hibernateSessionFactory;
	
//	on init
	@PostConstruct
	public void onInit() {
		System.out.println(this.getClass().getName() + " init");
		
//		get session
		Session session = this.hibernateSessionFactory.getSession();
		
//		begin transaction
		session.beginTransaction();
		
//		crud
		
		Query<Popust> queryP = session.createQuery("select p from Popust as p ", Popust.class);
		List<Popust> popusti = queryP.getResultList();
		
		Query<ArtikalUMaloprodaji> queryA = session.createQuery("select a from ArtikalUMaloprodaji as a ", ArtikalUMaloprodaji.class);
		List<ArtikalUMaloprodaji> artikli = queryA.getResultList();
		
		Query<StavkaRacuna> queryS = session.createQuery("select s from StavkaRacuna as s ", StavkaRacuna.class);
		List<StavkaRacuna> stavkeRacuna = queryS.getResultList();
		
		Query<Racun> queryR = session.createQuery("select r from Racun as r ", Racun.class);
		List<Racun> racuni = queryR.getResultList();
		
		if(popusti.isEmpty() && artikli.isEmpty() && stavkeRacuna.isEmpty() && racuni.isEmpty()) {
			System.out.println("Baza je prazna");
			
//			create popust
			LocalDateTime pocetak = LocalDateTime.of(2020, 2, 10, 12, 20);
			LocalDateTime kraj = LocalDateTime.of(2020, 3, 10, 12, 20);
			Popust popust = new Popust("Extra popust na sve artikle", pocetak, kraj, 20.0);
//			
//			create artikal
			ArtikalUMaloprodaji keks = new ArtikalUMaloprodaji(49.99, 100.0);
			
//			create racun
			LocalDateTime datumIzdavanja = LocalDateTime.of(2020, 3, 2, 15, 21);
			Racun racun1 = new Racun(datumIzdavanja);
			
//			create stavka racuna
			StavkaRacuna stavkaRacuna = new StavkaRacuna(10.0, keks);
			racun1.addStavkaRacuna(stavkaRacuna);
			
			session.save(popust);
			session.save(keks);
			session.save(racun1);
			session.save(stavkaRacuna);
			
		}		
		
//		commit transaction
		session.getTransaction().commit();
		
//		close session
		session.close();
	}
	
//	on destroy
	@PreDestroy
	public void onDestroy() {
		System.out.println(this.getClass().getName() + " destroyed");
	}
	
//	READ
//	find all pageable
	public Page<T> findAll(Pageable pageable) {
		return this.repository.findAll(pageable);
	}
	
//	find all
	public Iterable<T> findAll() {
		return this.repository.findAll();
	}
	
//	find one
	public Optional<T> findOne(ID id) {
		return this.repository.findById(id);
	}
	
//	CREATE and UPDATE
	public void save(T model) {
		this.repository.save(model);
	}
	
//	DELETE
//	delete by id
	public void delete(ID id) {
		this.repository.deleteById(id);
	}
	
//	delete by model
	public void delete(T model) {
		this.repository.delete(model);
	}
	
}