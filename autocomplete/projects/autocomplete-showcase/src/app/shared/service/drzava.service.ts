import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// service
import { PageableCrudService } from 'projects/autocomplete/src/public-api';

// model
import { Drzava } from '../model/entity/drzava.model';
import { DrzavaFactory } from '../model/pattern/creational/factory/drzava-factory.model';

@Injectable()
export class DrzavaService extends PageableCrudService<Drzava, number> {
  constructor(protected httpClient: HttpClient) {
    super(
      httpClient,
      'http',
      'localhost',
      8080,
      'api/drzava',
      'api/drzava/naziv/',
      new DrzavaFactory()
    );
  }
}
