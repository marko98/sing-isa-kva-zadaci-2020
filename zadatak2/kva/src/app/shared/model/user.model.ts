// interface
import { Entity } from './interface/entity.interface';
import { Prototype } from './patterns/creational/prototype/prototype.interface';

export enum Role {
    ADMINISTRATOR,
    KUPAC
}

export declare interface UserDTO {
    "ime": string,
    "prezime": string,
    "korisnickoIme": string,
    "uloga": string,
    "lozinka": string
}

export class User implements Entity<User>, Prototype<User> {
    private firstName: string;
    private lastName: string;
    private username: string;
    private password: string;
    private role: Role;

    constructor(){}

    getFirstName = (): string => {
        return this.firstName;
    }

    setFirstName = (firstName: string): void => {
        this.firstName = firstName;
    }

    getLastName = (): string => {
        return this.lastName;
    }

    setLastName = (lastName: string): void => {
        this.lastName = lastName;
    }

    getUsername = (): string => {
        return this.username;
    }

    setUsername = (username: string): void => {
        this.username = username;
    }

    getPassword = (): string => {
        return this.password;
    }

    setPassword = (password: string): void => {
        this.password = password;
    }

    getRole = (): Role => {
        return this.role;
    }

    getStringRole = (): string => {
        let uloga;
        if (this.getRole() == Role.ADMINISTRATOR)
            uloga = "administrator";
        else if (this.getRole() == Role.KUPAC)
            uloga = "kupac";
        return uloga;
    }

    setRole = (role: Role): void => {
        this.role = role;
    }

    // interface entity
    getIdentificator = (): string => {
        return this.username;
    }

    update = (user: User): void => {
        this.setFirstName(user.getFirstName());
        this.setLastName(user.getLastName());
        this.setRole(user.getRole());
        this.setPassword(user.getPassword());
    }
    
    fromDict = (userDTO: UserDTO): void => {
        this.setFirstName(userDTO.ime);
        this.setLastName(userDTO.prezime);
        this.setUsername(userDTO.korisnickoIme);

        let uloga;
        if (userDTO.uloga == "ADMINISTRATOR")
            uloga = Role.ADMINISTRATOR;
        else if (userDTO.uloga == "KUPAC")
            uloga = Role.KUPAC;
        this.setRole(uloga);

        this.setPassword(userDTO.lozinka);
    };

    toDict = (): UserDTO => {
        let uloga;
        if (this.getRole() == Role.ADMINISTRATOR)
            uloga = "ADMINISTRATOR";
        else if (this.getRole() == Role.KUPAC)
            uloga = "KUPAC";
            
        return {
            "ime": this.getFirstName(),
            "prezime": this.getLastName(),
            "korisnickoIme": this.getUsername(),
            "uloga": uloga,
            "lozinka": this.password
        };
    }

    // interface prototype
    clone = (): User => {
        let clone = new User();
        clone.setFirstName(this.getFirstName());
        clone.setLastName(this.getLastName());
        clone.setUsername(this.getUsername());
        clone.setPassword(this.getPassword());
        clone.setRole(this.getRole());
        return clone;
    }

    prototype = (user: User): void => {
        user.setFirstName(this.getFirstName());
        user.setLastName(this.getLastName());
        user.setUsername(this.getUsername());
        user.setPassword(this.getPassword());
        user.setRole(this.getRole());
    }
}