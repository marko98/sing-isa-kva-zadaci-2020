export declare interface Entity<T> {
    getIdentificator(): string;
    update(t: T): void;
    fromDict(dict: any): void;
    toDict(): any;
}