import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { MainService } from 'src/app/shared/service/main.service';
import { Film } from 'src/app/filmovi/filmovi.component';

@Component({
  selector: 'app-add-projekcija',
  templateUrl: './add-projekcija.component.html',
  styleUrls: ['./add-projekcija.component.css']
})
export class AddProjekcijaComponent implements OnInit, OnDestroy {
  public filmovi: Film[] = [];

  constructor(private httpClient: HttpClient, private router: Router, private mainService: MainService) { }

  convert = (str: string): string => {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }
  
  findAllFilm = (): void => {
    this.httpClient
        .get(
            "http://localhost:3000/film"
        )
        .subscribe(
            (filmovi: Film[]) => {
                filmovi.forEach((film: Film) => {
                    // console.log(film);
                });
                this.filmovi = filmovi;
            },
            err => {
                console.log(err);
            }
        );
    }

  onSubmit = (form: NgForm): void => {
    console.log(form);
    if(form.valid) {
      let pocetak = this.convert(form.value.pocetak) + " " + form.value.sati + ":" + form.value.minute;

      let projekcija = {
        filmId: form.value.filmId,
        sala: form.value.sala,
        pocetak: pocetak
      }

      this.httpClient
        .post(
          "http://localhost:3000/projekcija",
          projekcija
        )
        .subscribe(
          result => {
            this.mainService.findAllProjekcije();
            this.router.navigate(['/projekcije']);
          },
          err => {

          }
        )
    }
  }

  ngOnInit(): void {
    this.findAllFilm();
  }

  ngOnDestroy(): void {
  }

}
