package isa.app.configuration;

import javax.jms.Queue;
import javax.jms.Topic;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfigurations {

    @Bean("logErrorQueue")
    public Queue logErrorQueue() {
	return new ActiveMQQueue("logErrorQueue");
    }

    @Bean("logWarningQueue")
    public Queue logWarningQueue() {
	return new ActiveMQQueue("logWarningQueue");
    }

    @Bean("logInformationTopic")
    public Topic logInformationTopic() {
	return new ActiveMQTopic("logInformationTopic");
    }

}