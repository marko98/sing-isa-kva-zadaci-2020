package isa.app.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import isa.app.dto.LogMessageDTO;

@Component
public class TopicListeners {
    
    @JmsListener(destination = "logInformationTopic")
    @SendTo("receivedInformation")
    public String onLogInformation(LogMessageDTO message) {
	System.out.println(message.getMessageTitle() + ", " + message.getMessageContent());
	
	return "Information received";
    }

}
